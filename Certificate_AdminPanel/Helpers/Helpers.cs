﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;

namespace Certificate_AdminPanel.Helpers
{
    //for image button in tables
    public static class ImageActionLinkHelper
    {
        //public static string ImageActionLink(this AjaxHelper helper, string imageUrl,string altText,string titleText, string actionName, object routeValues,string css, AjaxOptions ajaxOptions)
        public static string ImageActionLink(this AjaxHelper helper, string imageUrl, string id, string url, string actionName, object routeValues, string css, AjaxOptions ajaxOptions)
        {
            var builder = new TagBuilder("img");

            builder.MergeAttribute("src", imageUrl);
            builder.MergeAttribute("id", id);
            builder.MergeAttribute("href", url);

            //builder.MergeAttribute("class", "imageEdit");
            //builder.MergeAttribute("alt", altText);
            //builder.MergeAttribute("title", titleText);

            builder.AddCssClass(css);

            var link = helper.ActionLink("[replaceme]", actionName, routeValues, ajaxOptions);
            return link.ToHtmlString().Replace("[replaceme]", builder.ToString(TagRenderMode.SelfClosing));
        }
    }

    //for image upload control
    public static class HtmlExtensions
    {
        public static MvcHtmlString File(this HtmlHelper html, string name)
        {
            var tb = new TagBuilder("input");
            tb.Attributes.Add("type", "file");
            tb.Attributes.Add("name", name);
            tb.GenerateId(name);
            return MvcHtmlString.Create(tb.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString FileFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression)
        {
            string name = GetFullPropertyName(expression);
            return html.File(name);
        }

        #region Helpers

        static string GetFullPropertyName<T, TProperty>(Expression<Func<T, TProperty>> exp)
        {
            MemberExpression memberExp;

            if (!TryFindMemberExpression(exp.Body, out memberExp))
                return string.Empty;

            var memberNames = new Stack<string>();

            do
            {
                memberNames.Push(memberExp.Member.Name);
            }
            while (TryFindMemberExpression(memberExp.Expression, out memberExp));

            return string.Join(".", memberNames.ToArray());
        }

        static bool TryFindMemberExpression(Expression exp, out MemberExpression memberExp)
        {
            memberExp = exp as MemberExpression;

            if (memberExp != null)
                return true;

            if (IsConversion(exp) && exp is UnaryExpression)
            {
                memberExp = ((UnaryExpression)exp).Operand as MemberExpression;

                if (memberExp != null)
                    return true;
            }

            return false;
        }

        static bool IsConversion(Expression exp)
        {
            return (exp.NodeType == ExpressionType.Convert || exp.NodeType == ExpressionType.ConvertChecked);
        }

        #endregion
    }
}