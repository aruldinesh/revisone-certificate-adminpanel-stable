﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;

namespace Certificate_AdminPanel.Models
{
    [Table("AdminLogin")]
    public class AdminLogin : Gena
    {
        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int64 LoginID { get; set; }

        [Required]
        public Int64 RoleID { get; set; }

        [Display(Name = "Login Name")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9_@.-]*$", ErrorMessage = "Please insert proper value")]
        [Remote("Loginvalid", "Administer", HttpMethod = "POST", AdditionalFields = "LoginID", ErrorMessage = "Name is not available.")]
        [Required]
        public string LoginName { get; set; }
        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        //[Required]
        public string Password { get; set; }
        public string pwd { get; set; }
        public int Type { get; set; }
        
        public Int64 AdminID { get; set; }
        [ForeignKey("AdminID")]
        public virtual AdminMaster AdminMaster { get; set; }

        
      
        //[ForeignKey("UserID")]
        //public virtual UserMaster UserMst { get; set; }
    }
}