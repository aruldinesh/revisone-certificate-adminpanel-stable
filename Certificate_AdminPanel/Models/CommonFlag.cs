﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Certificate_AdminPanel.Models
{
    [Table("CommonFlag")]
    public class CommonFlag
    {
        

         [Key,DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
         public Int32 RecNo { get; set; }

       [Required]
	     public string Name { get; set; }


        [Required]
	     public string Value { get; set; }


       
    }
    [Table("InvoiceFlag")]
    public class InvoiceFlag
    {


        [Key]
        public Int32 SNo { get; set; }

        [Required]
        public string Name { get; set; }


        [Required]
        public int Value { get; set; }



    }

    [Table("GPS_CommonFlag")]
    public class GPS_CommonFlag
    {


        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int32 RecNo { get; set; }

        [Required]
        public string Name { get; set; }


        [Required]
        public string Value { get; set; }



    }
}
