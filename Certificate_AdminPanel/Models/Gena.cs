﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Certificate_AdminPanel.Models
{
    //for general properties in models
    public abstract class Gena
    {
        [DataType(DataType.Date, ErrorMessage = "Please fill in a valid date.")]
        //[RegularExpression(@"^(3[0-1]|2[0-9]|1[0-9]|0[1-9])\/(Jan|JAN|Feb|FEB|Mar|MAR|Apr|APR|May|MAY|Jun|JUN|Jul|JUL|Aug|AUG|Sep|SEP|Oct|OCT|Nov|NOV|Dec|DEC)\/\d{4}$", ErrorMessage = "Fill in a valid date.")]
        [Display(Name = "Created Date")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CDate { get; set; }

        [Display(Name = "Enabled / Disabled")]
        [Required]
        public bool E_Stat { get; set; }
    }

    //for general properties in models
    public abstract class CommonProp
    {

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime MDate { get; set; }

        [Required]
        public Int64 CUserID { get; set; }

        public Int64 MUserID { get; set; }

        [Required]
        public int CType { get; set; }

        public int MType { get; set; }

        [Display(Name = "Enabled / Disabled")]
        [Required]
        public bool E_Stat { get; set; }

    }

}