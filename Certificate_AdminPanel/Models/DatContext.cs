﻿using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Certificate_AdminPanel.Models
{
    public class DatContext : DbContext
    {
        public DbSet<Certificate> certificate_mst { get; set; }
        public DbSet<CertificateLog> certificate_log { get; set; }
        public DbSet<UserMaster> UserMst { get; set; }
        public DbSet<LoginMaster> LoginMst { get; set; }

      


        public DbSet<CompanyMaster> CompanyMst { get; set; }

        public DbSet<AdminMaster> AdminMst { get; set; }

        public DbSet<AdminLogin> AdminLogin { get; set; }

        public DbSet<GmtZone> GMT_ZONE { get; set; }
        public DbSet<Country> countries { get; set; }
        public DbSet<CommonFlag> CommonFlag { get; set; }
        public DbSet<SpeedLimiterSerialNo> SpeedLimiterSerialNo { get; set; }
        public DbSet<TamperSeal> TamperSeal { get; set; }
        public DbSet<Template> Template { get; set; }
        public DbSet<InspectionMaster> InspectionMst { get; set; }
        public DbSet<spare> spare { get; set; }
        public DbSet<Audit> Audit { get; set; }
        public DbSet<Invoice> invoice { get; set; }
        public DbSet<InvoiceFlag> invoice_flag { get; set; }
        public DbSet<Complaints> complaints { get; set; }
        public DbSet<GPSCertificate> gpscertificate { get; set; }


        public DbSet<GPSCertificateLog> gpscertificatelog { get; set; }
        public DbSet<GPSSerialNo> gpsserialno { get; set; }
        public DbSet<GPS_CommonFlag> gps_commonFlag { get; set; }


        public void executesp(string procedurename, object obj)
        {
            using (this)
            {
                //Had to go this route since EF Code First doesn't support output parameters 
                //returned from sprocs very well at this point
                using (this.Database.Connection)
                {
                    this.Database.Connection.Open();
                    DbCommand cmd = this.Database.Connection.CreateCommand();
                    string className = obj.ToString();
                    Type objType = Type.GetType(className);
                    object[] classAttribs = objType.GetCustomAttributes(false);
                    PropertyInfo[] propInfos = objType.GetProperties(BindingFlags.Public | BindingFlags.Instance);


                    PropertyInfo propInfo;

                    for (int i = 0; i < propInfos.Length - 1; i++)
                    {
                        propInfo = propInfos[i];

                        string clasName = propInfo.ToString();
                        Type objTyp = Type.GetType(clasName);


                        DbParameter dbParam = cmd.CreateParameter();
                        dbParam.ParameterName = propInfo.Name;
                        dbParam.Value = propInfo.GetValue(obj, null);
                        cmd.Parameters.Add(dbParam);
                    }

                  
                }
            }
        }

        public DbSet<UserView> UserViews { get; set; }

    }
}