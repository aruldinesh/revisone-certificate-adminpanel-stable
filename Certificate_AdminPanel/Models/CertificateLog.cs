﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Certificate_AdminPanel.Models
{
   
     [Table("certificate_log")]
    public class CertificateLog
    {
         [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
         public int recno { get; set; }
           [Key]
           public string CertificateNo { get; set; }            
           public string VehRegistrationNo { get; set; }
           public string OwnerName { get; set; }
           public string ChassisNo { get; set; }

           
            public string EngineNo { get; set; }
            
          
            public Int32 ManufactureYear { get; set; }

      
            public string MakeOfVehicle { get; set; }
            
           
            public string ModelOfVehicle { get; set; }

    
            public string TypeSpeedLimiter { get; set; }

            public Int32 SetSpeed { get; set; }

            public Int32? speed2 { get; set; }
            public string SerialNo { get; set; }

    
            public string TamperSealNo { get; set; }

            public DateTime InstallDate { get; set; }
         
            public DateTime ExpDate { get; set; }
          
            public string TechnicianName { get; set; }

            
            public string ApplicableStandard { get; set; }

          
            public string Country { get; set; }

            public Int64 DealerId { get; set; }

            public DateTime Cdate { get; set; }

          
            public Int64 Cuser { get; set; }

    
            public DateTime Mdate { get; set; }

        
            public Int64 Muser { get; set; }

            public bool E_Stat { get; set; }

            public Int32 Duplicate { get; set; }
    }
}
