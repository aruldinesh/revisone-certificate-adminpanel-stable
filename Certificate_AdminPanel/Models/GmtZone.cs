﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Certificate_AdminPanel.Models
{
    [Table("GMT_ZONE")]
    public class GmtZone
    {
        [Key]
        public Int32 tab_id { get; set; }
        [Required]
        public string time_zone { get; set; }
        [Required]
        public string Gmt_minute { get; set; }
    }
}