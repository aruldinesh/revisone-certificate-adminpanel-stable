﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Certificate_AdminPanel.Models
{
    [Table("tbl_GPSCertificate")]
    public class GPSCertificate
    {
        
            [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
            public string CertificateNo { get; set; }

           // [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Vehicle Reg No")]
            [RegularExpression("[a-z A-Z 0-9 .-]{1,40}", ErrorMessage = "enter valid characters")]
            public string VehicleNo { get; set; }
        [Required]
         [Display(Name = "Serial No")]
            public string TerminalId{get;set;}
        [Required] 
        [Display(Name = "Engine No")]
public string EngineNo {get;set;}
        [Required] 
        [Display(Name ="Chassis No")]
public string ChassisNo{get;set;}
        [Required] 
        [Display(Name = "Make Of Vehicle")]
public string Makeofvehicle{get;set;}
        [Required] 
        [Display(Name = "Model Of Vehicle")]
public string modelofvehicle{get;set;}
        [Required] 
        [Display(Name = "Fuel Mileage Ratio")]
public decimal fuelmileageratio{get;set;}
        [Required] 
        [Display(Name = "Odometer Reading")]
public decimal OdometerReading{get;set;}
        [Required] 
        [Display(Name = "Max Speed")]
        public double Maxspeed{get;set;}
        [Required]
         [Display(Name = "SIM No")]
        public string SIMNO{get;set;}
    [Display(Name = "IMEI No")]
    [Required]
        public string IMEINO{get;set;}
         [Display(Name = "SOS No")]
public string SOSNO{get;set;}
         [Display(Name = "Owner Name")]
public string OwnerName{get;set;}
     [Display(Name = "Owner Phone")]
public string OwnerPhone{get;set;}
         [Display(Name = "Driver Name")]
public string DriverName{get;set;}
         [Display(Name = "Driver Phone")]
public string DriverPhone { get; set; }
         [Display(Name = "Technician Name")]
public string TechnicianName { get; set; }

[Required]
[Display(Name = "Dealer ")]
public Int64 DealerId { get; set; }
         [Display(Name = "Install Date")]
        public DateTime InstallDate { get; set; }

public DateTime Cdate { get; set; }

public Int64 CUser { get; set; }

public DateTime ExpDate { get; set; }

public bool E_stat { get; set; }
public int Duplicate { get; set; }
         [Display(Name = "Applicable Standard")]
public string ApplicableStandard { get; set; }
public string Country { get; set; }
    }



    [Table("tbl_GPSCertificateLog")]
    public class GPSCertificateLog
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int recno { get; set; }
        public string CertificateNo { get; set; }

        // [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Vehicle Reg No")]
        [RegularExpression("[a-z A-Z 0-9 .-]{1,40}", ErrorMessage = "enter valid characters")]
        public string VehicleNo { get; set; }
        [Required]
        [Display(Name = "Serial No")]
        public string TerminalId { get; set; }
        [Required]
        [Display(Name = "Engine No")]
        public string EngineNo { get; set; }
        [Required]
        [Display(Name = "Chassis No")]
        public string ChassisNo { get; set; }
        [Required]
        [Display(Name = "Make Of Vehicle")]
        public string Makeofvehicle { get; set; }
        [Required]
        [Display(Name = "Model Of Vehicle")]
        public string modelofvehicle { get; set; }
        [Required]
        [Display(Name = "Fuel Mileage Ratio")]
        public decimal fuelmileageratio { get; set; }
        [Required]
        [Display(Name = "Odometer Reading")]
        public decimal OdometerReading { get; set; }
        [Required]
        [Display(Name = "Max Speed")]
        public double Maxspeed { get; set; }
        [Required]
        [Display(Name = "SIM No")]
        public string SIMNO { get; set; }
        [Display(Name = "IMEI No")]
        [Required]
        public string IMEINO { get; set; }
        [Display(Name = "SOS No")]
        public string SOSNO { get; set; }
        [Display(Name = "Owner Name")]
        public string OwnerName { get; set; }
        [Display(Name = "Owner Phone")]
        public string OwnerPhone { get; set; }
        [Display(Name = "Driver Name")]
        public string DriverName { get; set; }
        [Display(Name = "Driver Phone")]
        public string DriverPhone { get; set; }
        [Display(Name = "Technician Name")]
        public string TechnicianName { get; set; }

        [Required]
        [Display(Name = "Dealer ")]
        public Int64 DealerId { get; set; }
        [Display(Name = "Install Date")]
        public DateTime InstallDate { get; set; }

        public DateTime Cdate { get; set; }

        public Int64 CUser { get; set; }

        public DateTime ExpDate { get; set; }

        public bool E_stat { get; set; }
        public int Duplicate { get; set; }
        [Display(Name = "Applicable Standard")]
        public string ApplicableStandard { get; set; }
        public string Country { get; set; }

        public int Muser { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
