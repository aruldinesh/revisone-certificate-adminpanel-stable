﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Certificate_AdminPanel.Models
{
    [Table("tbl_Complaints")]
    public class Complaints
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name="Complaint No")]
        public string ComplaintNo { get; set; }
         [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name="Complaint Date")]
        public DateTime Cdate { get; set; }
                [Required]
        public string Customer { get; set; }
        [Required]
        [Display(Name="Product Description")]
        public string ProductDescription { get; set; }
        [Required]
        [Display(Name="Serial No")]
        public string SerialNo { get; set; }
        [Required]
        [Display(Name="Complaint Details")]
        public string ComplaintDetails { get; set; }
        public string Problem { get; set; }
        public string Reason { get; set; }
         [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfReturn { get; set; }
        public string Remark { get; set; }
        public Int64 Cuser { get; set; }
        public Int64 AdminId { get; set; }
        public bool Stat { get; set; }

        [Display(Name = "Rectififed By")]
        public string RectifiedBy { get; set; }

      }
}
