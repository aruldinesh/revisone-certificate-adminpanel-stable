﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.Models
{
    [Table("template_tbl")]
    public class Template
    {
        [Key,System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [Required(ErrorMessage="Template Name required")]
        public string TemplateName { get; set; }
        [Required(ErrorMessage="View Name required")]
        public string ViewName { get; set; }
    }
}