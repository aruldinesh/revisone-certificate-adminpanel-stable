﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.Models
{
    [Table("spare")]
    public class spare
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string ComponentId { get; set; }
        [Required]
        public string Typeofcomponent { get; set; }
        public DateTime date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? ReceivedDate { get; set; }

        public Int32 Quantity { get; set; }
        public Int32 Used { get; set; }
        [Required]
        public int dealerid { get; set; }
        public string summary { get; set; }
    }
}