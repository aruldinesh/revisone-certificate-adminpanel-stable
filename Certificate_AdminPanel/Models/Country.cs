﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Certificate_AdminPanel.Models
{
    [Table("countries")]
    public class Country
    {
       
        public Int32 id { get; set; }
 [Key]
        public string CountryCode { get; set; }

        public string CountryName { get; set; }
    }
}
