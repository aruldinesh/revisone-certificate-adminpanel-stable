﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.Models
{
   [Table("InspectionMst")]
    public class InspectionMaster : Gena
    {
        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int64 InspID { get; set; }

        [Required]
        [RegularExpression("^([a-zA-Z0-9 .&-]+)$", ErrorMessage = "Invalid  Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [RegularExpression("^([a-zA-Z0-9 .&-]+)$", ErrorMessage = "Invalid  Name")]
        [DataType(DataType.Text)]
        public string DisplayName { get; set; }

        [Required]
        [RegularExpression("(^(\\+?\\-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Invalid number")]
        public string MobileNo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}", ErrorMessage = "Invalid Email")]
        public string Email { get; set; }

        public Int64 UserID { get; set; }

        [Required]
        public Int32 TimeZoneID { get; set; }

        // [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        //[DataType(DataType.Date)]
        [Required]
        //[DateRange(Min = "2010/12/02", Max = "2015/12/20")]
        public DateTime MDate { get; set; }

        [Required]
        //[DataType(DataType.Date)]
        //[DateRange(Min="2010/12/02",Max="2015/12/20")]
        [Display(Name="Expiry Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]       
        public DateTime ExpDate { get; set; }

        [UIHint("DropDownList")]
        [Required(ErrorMessage = "You must select a Contact Prefix")]
        public Int64 AdminID { get; set; }

        [ForeignKey("AdminID")]
        public virtual AdminMaster AdminMst { get; set; }

        [ForeignKey("UserID")]
        public virtual UserMaster UserMst { get; set; }

        [ForeignKey("TimeZoneID")]
        public virtual GmtZone gmtzn { get; set; }
        public IEnumerable<GmtZone> GMT_ZONE { get; set; }
    }
    }
