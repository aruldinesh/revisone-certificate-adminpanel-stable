﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.Models
{
    [Table("SpeedLimiterSerialNo")]
    public class SpeedLimiterSerialNo
    {
        [Key,DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int Recno { get; set; }
        //[Required(ErrorMessage="SerialNo required")]
        public string SerialNo { get; set; }
        //[Required(ErrorMessage="TamperSealNo required")]
        //public string TamperSealNo { get; set; }
        //[Required]
        //public string SpeedLimiter { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Required]
        public int User { get; set; }
        public bool E_Stat { get; set; }
        public Int64 DealerId { get; set; }

        //public string InvoiceNo { get; set; }
        //[ForeignKey("Recno")]
        //public virtual TamperSeal TamperSealNo { get; set; }
    }
    [Table("TamperSeal")]
    public class TamperSeal
    {
        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int Recno { get; set; }
        //[Required]
        public string TamperSealNo { get; set; }
        //[Required]
        public DateTime Date { get; set; }
        //[Required]
        public int User { get; set; }
        public bool E_Stat { get; set; }
    }

    [Table("tbl_gpscertificateserialno")]
    public class GPSSerialNo
    {
        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public int Recno { get; set; }
        //[Required(ErrorMessage="SerialNo required")]
        public string SerialNo { get; set; }
        //[Required(ErrorMessage="TamperSealNo required")]
        //public string TamperSealNo { get; set; }
        //[Required]
        //public string SpeedLimiter { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Required]
        public int User { get; set; }
        public bool E_Stat { get; set; }
        public Int64 DealerId { get; set; }

        //public string InvoiceNo { get; set; }
        //[ForeignKey("Recno")]
        //public virtual TamperSeal TamperSealNo { get; set; }
    }
}