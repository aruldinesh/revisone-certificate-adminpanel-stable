﻿using System.ComponentModel.DataAnnotations;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
namespace Certificate_AdminPanel.Models
{
    [Table("LoginMst")]
    public class LoginMaster : Gena
    {

        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 LoginID { get; set; }

        [Required(ErrorMessage = "Login name")]
        [Display(Name = "LoginName")]
        [DataType(DataType.Text)]
        [StringLength(40)]
        [Remote("Loginvalid", "User", HttpMethod = "POST", AdditionalFields = "UserID", ErrorMessage = "Name is not available.")]
        [RegularExpression("^([a-zA-Z0-9 .-_]+)$", ErrorMessage = "Invalid  Name")]
        public string LoginName { get; set; }

        [Required(ErrorMessage = "Password")]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(40)]
        public string Password { get; set; }
        public string pwd { get; set; }
        [DataType(DataType.Text)]
        public Int64 RoleID { get; set; }

        [DataType(DataType.Text)]
        public int Type { get; set; }

        [Required]
        public Int64 UserID { get; set; }

        [ForeignKey("UserID")]
        public virtual UserMaster UserMst { get; set; }

    }
}