﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
namespace Certificate_AdminPanel.Models
{
    [Table("tbl_Invoice")]
    public class Invoice
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RecNo{get;set;}
        public string InvoiceNo{get;set;}
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime InvoiceDate{get;set;}
        [Required]
        public string PurchaseOrderNo{get;set;}
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PurchaseDate{get;set;}
        public int QuantityOrdered{get;set;}
        public int QuantityShipped{get;set;}
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal UnitPrice { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal ShippingPrice { get; set; }
        public string Description{get;set;}

        public string Buyer { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerAddress1 { get; set; }
        public string BuyerAddress2{get;set;}
        public bool I_stat { get; set; }
    }

    public class InvoiceView
    {
      
        public int RecNo { get; set; }
        public string InvoiceNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime InvoiceDate { get; set; }
        [Required]
        public string PurchaseOrderNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PurchaseDate { get; set; }
        public int QuantityOrdered { get; set; }
        public int QuantityShipped { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal UnitPrice { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public decimal ShippingPrice { get; set; }
        public string Description { get; set; }
       
        public bool I_stat { get; set; }
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}", ApplyFormatInEditMode = true)]
        public Decimal Total { get; set; }

        public string Buyer { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerAddress1 { get; set; }
        public string BuyerAddress2 { get; set; }
        public IEnumerable<string> SerialNO { get; set; }

        public string ImageUrl { get; set; }

        public decimal TotalUnit { get; set; }
    }

}