﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Certificate_AdminPanel.Models
{
     [Table("certificate_mst")]
    public class Certificate
    {
          [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
           public string CertificateNo { get; set; }

            //[Required]
            [DataType(DataType.Text)]
            [Display(Name = "Vehicle Reg No")]
            [RegularExpression("[a-z A-Z 0-9 .-]{1,40}", ErrorMessage = "enter valid characters")]
            public string VehRegistrationNo { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Owner Name")]
            [RegularExpression("[a-z A-Z 0-9 .&,-]{1,100}", ErrorMessage = "invalid characters")]
            public string OwnerName { get; set; }


            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Chassis No")]
            [RegularExpression("[a-z A-Z 0-9 ./-]{6,40}", ErrorMessage = "enter valid ChassisNo")]
            public string ChassisNo { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name="Engine No")]
            [RegularExpression("[a-z A-Z 0-9 ./-]{6,40}", ErrorMessage = "enter valid EngineNo")]
           // [RegularExpression(@"^(?.*[0-9]{3})[a-z0-9]+$", ErrorMessage = "enter valid characters")]

            public string EngineNo { get; set; }
            
            [Required]
            [Display(Name="Manufacture Year")]
            //[StringLength(4,ErrorMessage="enter valid year")]
            public Int32 ManufactureYear { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name="Make Of Vehicle")]
            [RegularExpression("[a-z A-Z 0-9 .&/-]{1,40}", ErrorMessage = "enter valid characters")]
            public string MakeOfVehicle { get; set; }
            
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Model Of Vehicle")]
            [RegularExpression("[a-z A-Z 0-9 .&/-]{1,40}", ErrorMessage = "enter valid characters")]
            public string ModelOfVehicle { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Type Of Speed Limiter")]
            [RegularExpression("[a-z A-Z 0-9 ./-]{1,40}", ErrorMessage = "enter valid characters")]
            public string TypeSpeedLimiter { get; set; }

            [Required]
            [Display(Name = "Set Speed")]
            //[StringLength(4,ErrorMessage="enter valid speed")]
            public Int32 SetSpeed { get; set; }
            public Int32? speed2 { get;set; }
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Serial No")]
            [RegularExpression("[a-z A-Z 0-9 ./-]{1,40}", ErrorMessage = "enter valid characters")]
            public string SerialNo { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Tamper Seal No")]
            [RegularExpression("[a-z A-Z 0-9 ./-]{1,40}", ErrorMessage = "enter valid characters")]
            public string TamperSealNo { get; set; }

            [Required]
           // [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]
            [Display(Name = "Install Date")]
            public DateTime InstallDate { get; set; }
            [Required]
            [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
            [Display(Name="Expiry Date")]
            public DateTime ExpDate { get; set; }
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Technician Name")]
           // [RegularExpression("[a-z A-Z 0-9 .&,/-]{1,80}", ErrorMessage = "invalid characters")]
            public string TechnicianName { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Applicable Standard")]
           // [RegularExpression("[a-z A-Z 0-9 ./-]{1,20}", ErrorMessage = "enter valid characters")]
            public string ApplicableStandard { get; set; }

            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Country")]
            public string Country { get; set; }

            [Required]
            [Display(Name = "Dealer ID")]
            public Int64 DealerId { get; set; }

            [Required]
            [DisplayFormat(DataFormatString="{0:dd'/'MMM'/'yyyy}")]
            //[DataType(DataType.Date)]
            [Display(Name = "Created Date")]
            public DateTime Cdate { get; set; }

            [Required]
            [Display(Name = "C User")]
            public Int64 Cuser { get; set; }

            [Required]
            //[DataType(DataType.Date)]
            [DisplayFormat(DataFormatString="{0:dd'/'MMM'/'yyyy}")]
            [Display(Name = "Modified Date")]
            public DateTime Mdate { get; set; }

            [Required]
            [Display(Name = "M User")]
            public Int64 Muser { get; set; }

            [Required]
            [Display(Name = "Enable")]
            public bool E_Stat { get; set; }

            [Required]
            [Display(Name = "Duplicate")]
            public Int32 Duplicate { get; set; }
            [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
            
            [Display(Name="Calibration Date")]
            public DateTime? CalibrationDate { get; set; }
            [ForeignKey("Country")]
            public virtual Country con { get; set; }
           
    }
}