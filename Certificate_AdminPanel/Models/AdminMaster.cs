﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
namespace Certificate_AdminPanel.Models
{
    [Table("AdminMst")]
    public class AdminMaster:Gena
    {

        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int64 AdminID { get; set; }

       
        public Int64 UpperAdminID { get; set; }

        [Required]
        [RegularExpression("^([a-zA-Z0-9 .&-]+)$", ErrorMessage = "Invalid  Name")]
        [Remote("Namevalid", "Administer", HttpMethod = "POST", AdditionalFields = "AdminID", ErrorMessage = "Name is not available.")]
        public string Name { get; set; }

        [Required]
        [RegularExpression("^([a-zA-Z0-9 .&-]+)$", ErrorMessage = "Invalid  Name")]
        public string DisplayName { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("(^(\\+?\\-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Invalid number")]
        //[RegularExpression(@"^(?:[0-9]+(?:-[0-9])?)*$", ErrorMessage = "Please enter Proper phone number")]
        //[StringLength(20)]
        public string PhoneNo { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("(^(\\+?\\-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Invalid number")]
        //[RegularExpression(@"^(?:[0-9]+(?:-[0-9])?)*$", ErrorMessage = "Please enter Proper Mobile number")]
        //[StringLength(20)]
        public string MobileNo { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",ErrorMessage = "Invalid Email")]
        [Required]
        public string Email { get; set; }

        public Int64 UserID { get; set; }

        public Int32 TimeZoneID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        public DateTime MDate { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }

        
        [Required]
        //[DateGreaterThan("MDate")]
        [Display(Name="Expiry Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]
        public DateTime ExpDate { get; set; }
        

        [Required]
        public int Type { get; set; }
        public int TemplateID { get; set; }
        //[ForeignKey("UserID")]
        //public virtual UserMaster UserMst { get; set; }
        //[ForeignKey("TimeZoneID")]
        //public virtual GmtZone gmtzn { get; set; }
       
        //[ForeignKey("UpperAdminID")]
        //public virtual AdminMaster UpperAdminMaster { get; set; }
        public bool GPS_stat { get; set; }
    }
}