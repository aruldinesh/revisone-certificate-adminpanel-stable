﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Certificate_AdminPanel.Models
{
    [Table("UserMst")]
    public class UserMaster:CommonProp
    {

        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 UserID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Date, ErrorMessage = "Please fill in a valid date.")]
        [Display(Name = "Register Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        //[RegularExpression(@"^(?:((31-(Jan|Mar|May|Jul|Aug|Oct|Dec))|((([0-2]\d)|30)-(Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))|(([01]\d|2[0-8])-Feb))|(29-Feb(?=-((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)))))-((1[6-9]|[2-9]\d)\d{2})$")]
        //[RegDateGraterthan("RegDate")]
        public DateTime RegDate { get; set; }

        [Required(ErrorMessage = "Required")]
        [RegularExpression("(^(\\+?\\-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Invalid number")]       
        //[Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Text)]        
        [RegularExpression("^([a-zA-Z0-9 .&-]+)$", ErrorMessage = "Invalid  Name")]
        [Display(Name = "User Name")]
        public string Name { get; set; }

       
        [Display(Name = "Mobile Number")]       
        [Required]
        [RegularExpression("(^(\\+?\\-? *[0-9]+)([,0-9 ]*)([0-9 ])*$)|(^ *$)", ErrorMessage = "Invalid number")]
        public string Mobile { get; set; }

        
        [Required]
        public Int64 CompID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]         
        public string Address { get; set; }

         }
}