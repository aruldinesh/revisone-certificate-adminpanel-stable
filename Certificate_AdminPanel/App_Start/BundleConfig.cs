﻿using System.Web;
using System.Web.Optimization;

namespace Track
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            // <!-- Bootstrap 3.3.6 -->
            bundles.Add(new StyleBundle("~/bundles/bootstrap").Include("~/AdminTemplates/bootstrap/css/bootstrap.min.css"));
            //Font-awesome
            //
            //<!-- Theme style -->
            bundles.Add(new StyleBundle("~/bundles/Theme").Include("~/AdminTemplates/dist/css/AdminLTE.min.css"));
            // <!-- Material Design -->
            bundles.Add(new StyleBundle("~/bundles/MaterialDesign").Include("~/AdminTemplates/dist/css/bootstrap-material-design.min.css",
                "~/AdminTemplates/dist/css/ripples.min.css",
                "~/AdminTemplates/dist/css/MaterialAdminLTE.min.css"));
            //MaterialAdminLTE Skins
            bundles.Add(new StyleBundle("~/bundles/MaterialThemeSkins").Include("~/AdminTemplates/dist/css/skins/all-md-skins.min.css"));
            // <!-- Morris chart -->
            bundles.Add(new StyleBundle("~/bundles/morris").Include("~/AdminTemplates/plugins/morris/morris.css"));
            //<!-- jvectormap -->
            bundles.Add(new StyleBundle("~/bundles/jvectormap").Include("~/AdminTemplates/plugins/jvectormap/jquery-jvectormap-1.2.2.css"));
            // <!-- Date Picker -->
            bundles.Add(new StyleBundle("~/bundles/Datetime").Include("~/AdminTemplates/plugins/datepicker/datepicker3.css"));
            //<!-- Daterange picker -->
            bundles.Add(new StyleBundle("~/bundles/DateRangePicker").Include("~/AdminTemplates/plugins/daterangepicker/daterangepicker.css"));
            //<!-- bootstrap wysihtml5 - text editor -->
            bundles.Add(new StyleBundle("~/bundles/wysihtml5").Include("~/AdminTemplates/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));

          

            //<!-- Jquery 2.2.3 -->
            bundles.Add(new ScriptBundle("~/bundles/jqueryLib").Include("~/AdminTemplates/plugins/jQuery/jquery-2.2.3.min.js",
                       "~/AdminTemplates/plugins/jQueryUI/jquery-ui.min.js"               
                        ));
            //<!-- Bootstrap 3.3.6 -->
            bundles.Add(new ScriptBundle("~/bundles/Bootstrapjs").Include("~/AdminTemplates/bootstrap/js/bootstrap.min.js"));
            //<!-- Material Design -->
            bundles.Add(new ScriptBundle("~/bundles/MaterialDesignjs").Include("~/AdminTemplates/dist/js/material.min.js",
                      "~/AdminTemplates/dist/js/ripples.min.js"));
            // <!-- Morris.js charts -->
            bundles.Add(new ScriptBundle("~/bundles/Morris").Include("~/Scripts/raphael-min.js", "~/AdminTemplates/plugins/morris/morris.min.js"));
            //<!-- Sparkline -->
            bundles.Add(new ScriptBundle("~/bundles/Sparkline").Include("~/AdminTemplates/plugins/sparkline/jquery.sparkline.min.js"));
            //<!-- jvectormap -->
            bundles.Add(new ScriptBundle("~/bundles/jvectormap").Include("~/AdminTemplates/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                       "~/AdminTemplates/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"));
            //<!-- jQuery Knob Chart -->
            bundles.Add(new ScriptBundle("~/bundles/Knob").Include("~/AdminTemplates/plugins/knob/jquery.knob.js"));
            //<!-- daterangepicker -->
            bundles.Add(new ScriptBundle("~/bundles/DateRangePicker").Include("~/Scripts/moment.min.js","~/AdminTemplates/plugins/daterangepicker/daterangepicker.js",
                       "~/AdminTemplates/plugins/datepicker/bootstrap-datepicker.js"));
            //<!-- datepicker -->
            bundles.Add(new ScriptBundle("~/bundles/Datepicker").Include("~/AdminTemplates/plugins/datepicker/bootstrap-datepicker.js"));
            //<!-- Bootstrap WYSIHTML5 -->
            bundles.Add(new ScriptBundle("~/bundles/WYSIHTML5").Include("~/AdminTemplates/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js"));
            //<!-- Slimscroll -->
            bundles.Add(new ScriptBundle("~/bundles/Slimscroll").Include("~/AdminTemplates/plugins/slimScroll/jquery.slimscroll.min.js"));
            //<!-- FastClick -->
            bundles.Add(new ScriptBundle("~/bundles/FastClick").Include(
                       "~/AdminTemplates/plugins/fastclick/fastclick.js"));
            //<!-- AdminLTE App -->
            bundles.Add(new ScriptBundle("~/bundles/AdminLTEApp").Include("~/AdminTemplates/dist/js/app.min.js","~/AdminTemplates/dist/js/demo.js"));
         
            
            //Datatable style and scripts
            bundles.Add(new ScriptBundle("~/bundles/dataTable").Include("~/AdminTemplates/plugins/datatables/jquery.dataTables.min.js",
                    "~/AdminTemplates/plugins/datatables/dataTables.bootstrap.min.js"));
           
            bundles.Add(new StyleBundle("~/bundles/BootstrapDatatable").Include("~/AdminTemplates/plugins/datatables/dataTables.bootstrap.css"));
            bundles.Add(new StyleBundle("~/bundles/DatatableResposive").Include("~/AdminTemplates/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css"));
            bundles.Add(new ScriptBundle("~/bundles/DatetableResposiveJs").Include("~/AdminTemplates/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js"));
           
            
            BundleTable.EnableOptimizations = true;
        }
    }
}