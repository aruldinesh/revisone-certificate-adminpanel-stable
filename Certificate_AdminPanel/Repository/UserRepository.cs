﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity;
using System.Configuration;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;
using System.Transactions;

namespace Certificate_AdminPanel
{
    public class UserRepository
    {

        DatContext db = new DatContext();

        public bool GetUserObjByUserName(string userName, string passWord)
        {

            var loginmst = db.LoginMst.SingleOrDefault(u => u.LoginName == userName && u.Password == passWord && u.E_Stat == true);
            var adminmst = db.AdminLogin.SingleOrDefault(u => u.LoginName == userName && u.Password == passWord && u.Type == 1 && u.E_Stat == true);


            if (adminmst != null)
            {
                var de = db.AdminMst.SingleOrDefault(x => x.AdminID == adminmst.AdminID);
                if (de.Type == 0)
                {
                    return true;
                }
                else
                {
                    var res = from a in db.AdminLogin
                              where de.AdminID == a.AdminID
                              select a;
                    if (res.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {

                        return false;
                    }
                }
            }
            else   if (loginmst != null)
                {
                    var de = db.UserMst.SingleOrDefault(x => x.UserID == loginmst.UserID);

                    var res = (from a in db.UserMst
                               join c in db.CompanyMst on a.CompID equals c.CompID
                               join d in db.AdminLogin on c.AdminID equals d.AdminID
                               where a.UserID == de.UserID
                               select a);
                    if (res.Count() > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else return false;
            //UserMaster user =  usersTable.SingleOrDefault(u => u.UserName == userName && u.Password == passWord);
            //return user;
        }

        public LoginMaster GetUserObjByUserName(string userName)
        {
            LoginMaster user = db.LoginMst.SingleOrDefault(u => u.LoginName == userName);
            return user;
        }

        public IEnumerable<LoginMaster> GetAllUsers()
        {
            return db.LoginMst.AsEnumerable();
        }

        public int RegisterUser(UserMaster userObj)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    //db.UserMst.Add(userObj);
                    // db.LoginMst.Add(userObj.LoginMst);
                    // db.SaveChanges();
                    scope.Complete(); //  To commit.
                }
                catch (Exception)
                {
                    return 1;
                }
            }

            return 0;
        }

        public void getuserdetails()
        {


            var result = from tab1 in db.UserMst
                         join tab2 in db.LoginMst
                         on tab1.UserID equals tab2.UserID into resultantQuery
                         from tab2 in resultantQuery

                         select new UserView
                         {
                             Name = tab1.Name,
                             Address = tab1.Address,
                             CDate = tab1.CDate,
                             Created = db.UserMst.SingleOrDefault(u => u.UserID == tab1.CUserID).Name,
                             Company = db.CompanyMst.SingleOrDefault(u => u.CompID == tab1.CompID).Name,
                             E_Stat = tab1.E_Stat,
                             LoginName = tab2.LoginName,
                             MDate = tab1.MDate,
                             Mobile = tab1.Mobile,
                             Modified = db.UserMst.SingleOrDefault(u => u.UserID == tab1.MUserID).Name,
                             PhoneNo = tab1.PhoneNo,
                             RegDate = tab1.RegDate,

                             UserID = tab1.UserID
                         };


            //db.UserMst.Select(u => u.UserID == u.LoginMst.UserID);

        }

    }
}
