﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Web;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;

namespace Certificate_AdminPanel.Repository
{
    public class CommonData
    {

        public static void JsonSpeedLimiterPush(List<CertificateView> results)
        {
            string path = HttpContext.Current.Server.MapPath("~/JsonStorage/SpeedLimiter.txt");
            using (DatContext db = new DatContext())
            {
                try
                {

                    var info = new FileInfo(path);
                    if ((!info.Exists) || info.Length == 0)
                    {
                        // file is empty or non-existant  
                        string jsonData = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                        // Creates or overwrites the file with the contents of the JSON
                        System.IO.File.WriteAllText(path, jsonData);
                    }
                    else
                    {
                        // Read existing json data
                        var jsonData = System.IO.File.ReadAllText(path);
                        List<CertificateView> items = JsonConvert.DeserializeObject<List<CertificateView>>(jsonData);
                        if (items != null && !(items.Count() == results.Count()))
                        {

                            // Update json data string
                            string Data = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            System.IO.File.WriteAllText(path, Data);
                        }
                        else if (items == null)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                        else if (DateTime.UtcNow.Subtract(results.OrderByDescending(xx => xx.Mdate).ToList()[0].Mdate).Minutes <= 15)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                        else if (DateTime.UtcNow.Subtract(results.OrderByDescending(xx => xx.Cdate).ToList()[0].Cdate).Minutes <= 15)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
        }
        public static void JsonSpeedLimiterDevicesPush(List<SpdLmtrDeliveryView> results)
        {
            string path = HttpContext.Current.Server.MapPath("~/JsonStorage/SpeedLimiterDeviceDetails.txt");
            using (DatContext db = new DatContext())
            {
                try
                {

                    var info = new FileInfo(path);
                    if ((!info.Exists) || info.Length == 0)
                    {
                        // file is empty or non-existant  
                        string jsonData = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                        // Creates or overwrites the file with the contents of the JSON
                        System.IO.File.WriteAllText(path, jsonData);
                    }
                    else
                    {
                        // Read existing json data
                        var jsonData = System.IO.File.ReadAllText(path);
                        List<CertificateView> items = JsonConvert.DeserializeObject<List<CertificateView>>(jsonData);
                        if (items != null && !(items.Count() == results.Count()))
                        {

                            // Update json data string
                            string Data = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            System.IO.File.WriteAllText(path, Data);
                        }
                        else if (items == null)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                        else if (DateTime.UtcNow.Subtract(results.OrderByDescending(xx => xx.Date).ToList()[0].Date).Minutes < 15)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
        }
        public static void JsonGpsDevicesPush(List<GPSCertificateSerialNoView> results)
        {
            string path = HttpContext.Current.Server.MapPath("~/JsonStorage/GpsDeviceDetails.txt");
            using (DatContext db = new DatContext())
            {
                try
                {

                    var info = new FileInfo(path);
                    if ((!info.Exists) || info.Length == 0)
                    {
                        // file is empty or non-existant  
                        string jsonData = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                        // Creates or overwrites the file with the contents of the JSON
                        System.IO.File.WriteAllText(path, jsonData);
                    }
                    else
                    {
                        // Read existing json data
                        var jsonData = System.IO.File.ReadAllText(path);
                        List<GPSCertificateSerialNoView> items = JsonConvert.DeserializeObject<List<GPSCertificateSerialNoView>>(jsonData);
                        if (items != null && !(items.Count() == results.Count()))
                        {

                            // Update json data string
                            string Data = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            System.IO.File.WriteAllText(path, Data);
                        }
                        else if (items == null)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
        }
        public static void JsonGpsDPush(List<GPSCertificateView> results)
        {
            string path = HttpContext.Current.Server.MapPath("~/JsonStorage/Gps.txt");
            using (DatContext db = new DatContext())
            {
                try
                {

                    var info = new FileInfo(path);
                    if ((!info.Exists) || info.Length == 0)
                    {
                        // file is empty or non-existant  
                        string jsonData = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                        // Creates or overwrites the file with the contents of the JSON
                        System.IO.File.WriteAllText(path, jsonData);
                    }
                    else
                    {
                        // Read existing json data
                        var jsonData = System.IO.File.ReadAllText(path);
                        List<GPSCertificateView> items = JsonConvert.DeserializeObject<List<GPSCertificateView>>(jsonData);
                        if (items != null && !(items.Count() == results.Count()))
                        {

                            // Update json data string
                            string Data = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            System.IO.File.WriteAllText(path, Data);
                        }
                        else if (items == null)
                        {
                            // file is empty or non-existant  
                            string Datas = JsonConvert.SerializeObject(results.ToArray(), Formatting.Indented);
                            // Creates or overwrites the file with the contents of the JSON
                            System.IO.File.WriteAllText(path, Datas);
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
            }
        }
    }
}