﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;

namespace Certificate_AdminPanel.Controllers
{
    [Authorize]
    public class spareController : Controller
    {
        //
        // GET: /spare/
        [Audit]
        public ActionResult Index(spareview _spr)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    if (_spr.startdate != null && _spr.enddate != null && _spr.DealerDD != null)
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   where _spr.DealerDD == b.AdminID && _spr.startdate <= a.date && _spr.enddate >= a.date
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(z => z.ComponentId == a.ComponentId).Where(z=>z.dealerid==_spr.DealerDD).Sum(s => s.Quantity) - db.spare.Where(z => z.ComponentId == a.ComponentId).Where(z=>z.dealerid==_spr.DealerDD).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        AdminList();
                        return View(list.ToList());
                    }
                    else if ( _spr.DealerDD != null)
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   where _spr.DealerDD == b.AdminID 
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(z => z.ComponentId == a.ComponentId).Where(z => z.dealerid == _spr.DealerDD).Sum(s => s.Quantity) - db.spare.Where(z => z.ComponentId == a.ComponentId).Where(z => z.dealerid == _spr.DealerDD).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        AdminList();
                        return View(list.ToList());
                    }
                    else if (_spr.startdate != null && _spr.enddate != null)
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   where _spr.startdate <= a.date && _spr.enddate >= a.date
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Quantity) - db.spare.Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        AdminList();
                        return View(list.ToList());
                    }
                    else
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Quantity) - db.spare.Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        AdminList();
                        return View(list.ToList());
                    }
                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    if (_spr.startdate != null && _spr.enddate != null)
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   where b.AdminID == SessionWrapper.AdminID &&_spr.startdate<=a.date &&_spr.enddate>=a.date
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(k => k.dealerid == SessionWrapper.AdminID).Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Quantity) - db.spare.Where(k => k.dealerid == SessionWrapper.AdminID).Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        return View(list.ToList());
                    }
                    else 
                    {
                        var list = from a in db.spare
                                   join b in db.AdminMst on a.dealerid equals b.AdminID
                                   where b.AdminID == SessionWrapper.AdminID
                                   select new spareview
                                   {
                                       Id = a.Id,
                                       ComponentId = a.ComponentId,
                                       Typeofcomponent = a.Typeofcomponent,
                                       date = a.date,
                                       ReceivedDate = a.ReceivedDate,
                                       Quantity = a.Quantity,
                                       Used = a.Used,
                                       dealerid = a.dealerid,
                                       Balance = a.Quantity - a.Used,
                                       TotalBalance = (db.spare.Where(k => k.dealerid == SessionWrapper.AdminID).Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Quantity) - db.spare.Where(k => k.dealerid == SessionWrapper.AdminID).Where(z => z.ComponentId == a.ComponentId).Sum(s => s.Used)),
                                       summary = a.summary,
                                       DealerName = b.Name,
                                   };
                        return View(list.ToList());
                    }
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }

        }

        public ActionResult Create()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                AdminList();
                return View();
            }
            return View("UnAuthenticated");
        }
        //
        //POST:/spare/Create
        [HttpPost,Audit]
        public ActionResult Create(spare _spare)
        {

            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    _spare.date = DateTime.Now;
                    _spare.Used = 0;

                    if (ModelState.IsValid)
                    {
                        db.spare.Add(_spare);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    AdminList();
                    return View();
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        //
        //GET/spare/Edit
        public ActionResult Edit(long id)
        {
            using(DatContext db=new DatContext())
            {
                spare _spare = db.spare.Find(id);
                return PartialView(_spare);
            }
        }
        //
        //POST/spare/Edit/1
        [HttpPost,Audit]
        public ActionResult Edit(spare _spare)
        {
            using (DatContext db = new DatContext())
            {
               spare sp = db.spare.Find(_spare.Id);
             
               sp.Used = _spare.Used;
               sp.ReceivedDate = _spare.ReceivedDate;
               sp.summary = _spare.summary;
               
                if (ModelState.IsValid)
                {
                    
                    db.Entry(sp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
             }
            return View();
        }

        public ActionResult Delete(long id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    spare _spare = db.spare.Find(id);
                    return PartialView(_spare);
                }
                else
                    return View("UnAuthenticated");
            }
        }
        [HttpPost,Audit,ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            using (DatContext db = new DatContext())
            {
                spare _spare = db.spare.Find(id);
                db.spare.Remove(_spare);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
          
        }

        //dropdown list for admin lists
        [NonAction]
        private void AdminList()
        {
            using (DatContext db = new DatContext())
            {

                List<AdminMaster> admn = (from a in db.AdminMst where a.E_Stat == true select a).ToList();
                ViewBag.Admin = new SelectList(admn, "adminid", "Name");

            }
        }


        private int total(string compid)
        {
            using (DatContext db = new DatContext())
            {
                int tot=0;
                var x = db.spare.Select(a => a.ComponentId == compid);
                if (x.Count() > 0)
                {
                    tot = db.spare.Where(a => a.ComponentId == compid).Sum(a => a.Quantity);
                }
                return tot;
            }
        }
    }
}
