﻿using Certificate_AdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Certificate_AdminPanel.Controllers
{
    public class AccountController : Controller
    {


        //
        // GET: /Account/LogOn
        [Audit]
        public ActionResult LogOn()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            ViewBag.IsAdmin = false;
            return View("LogOn");
        }

        //
        // POST: /Account/LogOn

        [HttpPost, Audit]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                  


                    if (getauthlevel(model.UserName) != null && getauthlevel(model.UserName) != string.Empty && getauthlevel(model.UserName).Equals("user"))
                    {
                       // if (!isAccountDisabled("user", model.UserName, model.Password))
                       // {
                            ViewBag.IsAdmin = false;
                            SessionWrapper.IsAdmin = false;
                            SessionWrapper.DisplayName = getdisplayname(model.UserName);
                            FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                            setParameter(model.UserName);
                            if (HttpRuntime.Cache["LoggedInUsers"] != null) //if the list exists, add this user to it
                            {
                                //get the list of logged in users from the cache
                                List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
                                //Dictionary<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
                                //add this user to the list
                                loggedInUsers.Add(model.UserName);
                                //add the list back into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;

                              

                            }
                            else //the list does not exist so create it
                            {
                                //create a new list
                                List<string> loggedInUsers = new List<string>();
                                //add this user to the list
                                loggedInUsers.Add(model.UserName);
                                //add the list into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                            }
                            
                            
                            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Dashboard");
                            }
                       // }
                       // else
                       // {
                           // ModelState.AddModelError("", "Your Account has been Disabled");
                      //  }
                    }
                    else if ((getauthlevel(model.UserName) != null && getauthlevel(model.UserName) != string.Empty && getauthlevel(model.UserName).Equals("admin")))
                    {
                        //if (!isAccountDisabled("admin", model.UserName, model.Password))
                        //{
                            ViewBag.IsAdmin = true;
                            SessionWrapper.IsAdmin = true;
                            SessionWrapper.DisplayName = getdisplayname(model.UserName);
                            FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                            setParameter1(model.UserName);
                            if (HttpRuntime.Cache["LoggedInUsers"] != null) //if the list exists, add this user to it
                            {
                                //get the list of logged in users from the cache
                                List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
                                //add this user to the list
                                loggedInUsers.Add(model.UserName);
                                //add the list back into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                            }
                            else //the list does not exist so create it
                            {
                                //create a new list
                                List<string> loggedInUsers = new List<string>();
                                //add this user to the list
                                loggedInUsers.Add(model.UserName);
                                //add the list into the cache
                                HttpRuntime.Cache["LoggedInUsers"] = loggedInUsers;
                            }
                            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                return RedirectToAction("Index", "Dashboard");
                            }
                       // }
                       // else
                       // {
                         //   ModelState.AddModelError("", "Your Account has been Disabled");
                      //  }
                    }
                    else
                    {

                        ModelState.AddModelError("", "UserName or Password is incorrect.");
                    }
                }
                else
                {

                    ModelState.AddModelError("", "UserName or Password is incorrect.");
                }
            }


            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private void setParameter1(string p)
        {
            try
            {
                using (DatContext db = new DatContext())
                {
                    var login = db.AdminLogin.SingleOrDefault(c => c.LoginName == p);
                    var adminlogo = db.AdminMst.SingleOrDefault(c => c.AdminID == login.AdminID).Logo;
                    SessionWrapper.AdminID = login.AdminID;
                    //display logo 
                    SessionWrapper.DisplayLogo = adminlogo;
                    if (login.AdminID == 1)
                    {
                        SessionWrapper.AdminLevel = AdminLevel.Admin;
                    }
                    else
                    {
                        SessionWrapper.AdminLevel = AdminLevel.SubAdmin;
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        private string getauthlevel(string username)
        {

            using (DatContext db = new DatContext())
            {
                int x = db.AdminLogin.Where(l => l.LoginName == username && l.Type == 1).Count();
                string val = x > 0 ? (db.AdminLogin.SingleOrDefault(l => l.LoginName == username && l.Type == 1).LoginName) : null;
                if (val != null)
                {
                    return "admin";
                }
                else
                {
                    return "user";
                }
            }
        }
        private bool isAccountDisabled(string usertype, string username, string password)
        {
            using (DatContext db = new DatContext())
            {
                string sha1Pswd = GetMD5Hash(password);
                if (usertype == "user")
                    return !(db.LoginMst.FirstOrDefault(l => l.LoginName == username && l.Password == sha1Pswd).E_Stat);
                else
                    return !(db.AdminLogin.FirstOrDefault(a => a.LoginName == username && a.Password == sha1Pswd).E_Stat);
            }


        }
        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        private void setParameter(string username)
        {
            try
            {
                SessionWrapper.IsAdmin = false;
                DatContext db = new DatContext();
                var usrdet = db.LoginMst.SingleOrDefault(l => l.LoginName == username);
                if (usrdet.Type == 0)
                {
                    SessionWrapper.AdminLevel = AdminLevel.CompanyAdmin;
                    var cmpid = db.LoginMst.SingleOrDefault(a => a.LoginName == username).UserMst.CompID;
                    var admnid = db.CompanyMst.SingleOrDefault(b => b.CompID == cmpid).AdminID;
                    var adminlogo = db.AdminMst.SingleOrDefault(c => c.AdminID == admnid).Logo;
                    SessionWrapper.AdminID = admnid;
                    //display logo 
                    SessionWrapper.DisplayLogo = adminlogo;
                }
                else if (usrdet.Type == 1)
                {
                    SessionWrapper.AdminLevel = AdminLevel.CompanyUser;
                    var cmpid = db.LoginMst.SingleOrDefault(a => a.LoginName == username).UserMst.CompID;
                    var admnid = db.CompanyMst.SingleOrDefault(b => b.CompID == cmpid).AdminID;
                    var adminlogo = db.AdminMst.SingleOrDefault(c => c.AdminID == admnid).Logo;
                    //display logo 
                    SessionWrapper.DisplayLogo = adminlogo;
                    SessionWrapper.AdminID = admnid;
                }
                else if (usrdet.Type == 2)
                {
                    SessionWrapper.AdminLevel = AdminLevel.Inspection;
                    var cmpid = db.LoginMst.SingleOrDefault(a => a.LoginName == username).UserMst.CompID;
                    var admnid = db.InspectionMst.SingleOrDefault(b => b.InspID == cmpid).AdminID;
                    var adminlogo = db.AdminMst.SingleOrDefault(c => c.AdminID == admnid).Logo;
                    //display logo 
                    SessionWrapper.DisplayLogo = adminlogo;
                    SessionWrapper.AdminID = admnid;
                }
                else
                {
                    SessionWrapper.AdminLevel = AdminLevel.None;
                }

                //display name
                SessionWrapper.DisplayName = usrdet.UserMst.Name;
                //company id
                SessionWrapper.CompanyID = usrdet.UserMst.CompID;
                SessionWrapper.UserID = usrdet.UserID;

                //int x;
                //var maxVal = db.DepartmentMst.AsEnumerable()
                //.Max(r => int.TryParse(r.Email, out x) ? (int?)x : null);


                //SessionWrapper.TimeZone = db.CompanyMst.Find(deptdet.CompID).TimeZoneID;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //
        // GET: /Account/LogOff
        [Audit]
        public ActionResult LogOff()
        {

            //SessionWrapper

            string username = User.Identity.Name; //get the users username who is logged in
            if (HttpRuntime.Cache["LoggedInUsers"] != null)//check if the list has been created
            {
                //the list is not null so we retrieve it from the cache
                List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
                if (loggedInUsers.Contains(username))//if the user is in the list
                {
                    //then remove them
                    loggedInUsers.Remove(username);

                }
                // else do nothing
            }
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            FormsAuthentication.SignOut();
            Session.Abandon(); 
            return RedirectToAction("LogOn", "Account");
            
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Account");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        //
        // POST: /Account/ChangePassword

        [Authorize, Audit]
        [HttpPost]
        public ActionResult ChangePassword(string LoginName, string password)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            {
                using (DatContext db = new DatContext())
                {
                    if (db.LoginMst.Count(l => l.LoginName == LoginName) > 0)
                    {
                        LoginMaster objuser = db.LoginMst.SingleOrDefault(z => z.LoginName == LoginName);

                        objuser.LoginName = LoginName;
                        objuser.pwd = password;
                        objuser.Password = AdministerController.GetMD5Hash(password);
                        if (ModelState.IsValid)
                        {
                            db.Entry(objuser).State = EntityState.Modified;
                            db.SaveChanges();
                            TempData["Message1"] = "Change Password";
                            TempData["Message"] = "your Password has been changed successfully!!";
                            return RedirectToAction("ChangePasswordSuccess");
                        }
                        else
                        {
                            ModelState.AddModelError("", "The new password is invalid.");
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid LoginName");
                    }
                    return View();
                }
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        //public ActionResult ChangePassword(ChangePasswordModel model)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        // ChangePassword will throw an exception rather
        //        // than return false in certain failure scenarios.
        //        bool changePasswordSucceeded;
        //        try
        //        {
        //            MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
        //            changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
        //        }
        //        catch (Exception)
        //        {
        //            changePasswordSucceeded = false;
        //        }

        //        if (changePasswordSucceeded)
        //        {
        //            return RedirectToAction("ChangePasswordSuccess");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //
        // GET: /Account/ChangePasswordSuccess
        [Audit]
        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        private string getdisplayname(string uname)
        {
          //  if (Request.IsAuthenticated)
            //{
                DatContext db = new DatContext();
                if (db.AdminLogin.Where(a => a.LoginName == uname && a.Type == 1).Count() > 0)
                {
                    string name = db.AdminMst.Find(db.AdminLogin.FirstOrDefault(l => l.LoginName == uname).AdminID).DisplayName;
                    return name;
                }
                else if (db.LoginMst.Where(a => a.LoginName == uname).Count() > 0)
                {
                    string name = db.LoginMst.FirstOrDefault(l => l.LoginName == uname).UserMst.Name;
                    return name;
                }
                else
                {
                    return "Guest!!";
                }
           // }
          // else { return "Guest !!"; }
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [AllowAnonymous]
        [Audit]
        [HttpPost]
        public ActionResult ForgotPassword(string LoginName)
        {
            DatContext db = new DatContext();

            if (db.LoginMst.Count(l => l.LoginName == LoginName) > 0)
            {
                //var userid = (from i in db.LoginMst
                //              where i.LoginName == LoginName
                //              select i.LoginID);

                var loginid = db.LoginMst.FirstOrDefault(a => a.LoginName == LoginName).LoginID;

                var userid = db.LoginMst.Find(loginid).UserID;
                var compid = db.UserMst.Find(userid).CompID;
                //generate random password
                var newpassword = db.LoginMst.FirstOrDefault(a => a.LoginID == loginid).pwd;
                //reset password
                if (db.CompanyMst.Where(a => a.CompID == compid).Count() > 0)
                {
                    //get user emailid to send password
                    var emailid = (from a in db.UserMst
                                   join c in db.LoginMst on a.UserID equals c.UserID
                                   join b in db.CompanyMst on a.CompID equals b.CompID
                                   where c.LoginID == loginid
                                   select new { CompanyEmail = b.Email }).Single();
                    //send email
                    string subject = "New Password";
                    string body = "<b>Please find your Password</b><br/><b> LoginName=</b>" + LoginName + "<br/><b>Password</b>=" + newpassword;//edit it
                    try
                    {

                        SendEMail(emailid.CompanyEmail, subject, body);
                        TempData["Message1"] = "Retreive Password";
                        TempData["Message"] = "Success! Check your email for Password";

                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occured while sending email." + ex.Message;
                    }
                    return RedirectToAction("ChangePasswordSuccess");
                    //display message
                    //  TempData["Message"] = "Success! Check email we sent for your  Password  ";

                }
                else
                {

                    var emailid = (from a in db.UserMst
                                   join c in db.LoginMst on a.UserID equals c.UserID
                                   join b in db.InspectionMst on a.CompID equals b.InspID
                                   where c.LoginID == loginid
                                   select new { InspectionEmail = b.Email }).Single();
                    //send email
                    string subject = "New Password";
                    string body = "<b>Please find your Password</b><br/><b> Loginname=</b>" + LoginName + "<br/><b>Password</b>=" + newpassword;//edit it
                    try
                    {

                        SendEMail(emailid.InspectionEmail, subject, body);
                        TempData["Message1"] = "Retreive Password";
                        TempData["Message"] = "Success! Check your email for Password";

                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occured while sending email." + ex.Message;
                    }
                    return RedirectToAction("ChangePasswordSuccess");
                }
            }
            else
            {
                TempData["Message"] = "Login Name doesnot exist";
                return View();
            }


        }
        public JsonResult getLoginUsers()
        {
            try
            {
                var login = HttpRuntime.Cache["LoggedInUsers"];
                var logout = HttpRuntime.Cache["LoggedOutUsers"];
                return Json(new { login, logout }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }
        }


        private void SendEMail(string emailid, string subject, string body)
        {
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "cpanel27.interactivedns.com";
            client.Port = 587;


            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("info@thinture.com", "thinture12");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            msg.From = new MailAddress("info@thinture.com");
            msg.To.Add(new MailAddress(emailid));

            msg.Subject = subject;
            msg.IsBodyHtml = true;
            msg.Body = body;

            client.Send(msg);
        }
        //private string getdisplaylogo()
        //{
        //    if (Request.IsAuthenticated)
        //    {
        //        DatContext db = new DatContext();
        //        string logo = (from a in db.LoginMst
        //                       join b in db.UserMst on a.UserID equals b.UserID
        //                       join c in db.CompanyMst on b.CompID equals c.CompID
        //                       join d in db.AdminMst on c.AdminID equals d.AdminID

        //                       where a.LoginName == User.Identity.Name
        //                       select new { d.Logo }).ToString();
        //        var l = db.LoginMst.SingleOrDefault(a => a.LoginName == User.Identity.Name).UserMst.CompID;
        //        var x = db.CompanyMst.SingleOrDefault(b => b.CompID == l).AdminID;
        //        var y = db.AdminMst.SingleOrDefault(c => c.AdminID == x).Logo;
        //        return y;
        //    }
        //    else { return "Guest!!"; }
        //}
        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }


}
