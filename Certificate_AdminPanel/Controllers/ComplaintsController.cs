﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;

namespace Certificate_AdminPanel.Controllers
{
    public class ComplaintsController : Controller
    {
        //
        // GET: /Complaints/
        [Authorize]
        public ActionResult Index()
        {
            using (DatContext db = new DatContext())
            {

                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {

                    //var res = from a in db.complaints
                    //          where a.Stat == false
                    //          select   new 
                    //          {
                    //              Id=a.Id,
                    //              Cdate=a.Cdate,
                    //              Customer=a.Customer,
                    //              DealerId=a.AdminId,
                    //              ProductDescription=a.ProductDescription,
                    //              SerialNo=a.SerialNo,
                    //              ComplaintDetails=a.ComplaintDetails,
                    //              Cuser=a.Cuser,
                    //             AdminId=a.AdminId,
                    //             Stat=a.Stat,
                    //              DateOfReturn=a.DateOfReturn,
                    //              Remark=a.Remark,
                    //              Reason=a.Reason

                    //          };
                    var res = from a in db.complaints
                              where a.Stat == false
                              select new ComplaintsView 
                              {
                                  Id = a.Id,
                                  ComplaintNo=a.ComplaintNo,
                                  Cdate = a.Cdate,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem != null ? a.Problem : "N/A",
                                  Reason = a.Reason != null ? a.Reason : "N/A",
                                  DateOfReturn = a.DateOfReturn ,
                                  Remark = a.Remark != null ? a.Remark : "N/A",
                                  Cuser = a.Cuser,
                                  RectifiedBy=a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  Stat = a.Stat
                              };
                    return View(res.ToList().OrderByDescending(a=>a.Cdate));
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var res = from a in db.complaints
                              //join  b in db.UserMst on a.Cuser equals b.UserID
                              //join c in db.CompanyMst on b.CompID equals c.CompID
                              join d in db.AdminMst on a.AdminId equals d.AdminID
                              where d.AdminID == SessionWrapper.AdminID && a.Stat == false
                              select new ComplaintsView
                              {
                                  Id=a.Id,
                                  ComplaintNo = a.ComplaintNo,
                                  Cdate = a.Cdate,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem!=null?a.Problem:"N/A",
                                  Reason = a.Reason != null ? a.Reason : "N/A",
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark != null ? a.Remark : "N/A",
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId=a.AdminId,
                                  Stat=a.Stat

                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                    var res = from a in db.complaints
                              join b in db.UserMst on a.Cuser equals b.UserID
                              join c in db.CompanyMst on b.CompID equals c.CompID
                              where c.CompID == SessionWrapper.CompanyID && a.Stat == false
                              select new ComplaintsView 
                              {
                                  Id = a.Id,
                                  ComplaintNo = a.ComplaintNo,
                                  Cdate = a.Cdate,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem != null ? a.Problem : "N/A",
                                  Reason = a.Reason != null ? a.Reason : "N/A",
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark != null ? a.Remark : "N/A",
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  Stat = a.Stat
                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    var res = from a in db.complaints
                              join b in db.UserMst on a.Cuser equals b.UserID
                              where b.UserID == SessionWrapper.UserID && a.Stat == false
                              select new ComplaintsView 
                              {
                                  Id = a.Id,
                                  ComplaintNo = a.ComplaintNo,
                                  Cdate = a.Cdate,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem != null ? a.Problem : "N/A",
                                  Reason = a.Reason != null ? a.Reason : "N/A",
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark != null ? a.Remark : "N/A",
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  Stat = a.Stat
                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));
                }

                //var res = from a in db.complaints
                //          join b in db.UserMst on a.Cuser equals b.UserID
                //          //join
                //          select new ComplaintsView
                //          {
                //              Cdate = a.Cdate,
                //              Customer = a.Customer,
                //              ProductDescription = a.ProductDescription,
                //              SerialNo = a.SerialNo,
                //              ComplaintDetails = a.ComplaintDetails,
                //              Problem = a.Problem,
                //              Reason = a.Reason,
                //              DateOfReturn = a.DateOfReturn,
                //              Remark = a.Remark,
                //              Cuser = a.Cuser,
                //              MUser = a.MUser,
                //              DealerId = a.DealerId,
                //              DealerName = b.Name
                //          };
                ////return View(db.complaints.ToList());
                //return View(res.ToList());
            }
            return View("UnAuthenticated");
        }
        [Authorize]
        public ActionResult ComplaintsRecord()
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {

                    var res = from a in db.complaints
                              join b in db.AdminMst on a.AdminId equals b.AdminID
                              where a.Stat == true
                              select new ComplaintsView
                              {
                                  Cdate = a.Cdate,
                                  ComplaintNo = a.ComplaintNo,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem,
                                  Reason = a.Reason,
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark,
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  DealerName = b.Name

                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));

                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var res = from a in db.complaints
                              
                              join d in db.AdminMst on a.AdminId equals d.AdminID
                              where d.AdminID == SessionWrapper.AdminID && a.Stat == true
                              select new ComplaintsView
                           {
                               Cdate = a.Cdate,
                               ComplaintNo = a.ComplaintNo,
                               Customer = a.Customer,
                               ProductDescription = a.ProductDescription,
                               SerialNo = a.SerialNo,
                               ComplaintDetails = a.ComplaintDetails,
                               Problem = a.Problem,
                               Reason = a.Reason,
                               DateOfReturn = a.DateOfReturn,
                               Remark = a.Remark,
                               Cuser = a.Cuser,
                               RectifiedBy = a.RectifiedBy,
                               DealerId = a.AdminId,
                               DealerName = d.Name

                           };
                    return View(res.ToList());
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                    var res = from a in db.complaints
                              join b in db.UserMst on a.Cuser equals b.UserID
                              join c in db.CompanyMst on b.CompID equals c.CompID
                              join d in db.AdminMst on c.AdminID equals d.AdminID
                              where c.CompID == SessionWrapper.CompanyID && a.Stat == true
                              select new ComplaintsView
                              {
                                  Cdate = a.Cdate,
                                  ComplaintNo = a.ComplaintNo,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem,
                                  Reason = a.Reason,
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark,
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  DealerName = d.Name

                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    var res = from a in db.complaints
                              join b in db.UserMst on a.Cuser equals b.UserID
                              where b.UserID == SessionWrapper.UserID && a.Stat == true
                              select new ComplaintsView
                              {
                                  Cdate = a.Cdate,
                                  ComplaintNo = a.ComplaintNo,
                                  Customer = a.Customer,
                                  ProductDescription = a.ProductDescription,
                                  SerialNo = a.SerialNo,
                                  ComplaintDetails = a.ComplaintDetails,
                                  Problem = a.Problem,
                                  Reason = a.Reason,
                                  DateOfReturn = a.DateOfReturn,
                                  Remark = a.Remark,
                                  Cuser = a.Cuser,
                                  RectifiedBy = a.RectifiedBy,
                                  DealerId = a.AdminId,
                                  DealerName = b.Name

                              };
                    return View(res.ToList().OrderByDescending(a => a.Cdate));
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Complaints _complaints)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    _complaints.Cuser = SessionWrapper.UserID;
                    var compid = db.UserMst.Find(SessionWrapper.UserID).CompID;
                    var admnid = db.CompanyMst.Find(compid).AdminID;
                    _complaints.AdminId = admnid;
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                 // _complaints.Cuser = SessionWrapper.CompanyID;
                   _complaints.Cuser = SessionWrapper.UserID;
                    var admnid = db.CompanyMst.Find(SessionWrapper.CompanyID).AdminID;
                    _complaints.AdminId = admnid;
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    _complaints.AdminId = SessionWrapper.AdminID;
                }
                
                    if (ModelState.IsValid)
                {

                    _complaints.ComplaintNo = string.Format("{0}{1}", "TH/CP/", db.complaints.OrderByDescending(a=>a.Id).Select(a=>a.Id).First()+1);
                    db.complaints.Add(_complaints);
                    db.SaveChanges();

                    
                    return RedirectToAction("Index");
                }
            }
            return View();

        }

        //private void SendEMail(string emailid, string subject, string body)
        //{
        //    System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
        //    client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //    client.EnableSsl = true;
        //    client.Host = "cpanel27.interactivedns.com";
        //    client.Port = 587;


        //    System.Net.NetworkCredential credentials = new System.Net.NetworkCredential("ullas@thinture.com", "thinture14");
        //    client.UseDefaultCredentials = false;
        //    client.Credentials = credentials;

        //    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
        //    msg.From = new MailAddress("ullas@thinture.com");
        //    msg.To.Add(new MailAddress(emailid));

        //    msg.Subject = subject;
        //    msg.IsBodyHtml = true;
        //    msg.Body = body;

        //    client.Send(msg);
        //}
       [Authorize]
        public ActionResult Edit(int id)
        {
            using (DatContext db = new DatContext())
            {
                var res = db.complaints.Find(id);
                return PartialView(res);
            }
            //return View();
        }
        [HttpPost]
        public ActionResult Edit(Complaints _comp)
        {
            using (DatContext db = new DatContext())
            {
                //_comp.AdminId = SessionWrapper.AdminID;

                if (_comp.Stat == true)
                {
                    if (_comp.Remark == null)
                    { ModelState.AddModelError("Remark", "Field is required");
 
                    }
                    if (_comp.Reason == null)
                    { ModelState.AddModelError("Reason", "Field is required");
 
                    }
                    if (_comp.Problem == null)
                    { ModelState.AddModelError("Problem", "Field is required");
                    }
                    if (_comp.DateOfReturn == null)
                    {ModelState.AddModelError("DateOfReturn", "Field is required");
 
                    }                     
                   
                }
                if (ModelState.IsValid)
                {
                    db.Entry(_comp).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return PartialView(_comp);
        }
        [Authorize]
        public ActionResult Delete(long id)
        {
            using (DatContext db = new DatContext())
            {
                Complaints _spare = db.complaints.Find(id);
                return PartialView(_spare);
            }

        }

        [HttpPost, Audit, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            using (DatContext db = new DatContext())
            {
                Complaints fg = db.complaints.Find(id);
                db.complaints.Remove(fg);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

    }
}
