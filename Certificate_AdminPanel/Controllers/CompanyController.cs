﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Certificate_AdminPanel.Models;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Certificate_AdminPanel.ViewModels;
using System.Data.SqlClient;

namespace Certificate_AdminPanel.Controllers
{
    [Authorize]
    public class CompanyController : Controller
    {

        private DatContext db = new DatContext();
        List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
      
        string password_create;
        string loginname_create;
        string path_create;
        //
        // GET: /Company/
        [Audit]
        public ActionResult Index()
        {

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            try
            {
                if (SessionWrapper.IsAdmin)
                {

                    //var companymst = db.CompanyMst.Include(c => c.AdminMst).Include(c => c.UserMst).Include(c => c.gmtzn);
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var com = (from a in db.CompanyMst
                                  join b in db.AdminMst on a.AdminID equals b.AdminID
                                  join c in db.UserMst on a.CompID equals c.CompID
                                  join d in db.GMT_ZONE on a.TimeZoneID equals d.tab_id
                                  join e in db.LoginMst on c.UserID equals e.UserID
                                  where a.E_Stat==true
                                  select new companyview
                                  {
                                      CompID=a.CompID,
                                      Name = a.Name,
                                      DisplayName = a.DisplayName,
                                      Address = a.Address,
                                      PhoneNo = a.PhoneNo,
                                      MobileNo = a.MobileNo,
                                      Email = a.Email,
                                      Logo=a.Logo,
                                      LoginName=e.LoginName,
                                      pwd=e.pwd,
                                      //UserID=a.UserID,
                                      E_Stat=a.E_Stat,
                                      //TimeZoneID = a.TimeZoneID,
                                      ExpDate = a.ExpDate,
                                      CDate = a.CDate,
                                      MDate=a.MDate,
                                      AdminID=a.AdminID,
                                     // UserName =c.Name,
                                      AdminName=b.Name,
                                      time_zone=d.time_zone,
                                      isLogin = loggedInUsers.Contains(e.LoginName) ? true : false
                                  }).GroupBy(a=>a.CompID).Select(g=>g.FirstOrDefault());

                        //var com = from a in db.CompanyMst
                        //          join b in db.GMT_ZONE on a.TimeZoneID equals b.tab_id
                        //          select new companyview
                        //              {
                        //                  Name = a.Name,
                        //                  DisplayName = a.DisplayName,
                        //                  Address = a.Address,
                        //                  PhoneNo = a.PhoneNo,
                        //                  MobileNo = a.MobileNo,
                        //                  Email = a.Email,
                        //                  // UserID=a.UserID,
                        //                  TimeZoneID = a.TimeZoneID,
                        //                  ExpDate = a.ExpDate,
                        //                  CDate = a.CDate,
                        //                   //AdminID=a.AdminID,
                        //                  time_zone = b.time_zone,

                        //              };
                         //companymst = from co in companymst orderby co.CompID select co;
                         return View(com);
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        //var companymst = db.CompanyMst.Include(c => c.AdminMst).Include(c => c.UserMst).Include(c => c.gmtzn);
                      var com = (from a in db.CompanyMst
                                  join b in db.AdminMst on a.AdminID equals b.AdminID
                                  join c in db.UserMst on a.CompID equals c.CompID
                                  join d in db.GMT_ZONE on a.TimeZoneID equals d.tab_id
                                  join e in db.LoginMst on c.UserID equals e.UserID 
                                  where a.AdminID==SessionWrapper.AdminID && a.E_Stat==true
                                  select new companyview
                                  {
                                      CompID=a.CompID,
                                      Name = a.Name,
                                      DisplayName = a.DisplayName,
                                      LoginName=e.LoginName,
                                      pwd=e.pwd,
                                      Address = a.Address,
                                      PhoneNo = a.PhoneNo,
                                      MobileNo = a.MobileNo,
                                      Email = a.Email,
                                      Logo=a.Logo,
                                      ExpDate = a.ExpDate,
                                      CDate = a.CDate,
                                      MDate=a.MDate,
                                      //UserID=a.UserID,
                                      AdminID=a.AdminID,
                                      E_Stat=a.E_Stat,
                                      //UserName =c.Name,
                                      AdminName=b.Name,
                                      time_zone=d.time_zone,
                                      isLogin = loggedInUsers.Contains(e.LoginName) ? true : false
                                  }).GroupBy(a=>a.CompID).Select(g=>g.FirstOrDefault());
                        
                        //return View(companymst);
                        //companymst = from co in companymst where co.AdminID == SessionWrapper.AdminID orderby co.CompID select co;
                        return View(com);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return View("UnAuthenticated");
        }

        //
        // GET: /Company/Details/5
        [Audit]
        public ActionResult Details(long? id)
        {
            try
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin||SessionWrapper.AdminLevel==AdminLevel.SubAdmin)
                {
                    CompanyMaster companymaster = db.CompanyMst.Find(id);
                    AdminMaster admn = db.AdminMst.Find(companymaster.AdminID);
                    //var usr = db.UserMst.Where(a => a.Name == (db.CompanyMst.Find(id)).Name);
                    UserMaster us = db.UserMst.FirstOrDefault(a => a.CompID == companymaster.CompID);
                    LoginMaster lgn = db.LoginMst.FirstOrDefault(a=>a.UserID==us.UserID);
                    GmtZone GMT = db.GMT_ZONE.Find(companymaster.TimeZoneID);
                   
                    return PartialView(new companyview 
                    {
                        CompID = companymaster.CompID,
                        Name = companymaster.Name,
                        DisplayName = companymaster.DisplayName,
                        LoginName = lgn.LoginName,
                        pwd = lgn.pwd,
                        Address = companymaster.Address,
                        PhoneNo = companymaster.PhoneNo,
                        MobileNo = companymaster.MobileNo,
                        Email = companymaster.Email,

                        ExpDate = companymaster.ExpDate,
                        CDate = companymaster.CDate,
                        MDate = companymaster.MDate,
                        //UserID=a.UserID,
                        AdminID = admn.AdminID,
                       // E_Stat = companymaster.E_Stat,
                        //UserName =c.Name,
                        AdminName = admn.Name,
                        time_zone = GMT.time_zone,
                    
                    });
                }
                else 
                {
                    return View("UnAuthenticated");
                }
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        ////
        //// GET: /Company/Create
        //public ActionResult Details()
        //{
        //    var abc = from a in db.CompanyMst join b in db.GMT_ZONE on a.TimeZoneID equals b.tab_id select new companyview 
        //    {Name=a.Name,
        //    DisplayName=a.DisplayName,
        //    Address=a.Address,
        //    PhoneNo=a.PhoneNo,
        //    MobileNo=a.MobileNo,
        //    Email=a.Email,
        //    UserID=a.UserID,
        //    TimeZoneID=a.TimeZoneID,
        //    ExpDate=a.ExpDate,
        //    CDate=a.CDate,
        //    AdminID=a.AdminID,
        //    time_zone=b.time_zone,
        //    };
        //    return View(abc);
           
        //}
       
        public ActionResult Create()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                filldropdowns();
               
                return PartialView();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        //
        // POST: /Company/Create1q

        [HttpPost,Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CompanyMaster companymaster)
        {
            loginname_create = null;
            password_create = null;
            
            var x = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {

                if (!string.IsNullOrEmpty(Request["loginname"].ToString()))
                {
                    loginname_create = Request["loginname"].ToString();
                }
                else
                {
                    ModelState.AddModelError("loginname", "Login name required");
                }
                if (!string.IsNullOrEmpty(Request["password"].ToString()))
                {
                    password_create = Request["password"].ToString();
                }
                else
                {
                    ModelState.AddModelError("password", "password  required");
                }

                //if (string.IsNullOrEmpty(lname))
                //{ ModelState.AddModelError("loginname", "Login name required"); }
                if (db.LoginMst.Count(l => l.LoginName == loginname_create) > 0)
                {
                    ModelState.AddModelError("loginname", "Login name already exists");
                }
                else if (companymaster.CDate >= companymaster.ExpDate)
                {
                    ModelState.AddModelError("ExpDate", "Expiry date should be after Created date !");
                }
                else if ( Request.Files["Logo"] == null || Request.Files["Logo"].ContentLength < 0 )
                {
                    path_create = "default";
                }


                if (ModelState.IsValid)
                {

                    //enter login name and password to login mst at the time of company creation
                    DateTime cdate = DateTime.UtcNow;
                    companymaster.MDate = cdate;
                    companymaster.UserID = SessionWrapper.AdminID;
                    companymaster.E_Stat = true;
                    
                    if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        companymaster.AdminID = SessionWrapper.AdminID;
                    }
                    LoginMaster objlogin = new LoginMaster();
                   
                    UserMaster obj_usermst = new UserMaster();
                    
                    //login details

                    objlogin.CDate = companymaster.CDate;
                    objlogin.E_Stat = companymaster.E_Stat;
                    objlogin.LoginName = loginname_create;
                    objlogin.Password = GetMD5Hash(password_create.ToString());
                    objlogin.pwd = password_create.ToString();
                    objlogin.RoleID = 1;
                    objlogin.Type = 0;

                   

                    //user details
                    obj_usermst.MDate = cdate;
                    obj_usermst.MDate = cdate;
                    obj_usermst.Mobile = companymaster.MobileNo;
                    obj_usermst.MUserID = companymaster.UserID;
                    obj_usermst.Name = companymaster.Name;
                    obj_usermst.PhoneNo = companymaster.PhoneNo;
                    obj_usermst.RegDate = companymaster.CDate;
                    obj_usermst.CDate = companymaster.CDate;
                    obj_usermst.Address = companymaster.Address;

                    

                    obj_usermst.E_Stat = companymaster.E_Stat;





                    if (Request.Files["Logo"] != null&& Request.Files["Logo"].ContentLength > 0)
                    {
                        var upload = Request.Files["Logo"];
                        if (upload.ContentType.Equals("image/jpg") || upload.ContentType.Equals("image/jpeg") || upload.ContentType.Equals("image/tif") ||
                            upload.ContentType.Equals("image/gif") || upload.ContentType.Equals("image/png"))
                        {
                            string KeyName = string.Empty;
                            try
                            {
                                KeyName = Guid.NewGuid() + upload.FileName;
                                string path = Server.MapPath("/Content/images/logo/") + KeyName;
                                upload.SaveAs(path);
                                companymaster.Logo = "/Content/images/logo/" + KeyName;
                            }
                            catch (DirectoryNotFoundException)
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("~") + "Content/images/logo/");
                                upload.SaveAs(Server.MapPath("/Content/images/logo/") + KeyName);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("file", "Enter image file only");
                        }
                    }
                    else
                    {
                        companymaster.Logo = "Content/Thinture.jpg";
                    }


                    try
                    {

                        
                        db.CompanyMst.Add(companymaster);
                        db.SaveChanges();
                        obj_usermst.CompID = companymaster.CompID;
                        db.UserMst.Add(obj_usermst);
                        db.SaveChanges();
                        objlogin.UserID = obj_usermst.UserID;
                        db.LoginMst.Add(objlogin);
                        db.SaveChanges();
                        
                       
                    }
                    catch
                    {
                        throw;
                    }

                    return RedirectToAction("Index");
                }


                filldropdowns();
               
                return View(companymaster);
            }
            return View("UnAuthenticated");
        }



        [HttpGet]
        public JsonResult GetCompanyName()
        {
            try
            {
                using (DatContext db = new DatContext())
                {
                    List<string> res;

                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var result = from admn in db.AdminMst
                                     where
                                         admn.E_Stat == true
                                     select admn.Name;
                        res = result.ToList();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var result = from admn in db.AdminMst
                                        where
                                     admn.AdminID==SessionWrapper.AdminID &&
                                         admn.E_Stat == true
                                     select admn.Name;
                        res = result.ToList();
                    }

                    else
                    {
                        var result = from admn in db.AdminMst
                                     join com in db.CompanyMst on admn.AdminID equals com.AdminID
                                     where

                                         admn.E_Stat == true
                                     select admn.Name;
                        res = result.ToList();
                    }
                    var resi = res.Distinct().ToList();



                    return Json(resi, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        public JsonResult GetCompanyDetails(string Name = null)
        {
            if (!string.IsNullOrEmpty(Name))
            {


                var com = from a in db.AdminMst
                          join d in db.GMT_ZONE on a.TimeZoneID equals d.tab_id
                          where a.Name == Name && a.E_Stat == true
                          select new companyview
                          {
                              //CompID = b.CompID,
                              Name = a.Name,
                              DisplayName = a.DisplayName,
                              Address = a.Address,
                              PhoneNo = a.PhoneNo,
                              MobileNo = a.MobileNo,
                              Email = a.Email,
                              Logo = a.Logo,
                              ExpDate = a.ExpDate,
                              CDate = a.CDate,
                              MDate = a.MDate,
                              //UserID=a.UserID,
                              AdminID = a.AdminID,
                              E_Stat = a.E_Stat,
                              //UserName =c.Name,
                              //AdminName = b.Name,
                              TimeZoneID = a.TimeZoneID,
                          };

                //return View(companymst);
                //companymst = from co in companymst where co.AdminID == SessionWrapper.AdminID orderby co.CompID select co;
                return Json(com.ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("not found", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckName(FormCollection form)
        {

            System.Threading.Thread.Sleep(3000);

            string name = form["username"];



            if (name.Equals("sudheesh"))

                return Json(false);

            return Json(true);

        }


        //
        // GET: /Company/Edit/5
       public ActionResult Edit(long? id)
        {
           
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                //HttpContext.Cache.Remove("cacheKey");//claering the cahce 
                CompanyMaster companymaster = db.CompanyMst.Find(id);
                filldropdowns();
                //var cacheKey = string.Format("{0}-{1}", "SomeKey", Session.SessionID);
                if (companymaster.Logo != null)
                {
                    HttpContext.Cache.Insert("cacheKey", companymaster.Logo);//setting the cache with logo
                }
                return PartialView(companymaster);
            }
            else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                //HttpContext.Cache.Remove("cacheKey");//claering the cahce 
                CompanyMaster companymaster = db.CompanyMst.SingleOrDefault(c => c.CompID == id && c.AdminID == SessionWrapper.AdminID);
                filldropdowns();
                //var cacheKey = string.Format("{0}-{1}", "SomeKey", Session.SessionID);
                if (companymaster.Logo != null)
                {
                    HttpContext.Cache.Insert("cacheKey", companymaster.Logo);//setting the cache with logo
                }
                return PartialView(companymaster);
            }
            return View("UnAuthenticated");
        }

        public ActionResult ImageUpload(long ID)
        {
            var ImageSrc = from src in db.CompanyMst
                           where src.CompID == ID
                           select src.Logo;
            return Json(ImageSrc, JsonRequestBehavior.AllowGet);
        }



        //
        // POST: /Company/Edit/5

        [HttpPost,Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CompanyMaster companymaster)
        {

            CompanyMaster comp = db.CompanyMst.Find(companymaster.CompID);
            //if (companymaster.Logo != null)
            //{
                if (Request.Files["Logo"].ContentLength > 0)
                {
                    var upload = Request.Files["Logo"];
                    if (upload.ContentType.Equals("image/jpg") || upload.ContentType.Equals("image/jpeg") || upload.ContentType.Equals("image/tif") ||
                        upload.ContentType.Equals("image/gif") || upload.ContentType.Equals("image/png"))
                    {
                        string KeyName = string.Empty;
                        try
                        {
                            string pp = comp.Logo;
                            string path1 = Server.MapPath(pp);
                            if (System.IO.File.Exists(path1))
                            {
                                System.IO.File.Delete(path1);
                            }

                            KeyName = Guid.NewGuid() + upload.FileName;
                            string path = Server.MapPath("/Content/images/logo/") + KeyName;
                            upload.SaveAs(path);
                            companymaster.Logo = "/Content/images/logo/" + KeyName;
                        }
                        catch (DirectoryNotFoundException)
                        {
                            System.IO.Directory.CreateDirectory(Server.MapPath("~") + "Content/images/logo/");
                            upload.SaveAs(Server.MapPath("/Content/images/logo/") + KeyName);
                        }
                    }
                    
                    comp.Logo = companymaster.Logo;
                }
            //}

                 UserMaster usr = db.UserMst.FirstOrDefault(a => a.CompID == comp.CompID);
            comp.Name = companymaster.Name;
            comp.DisplayName = companymaster.DisplayName;
            comp.Address = companymaster.Address;
            comp.PhoneNo = companymaster.PhoneNo;
            comp.MobileNo = companymaster.MobileNo;
            comp.Email = companymaster.Email;
            comp.TimeZoneID = companymaster.TimeZoneID;
           
            comp.ExpDate = companymaster.ExpDate;
            comp.E_Stat = companymaster.E_Stat;
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                comp.AdminID = companymaster.AdminID;
            }
           
            usr.Name = comp.Name;
            usr.PhoneNo = comp.PhoneNo;
            usr.Mobile = comp.MobileNo;
            usr.MDate = comp.MDate;
            usr.Address = comp.Address;
            if (ModelState.IsValid)
            {
                db.Entry(comp).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(usr).State = EntityState.Modified;
               db.SaveChanges();
                return RedirectToAction("Index");
            }
            filldropdowns();
            return View(companymaster);
        }


        //
        //GET:/Company/Details/5
        //public ActionResult Details(long id)
        //{
        //    CompanyMaster cmp = db.CompanyMst.Find(id);
        //    return PartialView(cmp);
        //}
        
        //
        //POST:/Company/Details/5
        //[HttpPost]
        //public ActionResult Details(CompanyMaster _cmp)
        //{
            
        //}


        //
        // GET: /Company/Delete/5
       
        public ActionResult Delete(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                CompanyMaster companymaster = db.CompanyMst.SingleOrDefault(c => c.CompID == id && c.AdminID == SessionWrapper.AdminID);
                return PartialView(companymaster);
            }
            else if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                CompanyMaster companymasters = db.CompanyMst.Find(id);
                return PartialView(companymasters);
            }
            return View("UnAuthenticated");
        }


        //
        // POST: /Company/Delete/5

        [HttpPost,Audit,ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                CompanyMaster companymaster = db.CompanyMst.Find(id);
               
                db.Database.ExecuteSqlCommand("exec sp_delete_company @CompID", new SqlParameter("@CompID", id));
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("UnAuthenticated");
        }

        private void filldropdowns()
        {
            try
            {
                ViewBag.bind_drop_zone = new SelectList(db.GMT_ZONE, "tab_id", "time_zone", "tab_id");
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var res = from sd in db.AdminMst where sd.Type == 1 select sd;
                    ViewBag.AdminIDs = new SelectList(res, "AdminID", "Name");
                }
            }
            catch { throw; }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }



    }
}