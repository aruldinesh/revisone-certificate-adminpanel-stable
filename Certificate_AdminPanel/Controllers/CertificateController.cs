﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using System.Data.Linq.SqlClient;
using Certificate_AdminPanel.ViewModels;

using ReportManagement;
using System.Data.Objects;
using Certificate_AdminPanel.Repository;
using Certificate_AdminPanel.Helpers;
using System.Configuration;
namespace Certificate_AdminPanel.Controllers
{

    public class CertificateController :PdfViewController
    {

        private String ForRegnValue = ConfigurationManager.AppSettings["ForRegn"] != null ? ConfigurationManager.AppSettings["ForRegn"].ToString() : "ForRegn";
        private int ExpiryDays = ConfigurationManager.AppSettings["ExpDays"] != null ?Convert.ToInt32(ConfigurationManager.AppSettings["ExpDays"].ToString()) : 180;//by default 6 months
        
 
        //
        // GET: /Certificate/
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [Authorize, Audit]
        public ActionResult Index(CertificateView cer)
        {
            using (DatContext db = new DatContext())
            {
                if (cer.Dealerdd != null)
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.DealerId == cer.Dealerdd
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     //CompID=comp.CompID,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();

                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID && a.DealerId == cer.Dealerdd
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();
                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                 && a.DealerId == cer.Dealerdd
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();

                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else
                    {
                        return View("UnAuthenticated");
                    }
                }
                else
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.E_Stat==true
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     //CompID=comp.CompID,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();
                        //Offline storage
                        CommonData.JsonSpeedLimiterPush(cn.OrderByDescending(z => z.Cdate).ToList());

                        return View();
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID &&   a.E_Stat==true
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();
                        // return View(cn.OrderByDescending(z => z.Cdate).ToList());
                        //Offline storage
                        CommonData.JsonSpeedLimiterPush(cn.OrderByDescending(z => z.Cdate).ToList());

                        return View();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                 && a.E_Stat == true
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        CompanyName();
                        // return View(cn.OrderByDescending(z => z.Cdate).ToList());
                        //Offline storage
                        CommonData.JsonSpeedLimiterPush(cn.OrderByDescending(z => z.Cdate).ToList());

                        return View();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    {
                        //var cert = from a in db.certificate_mst join c in db.countries
                        //           on a.Country equals c.CountryCode join b in db.CompanyMst
                        //           on a.DealerId equals b.CompID select new 
                        //           CertificateView{}
                        //var cert = db.certificate_mst.Include(c => c.con);
                        //cert = from a in cert where a.DealerId == SessionWrapper.CompanyID select a;
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.CompID == SessionWrapper.CompanyID
                                 && a.E_Stat == true
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        //Offline storage
                        CommonData.JsonSpeedLimiterPush(cn.OrderByDescending(z => z.Cdate).ToList());

                        return View();
                        //return View(cn.OrderByDescending(z => z.Cdate).ToList());

                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID

                                 where a.Cuser == SessionWrapper.UserID
                                 && a.E_Stat == true
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName,
                                     CalibrationDate = a.CalibrationDate

                                 };
                        //Offline storage
                        CommonData.JsonSpeedLimiterPush(cn.OrderByDescending(z => z.Cdate).ToList());

                        return View();
                        //return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else
                    {
                        return View("UnAuthenticated");
                    }
                }
            }

        }
        //
        // GET: /Certificate1/Details/5
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [Authorize, Audit]
        public ActionResult Details(string id)
        {

            using (DatContext db = new DatContext())
            {


                Certificate certificate = db.certificate_mst.Find(id);
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                string barCode = BarCodeToHTML.get39(certificate.CertificateNo, 1, 50);
                ViewBag.htmlBarcode = barCode;
                CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);
                Country con = db.countries.Find(certificate.Country);

                return View(new CertificateView
                {
                    CertificateNo = certificate.CertificateNo,
                    VehRegistrationNo = certificate.VehRegistrationNo,
                    OwnerName = certificate.OwnerName,
                    ChassisNo = certificate.ChassisNo,
                    EngineNo = certificate.EngineNo,
                    ManufactureYear = certificate.ManufactureYear,
                    MakeOfVehicle = certificate.MakeOfVehicle,
                    ModelOfVehicle = certificate.ModelOfVehicle,
                    TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                    SetSpeed = certificate.SetSpeed,
                    speed2 = certificate.speed2,
                    SerialNo = certificate.SerialNo,
                    TamperSealNo = certificate.TamperSealNo,
                    InstallDate = certificate.InstallDate,
                    ExpDate = certificate.ExpDate,
                    TechnicianName = certificate.TechnicianName,
                    ApplicableStandard = certificate.ApplicableStandard,
                    Country = certificate.Country,
                    Cdate = certificate.Cdate,
                    Cuser = certificate.Cuser,
                    Mdate = certificate.Mdate,
                    Muser = certificate.Muser,
                    E_Stat = certificate.E_Stat,
                    Duplicate = certificate.Duplicate,
                    DealerId = certificate.DealerId,
                    //CompID=comp.CompID,
                    Name = comp.Name,
                    Address = comp.Address,
                    CountryName = con.CountryName,
                    CalibrationDate = certificate.CalibrationDate
                });
            }
        }


        //
        //Certitificate/Details/1
        //[ValidateAntiForgeryToken]
        [Audit]
        public ActionResult Det(string id)
        {
            using (DatContext db = new DatContext())
            {
                Certificate cer = db.certificate_mst.Find(id);
                CompanyMaster comp = db.CompanyMst.Find(cer.DealerId);
                Country con = db.countries.Find(cer.Country);

                return View(new CertificateView
                {
                    CertificateNo = cer.CertificateNo,
                    VehRegistrationNo = cer.VehRegistrationNo,
                    OwnerName = cer.OwnerName,
                    ChassisNo = cer.ChassisNo,
                    EngineNo = cer.EngineNo,
                    ManufactureYear = cer.ManufactureYear,
                    MakeOfVehicle = cer.MakeOfVehicle,
                    ModelOfVehicle = cer.ModelOfVehicle,
                    TypeSpeedLimiter = cer.TypeSpeedLimiter,
                    SetSpeed = cer.SetSpeed,
                    speed2 = cer.speed2,
                    SerialNo = cer.SerialNo,
                    TamperSealNo = cer.TamperSealNo,
                    InstallDate = cer.InstallDate,
                    ExpDate = cer.ExpDate,
                    TechnicianName = cer.TechnicianName,
                    ApplicableStandard = cer.ApplicableStandard,
                    //Country = a.Country,
                    Cdate = cer.Cdate,
                    Cuser = cer.Cuser,
                    Mdate = cer.Mdate,
                    Muser = cer.Muser,
                    E_Stat = cer.E_Stat,
                    Duplicate = cer.Duplicate,
                    DealerId = cer.DealerId,
                    //CompID=comp.CompID,
                    Name = comp.DisplayName,
                    CountryName = con.CountryName,
                    CalibrationDate = cer.CalibrationDate == null ? cer.InstallDate : cer.CalibrationDate

                });



            }

        }



        //
        // GET: /Certificate/Create
        [Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [NoDirectAccess]
        public ActionResult Create()
        {
            if (SessionWrapper.IsAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
            {
                dropdown();
                CompanyName();
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        //
        // POST: /Certificate/Create

        [NoDirectAccess]
        [ValidateAntiForgeryToken, Authorize, Audit, HttpPost, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Create(Certificate certificate)
        {
            using (DatContext db = new DatContext())
            {
                var Exist_exp = from a in db.certificate_mst where a.ExpDate > DateTime.UtcNow select a;
                var Exist_Estat = from a in db.certificate_mst where a.E_Stat == true select a;
                Certificate cert = Exist_Estat.SingleOrDefault(c => c.VehRegistrationNo == certificate.VehRegistrationNo);

                if (certificate.VehRegistrationNo != null && certificate.VehRegistrationNo.Trim().ToUpper() == "DEMO")
                {
                    CertificateView cv = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        certificate.Cuser = SessionWrapper.AdminID;
                        certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        cv.AdminID = adminmaster.AdminID;
                        cv.AdminName = adminmaster.DisplayName.ToUpper();
                        cv.AdminAddress = adminmaster.Address;
                        cv.AdminLogo = adminmaster.Logo;
                        cv.AdminPhone = adminmaster.PhoneNo;
                        cv.AdminEmail = adminmaster.Email;
                        cv.AdminTemplate = temp.ViewName;
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        cv.AdminID = adminmaster.AdminID;
                        cv.AdminName = adminmaster.DisplayName.ToUpper();
                        cv.AdminAddress = adminmaster.Address;
                        cv.AdminPhone = adminmaster.PhoneNo;
                        cv.AdminLogo = adminmaster.Logo;
                        cv.AdminEmail = adminmaster.Email;
                        cv.AdminTemplate = temp.ViewName;
                        certificate.Cuser = SessionWrapper.UserID;
                        certificate.Muser = SessionWrapper.UserID;
                    }
                    if (!SessionWrapper.IsAdmin)
                    {
                        certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    certificate.Cdate = DateTime.UtcNow;
                    certificate.Mdate = DateTime.UtcNow;

                    if (certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                    {

                        certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
                    }
                    else
                    {

                        certificate.ExpDate = certificate.InstallDate.AddDays(-1).AddYears(1);
                    }

                    certificate.E_Stat = true;

                    Certificate cr = db.certificate_mst.Find(certificate.VehRegistrationNo);
                    if (certificate.CalibrationDate != null)
                    {
                        certificate.CalibrationDate = certificate.CalibrationDate;
                    }
                    //Certificate cr = new Certificate();
                    //cr.CertificateNo = cerr.CertificateNo;
                    if (ModelState.IsValid)
                    {
                        cr.SerialNo = certificate.SerialNo;
                        cr.TamperSealNo = certificate.TamperSealNo;
                        cr.TechnicianName = certificate.TechnicianName;
                        cr.VehRegistrationNo = certificate.VehRegistrationNo;
                        cr.Mdate = Convert.ToDateTime(cr.Mdate);
                        cr.Muser = certificate.Muser;
                        cr.ChassisNo = certificate.ChassisNo;
                        cr.Country = certificate.Country;
                        cr.ApplicableStandard = certificate.ApplicableStandard;
                        cr.EngineNo = certificate.EngineNo;
                        cr.Cdate = Convert.ToDateTime(certificate.Cdate);
                        cr.ExpDate = Convert.ToDateTime(certificate.ExpDate);
                        cr.InstallDate = certificate.InstallDate;
                        cr.MakeOfVehicle = certificate.MakeOfVehicle;
                        cr.ModelOfVehicle = certificate.ModelOfVehicle;
                        cr.OwnerName = certificate.OwnerName;
                        cr.SetSpeed = certificate.SetSpeed;
                        cr.speed2 = certificate.speed2;
                        cr.ManufactureYear = certificate.ManufactureYear;
                        cr.TypeSpeedLimiter = certificate.TypeSpeedLimiter;
                        cr.E_Stat = certificate.E_Stat;
                        cr.Duplicate = 0;


                        db.Entry(cr).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

                        Country con = db.countries.Find(certificate.Country);
                        CertificateView objcertificateview = new CertificateView
                        {
                            CertificateNo = certificate.CertificateNo,
                            VehRegistrationNo = certificate.VehRegistrationNo,
                            OwnerName = (certificate.OwnerName).ToUpper(),
                            ChassisNo = certificate.ChassisNo,
                            EngineNo = certificate.EngineNo,
                            ManufactureYear = certificate.ManufactureYear,
                            MakeOfVehicle = certificate.MakeOfVehicle,
                            ModelOfVehicle = certificate.ModelOfVehicle,
                            TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                            SetSpeed = certificate.SetSpeed,
                            speed2 = certificate.speed2,
                            SerialNo = certificate.SerialNo,
                            TamperSealNo = certificate.TamperSealNo,
                            InstallDate = certificate.InstallDate,
                            ExpDate = certificate.ExpDate,
                            TechnicianName = certificate.TechnicianName,
                            ApplicableStandard = certificate.ApplicableStandard,
                            Country = certificate.Country,
                            Cdate = certificate.Cdate,
                            Cuser = certificate.Cuser,
                            Mdate = certificate.Mdate,
                            Muser = certificate.Muser,
                            E_Stat = certificate.E_Stat,
                            Duplicate = certificate.Duplicate,
                            DealerId = certificate.DealerId,
                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                            MobileNo = comp.PhoneNo.Trim()!=comp.MobileNo.Trim()?comp.MobileNo:null,
                            CountryName = con.CountryName,
                            AdminName = cv.AdminName,
                            AdminAddress = cv.AdminAddress,
                            AdminPhone = cv.AdminPhone,
                            AdminEmail = cv.AdminEmail,
                            CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };
                        FillImageUrl(objcertificateview, cv.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", cv.AdminTemplate, objcertificateview);
                    }

                    dropdown();
                    CompanyName();
                    return View(certificate);
                }

                else
                {
                    if (certificate.VehRegistrationNo != null)
                    {

                        // if (db.certificate_mst.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 1)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");

                        // }
                        //else


                        if (Exist_exp.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 0)
                        {
                            ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        }

                        //if (db.certificate_mst.SingleOrDefault(a => a.VehRegistrationNo == certificate.VehRegistrationNo).E_Stat == true)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        // }

                    }
                    else
                    {
                        certificate.VehRegistrationNo =ForRegnValue!=null?ForRegnValue:"";
                    }
                    if (certificate.ChassisNo != null)
                    {

                        if (Exist_exp.Where(c => c.ChassisNo == certificate.ChassisNo).Count() > 0)
                        {
                            ModelState.AddModelError("ChassisNo", "ChassisNo already Exists");
                        }

                    }
                    if (certificate.EngineNo != null)
                    {
                        if (Exist_exp.Where(e => e.EngineNo == certificate.EngineNo).Count() > 0)
                        {
                            ModelState.AddModelError("EngineNo", "EngineNo already Exists");
                        }
                    }
                    //if (certificate.ApplicableStandard != null)
                    //{
                    //    if (certificate.ApplicableStandard == "KS 2295-2011")
                    //    {
                    //        certificate.SerialNo = string.Format("{0}{1}", "TK/S2/", certificate.SerialNo);
                    //    }
                    //}

                    var Exist_serial = from a in db.SpeedLimiterSerialNo where a.E_Stat == false select a;
                    var Exist_serialNo = from a in db.SpeedLimiterSerialNo where a.E_Stat == true select a;
                    //var Exist_tampseal = from a in db.TamperSeal where a.E_Stat == false select a;
                    // var Exist_tampsealNo = from a in db.TamperSeal where a.E_Stat == true select a;
                    if (certificate.SerialNo != null)
                    {

                        if (Exist_serial.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                            }


                        }
                        else if (Exist_serialNo.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already created ");
                            }

                            var z = db.certificate_mst.FirstOrDefault(a => a.SerialNo == certificate.SerialNo);
                            if (z.ChassisNo != certificate.ChassisNo)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already created with another device");

                            }
                            //if (db.certificate_mst.Where(l=>l.CertificateNo==certificate.CertificateNo).Count()>0)
                            //{
                            //    ModelState.AddModelError("SerialNo", "SerialNo already created with another device");
                            //}
                            //if()
                        }
                        else
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo does not exist in the available list");
                        }


                    }
                    //if (certificate.TamperSealNo != null )
                    //{
                    //    //if (Exist_tampseal.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{

                    //        if (Exist_exp.Where(t => t.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //        {
                    //            ModelState.AddModelError("TamperSealNo", "TamperSealNo already Exists");
                    //        }


                    //    //}
                    //    //else if (Exist_tampsealNo.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo already created");
                    //    //}
                    //    //else
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo doesnot Exists");
                    //    //}



                    //}


                    string certnum = (Convert.ToInt32(db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();
                    if (certificate.EngineNo != null)
                    {
                        certificate.CertificateNo = string.Format("{0}-{1}-{2}", certificate.Country, GetLast(certificate.EngineNo, 6), certnum.PadLeft(4, '0'));
                    }
                    CertificateView ce = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        certificate.Cuser = SessionWrapper.AdminID;
                        certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                        certificate.Cuser = SessionWrapper.UserID;
                        certificate.Muser = SessionWrapper.UserID;
                    }

                    if (!SessionWrapper.IsAdmin)
                    {
                        certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    certificate.Cdate = DateTime.UtcNow;
                    certificate.Mdate = DateTime.UtcNow;

                    if (certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                    {

                        certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
                    }
                    else
                    {

                        certificate.ExpDate = certificate.InstallDate.AddDays(-1).AddYears(1);
                    }

                    certificate.E_Stat = true;

                    if (ModelState.IsValid)
                    {
                        if (cert != null)
                        {
                            cert.E_Stat = false;
                            db.Entry(cert).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        db.certificate_mst.Add(certificate);
                        db.SaveChanges();
                        SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                        spd.E_Stat = true;
                        db.Entry(spd).State = EntityState.Modified;
                        //TamperSeal tamp = db.TamperSeal.SingleOrDefault(a => a.TamperSealNo == certificate.TamperSealNo);
                        //tamp.E_Stat = true;
                        //db.Entry(tamp).State = EntityState.Modified;
                        CommonFlag comflag = db.CommonFlag.Find(1);
                        comflag.Value = certnum;
                        db.Entry(comflag).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

                        Country con = db.countries.Find(certificate.Country);
                        CertificateView objcertificateview = new CertificateView
                        {
                            CertificateNo = certificate.CertificateNo,
                            VehRegistrationNo = certificate.VehRegistrationNo,
                            OwnerName = (certificate.OwnerName).ToUpper(),
                            ChassisNo = certificate.ChassisNo,
                            EngineNo = certificate.EngineNo,
                            ManufactureYear = certificate.ManufactureYear,
                            MakeOfVehicle = certificate.MakeOfVehicle,
                            ModelOfVehicle = certificate.ModelOfVehicle,
                            TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                            SetSpeed = certificate.SetSpeed,
                            speed2 = certificate.speed2,
                            SerialNo = certificate.SerialNo,
                            TamperSealNo = certificate.TamperSealNo,
                            InstallDate = certificate.InstallDate,
                            ExpDate = certificate.ExpDate,
                            TechnicianName = certificate.TechnicianName,
                            ApplicableStandard = certificate.ApplicableStandard,
                            Country = certificate.Country,
                            Cdate = certificate.Cdate,
                            Cuser = certificate.Cuser,
                            Mdate = certificate.Mdate,
                            Muser = certificate.Muser,
                            E_Stat = certificate.E_Stat,
                            Duplicate = certificate.Duplicate,
                            DealerId = certificate.DealerId,
                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                            CountryName = con.CountryName,
                            AdminName = ce.AdminName,
                            AdminAddress = ce.AdminAddress,
                            AdminPhone = ce.AdminPhone,
                            AdminEmail = ce.AdminEmail,
                            CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };

                        FillImageUrl(objcertificateview, ce.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", ce.AdminTemplate, objcertificateview);

                    }


                }

                dropdown();
                CompanyName();
                return View(certificate);
            }
        }
        [Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [NoDirectAccess]
        public ActionResult Calibrate()
        {
            dropdown();
            registrationdropdown();
            CompanyName();
            return View();
        }
        [ValidateAntiForgeryToken, Authorize, HttpPost, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [NoDirectAccess]
        public ActionResult Calibrate(Certificate certificate)
        {
            using (DatContext db = new DatContext())
            {

                var Exist_exp = from a in db.certificate_mst where a.ExpDate > EntityFunctions.AddDays(DateTime.UtcNow, ExpiryDays) select a;
                var Exist_Estat = from a in db.certificate_mst where a.E_Stat == true select a;
                Certificate cert = Exist_Estat.SingleOrDefault(c => c.VehRegistrationNo == certificate.VehRegistrationNo);

                if (certificate.VehRegistrationNo != null && certificate.VehRegistrationNo.Trim().ToUpper() == "DEMO")
                {
                    CertificateView cv = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        certificate.Cuser = SessionWrapper.AdminID;
                        certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        cv.AdminID = adminmaster.AdminID;
                        cv.AdminName = adminmaster.DisplayName.ToUpper();
                        cv.AdminAddress = adminmaster.Address;
                        cv.AdminLogo = adminmaster.Logo;
                        cv.AdminPhone = adminmaster.PhoneNo;
                        cv.AdminEmail = adminmaster.Email;
                        cv.AdminTemplate = temp.ViewName;
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        cv.AdminID = adminmaster.AdminID;
                        cv.AdminName = adminmaster.DisplayName.ToUpper();
                        cv.AdminAddress = adminmaster.Address;
                        cv.AdminPhone = adminmaster.PhoneNo;
                        cv.AdminLogo = adminmaster.Logo;
                        cv.AdminEmail = adminmaster.Email;
                        cv.AdminTemplate = temp.ViewName;
                        certificate.Cuser = SessionWrapper.UserID;
                        certificate.Muser = SessionWrapper.UserID;
                    }
                    if (!SessionWrapper.IsAdmin)
                    {
                        certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    certificate.Cdate = DateTime.UtcNow;
                    certificate.Mdate = DateTime.UtcNow;

                    if (certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                    {

                        certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
                    }
                    else
                    {

                        certificate.ExpDate = certificate.InstallDate.AddDays(-1).AddYears(1);
                    }

                    certificate.E_Stat = true;

                    Certificate cr = db.certificate_mst.Find(certificate.VehRegistrationNo);
                    if (certificate.CalibrationDate != null)
                    {
                        certificate.CalibrationDate = certificate.CalibrationDate;
                    }

                    //Certificate cr = new Certificate();
                    //cr.CertificateNo = cerr.CertificateNo;
                    if (ModelState.IsValid)
                    {
                        cr.SerialNo = certificate.SerialNo;
                        cr.TamperSealNo = certificate.TamperSealNo;
                        cr.TechnicianName = certificate.TechnicianName;
                        cr.VehRegistrationNo = certificate.VehRegistrationNo;
                        cr.Mdate = Convert.ToDateTime(cr.Mdate);
                        cr.Muser = certificate.Muser;
                        cr.ChassisNo = certificate.ChassisNo;
                        cr.Country = certificate.Country;
                        cr.ApplicableStandard = certificate.ApplicableStandard;
                        cr.EngineNo = certificate.EngineNo;
                        cr.Cdate = Convert.ToDateTime(certificate.Cdate);
                        cr.ExpDate = Convert.ToDateTime(certificate.ExpDate);
                        cr.InstallDate = certificate.InstallDate;
                        cr.MakeOfVehicle = certificate.MakeOfVehicle;
                        cr.ModelOfVehicle = certificate.ModelOfVehicle;
                        cr.OwnerName = certificate.OwnerName;
                        cr.SetSpeed = certificate.SetSpeed;
                        cr.speed2 = certificate.speed2;
                        cr.ManufactureYear = certificate.ManufactureYear;
                        cr.TypeSpeedLimiter = certificate.TypeSpeedLimiter;
                        cr.E_Stat = certificate.E_Stat;
                        cr.Duplicate = 0;


                        db.Entry(cr).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

                        Country con = db.countries.Find(certificate.Country);
                        CertificateView objcertificateview = new CertificateView
                        {
                            CertificateNo = certificate.CertificateNo,
                            VehRegistrationNo = certificate.VehRegistrationNo,
                            OwnerName = (certificate.OwnerName).ToUpper(),
                            ChassisNo = certificate.ChassisNo,
                            EngineNo = certificate.EngineNo,
                            ManufactureYear = certificate.ManufactureYear,
                            MakeOfVehicle = certificate.MakeOfVehicle,
                            ModelOfVehicle = certificate.ModelOfVehicle,
                            TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                            SetSpeed = certificate.SetSpeed,
                            speed2 = certificate.speed2,
                            SerialNo = certificate.SerialNo,
                            TamperSealNo = certificate.TamperSealNo,
                            InstallDate = certificate.InstallDate,
                            ExpDate = certificate.ExpDate,
                            TechnicianName = certificate.TechnicianName,
                            ApplicableStandard = certificate.ApplicableStandard,
                            Country = certificate.Country,
                            Cdate = certificate.Cdate,
                            Cuser = certificate.Cuser,
                            Mdate = certificate.Mdate,
                            Muser = certificate.Muser,
                            E_Stat = certificate.E_Stat,
                            Duplicate = certificate.Duplicate,
                            DealerId = certificate.DealerId,

                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                            MobileNo = comp.PhoneNo.Trim() != comp.MobileNo.Trim() ? comp.MobileNo : null,
                            CountryName = con.CountryName,
                            AdminName = cv.AdminName,
                            AdminAddress = cv.AdminAddress,
                            AdminPhone = cv.AdminPhone,
                            AdminEmail = cv.AdminEmail,
                            CalibrationDate = certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };
                        FillImageUrl(objcertificateview, cv.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", cv.AdminTemplate, objcertificateview);
                    }

                    dropdown();
                    CompanyName();
                    return View(certificate);
                }

                else
                {
                    if (certificate.VehRegistrationNo != null)
                    {

                        // if (db.certificate_mst.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 1)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");

                        // }
                        //else


                        if (Exist_exp.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 0)
                        {
                            ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        }

                        //if (db.certificate_mst.SingleOrDefault(a => a.VehRegistrationNo == certificate.VehRegistrationNo).E_Stat == true)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        // }

                    }
                    else
                    {
                        certificate.VehRegistrationNo = "";
                    }
                    if (certificate.ChassisNo != null)
                    {

                        if (Exist_exp.Where(c => c.ChassisNo == certificate.ChassisNo).Count() > 0)
                        {
                            ModelState.AddModelError("ChassisNo", "ChassisNo already Exists");
                        }

                    }
                    if (certificate.EngineNo != null)
                    {
                        if (Exist_exp.Where(e => e.EngineNo == certificate.EngineNo).Count() > 0)
                        {
                            ModelState.AddModelError("EngineNo", "EngineNo already Exists");
                        }
                    }
                    if (certificate.ApplicableStandard != null)
                    {
                        if (certificate.ApplicableStandard == "KS 2295-2011")
                        {
                            certificate.SerialNo = string.Format("{0}{1}", "TK/S2/", certificate.SerialNo);
                        }
                    }

                    var Exist_serial = from a in db.SpeedLimiterSerialNo where a.E_Stat == false select a;
                    var Exist_serialNo = from a in db.SpeedLimiterSerialNo where a.E_Stat == true select a;
                    //var Exist_tampseal = from a in db.TamperSeal where a.E_Stat == false select a;
                    // var Exist_tampsealNo = from a in db.TamperSeal where a.E_Stat == true select a;
                    if (certificate.SerialNo != null)
                    {

                        if (Exist_serial.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                            }


                        }
                        else if (Exist_serialNo.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already created ");
                            }

                            // var z = db.certificate_mst.FirstOrDefault(a => a.SerialNo == certificate.SerialNo);
                            // if (z.ChassisNo != certificate.ChassisNo)
                            // {
                            //   ModelState.AddModelError("SerialNo", "SerialNo already created with another device");

                            //  }
                            //if (db.certificate_mst.Where(l=>l.CertificateNo==certificate.CertificateNo).Count()>0)
                            //{
                            //    ModelState.AddModelError("SerialNo", "SerialNo already created with another device");
                            //}
                            //if()
                        }
                        else
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo does not exist in the available list");
                        }


                    }
                    //if (certificate.TamperSealNo != null )
                    //{
                    //    //if (Exist_tampseal.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{

                    //        if (Exist_exp.Where(t => t.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //        {
                    //            ModelState.AddModelError("TamperSealNo", "TamperSealNo already Exists");
                    //        }


                    //    //}
                    //    //else if (Exist_tampsealNo.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo already created");
                    //    //}
                    //    //else
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo doesnot Exists");
                    //    //}



                    //}


                    string certnum = (Convert.ToInt32(db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();
                    if (certificate.EngineNo != null)
                    {
                        certificate.CertificateNo = string.Format("{0}-{1}-{2}", certificate.Country, GetLast(certificate.EngineNo, 6), certnum.PadLeft(4, '0'));
                    }
                    CertificateView ce = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        certificate.Cuser = SessionWrapper.AdminID;
                        certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                        certificate.Cuser = SessionWrapper.UserID;
                        certificate.Muser = SessionWrapper.UserID;
                    }

                    if (!SessionWrapper.IsAdmin)
                    {
                        certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    certificate.Cdate = DateTime.UtcNow;
                    certificate.Mdate = DateTime.UtcNow;

                    //if (certificate.InstallDate.AddYears(1).Date <= EntityFunctions.AddDays(DateTime.UtcNow.Date,30))
                    //{

                    //    certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
                    //}
                    //else
                    //{

                    certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
                    //}

                    certificate.E_Stat = true;

                    if (ModelState.IsValid)
                    {
                        if (cert != null)
                        {
                            cert.E_Stat = false;
                            db.Entry(cert).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        db.certificate_mst.Add(certificate);
                        db.SaveChanges();
                        SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                        spd.E_Stat = true;
                        db.Entry(spd).State = EntityState.Modified;
                        //TamperSeal tamp = db.TamperSeal.SingleOrDefault(a => a.TamperSealNo == certificate.TamperSealNo);
                        //tamp.E_Stat = true;
                        //db.Entry(tamp).State = EntityState.Modified;
                        CommonFlag comflag = db.CommonFlag.Find(1);
                        comflag.Value = certnum;
                        db.Entry(comflag).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

                        Country con = db.countries.Find(certificate.Country);
                        CertificateView objcertificateview = new CertificateView
                        {
                            CertificateNo = certificate.CertificateNo,
                            VehRegistrationNo = certificate.VehRegistrationNo,
                            OwnerName = (certificate.OwnerName).ToUpper(),
                            ChassisNo = certificate.ChassisNo,
                            EngineNo = certificate.EngineNo,
                            ManufactureYear = certificate.ManufactureYear,
                            MakeOfVehicle = certificate.MakeOfVehicle,
                            ModelOfVehicle = certificate.ModelOfVehicle,
                            TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                            SetSpeed = certificate.SetSpeed,
                            speed2 = certificate.speed2,
                            SerialNo = certificate.SerialNo,
                            TamperSealNo = certificate.TamperSealNo,
                            InstallDate = certificate.InstallDate,
                            ExpDate = certificate.ExpDate,
                            TechnicianName = certificate.TechnicianName,
                            ApplicableStandard = certificate.ApplicableStandard,
                            Country = certificate.Country,
                            Cdate = certificate.Cdate,
                            Cuser = certificate.Cuser,
                            Mdate = certificate.Mdate,
                            Muser = certificate.Muser,
                            E_Stat = certificate.E_Stat,
                            Duplicate = certificate.Duplicate,
                            DealerId = certificate.DealerId,
                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                            MobileNo = comp.PhoneNo.Trim() != comp.MobileNo.Trim() ? comp.MobileNo : null,
                            CountryName = con.CountryName,
                            AdminName = ce.AdminName,
                            AdminAddress = ce.AdminAddress,
                            AdminPhone = ce.AdminPhone,
                            AdminEmail = ce.AdminEmail,
                            CalibrationDate = certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };

                        FillImageUrl(objcertificateview, ce.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", ce.AdminTemplate, objcertificateview);

                    }


                }

                dropdown();
                CompanyName();
                return View(certificate);
            }
        }
        //[ValidateAntiForgeryToken, Authorize, HttpPost, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        //public ActionResult Calibrate(Certificate certificate)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        //var Exist_exp = from a in db.certificate_mst where a.ExpDate >= DateTime.UtcNow select a;
        //        var Exist_Estat = from a in db.certificate_mst where a.E_Stat == true select a;
        //        Certificate cert = Exist_Estat.SingleOrDefault(c => c.VehRegistrationNo == certificate.VehRegistrationNo);


        //        ////certificate.VehRegistrationNo = certificate.VehRegistrationNo;

        //        string certnum = (Convert.ToInt32(db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();

        //        certificate.CertificateNo = string.Format("{0}-{1}-{2}", certificate.Country, certificate.VehRegistrationNo, certnum.PadLeft(4, '0'));

        //        //certificate.Cuser = (int)SessionWrapper.AdminID;
        //        //certificate.Muser = (int)SessionWrapper.AdminID;
        //        CertificateView ce = new CertificateView();
        //        if (SessionWrapper.IsAdmin)
        //        {
        //            certificate.Cuser = SessionWrapper.AdminID;
        //            certificate.Muser = SessionWrapper.AdminID;
        //            AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
        //            Template temp = db.Template.Find(adminmaster.TemplateID);
        //            ce.AdminID = adminmaster.AdminID;
        //            ce.AdminName = adminmaster.DisplayName.ToUpper();
        //            ce.AdminAddress = adminmaster.Address;
        //            ce.AdminLogo = adminmaster.Logo;
        //            ce.AdminPhone = adminmaster.PhoneNo;
        //            ce.AdminEmail = adminmaster.Email;
        //            ce.AdminTemplate = temp.ViewName;
        //        }
        //        else
        //        {
        //            var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
        //            AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
        //            Template temp = db.Template.Find(adminmaster.TemplateID);
        //            ce.AdminID = adminmaster.AdminID;
        //            ce.AdminName = adminmaster.DisplayName.ToUpper();
        //            ce.AdminAddress = adminmaster.Address;
        //            ce.AdminPhone = adminmaster.PhoneNo;
        //            ce.AdminLogo = adminmaster.Logo;
        //            ce.AdminEmail = adminmaster.Email;
        //            ce.AdminTemplate = temp.ViewName;
        //            certificate.Cuser = SessionWrapper.UserID;
        //            certificate.Muser = SessionWrapper.UserID;
        //        }          

        //        //if (!SessionWrapper.IsAdmin)
        //        //{
        //        //    certificate.DealerId = SessionWrapper.CompanyID;
        //        //}

        //        certificate.Cdate = DateTime.UtcNow.Date;
        //        certificate.Mdate = DateTime.UtcNow.Date;
        //        if (certificate.InstallDate.AddYears(1) < DateTime.UtcNow)
        //        {

        //            certificate.ExpDate = certificate.Cdate.AddDays(-1).AddYears(1);
        //        }
        //        else
        //        {

        //            certificate.ExpDate = certificate.InstallDate.Date.AddDays(-1).AddYears(1);
        //        }

        //        certificate.E_Stat = true;

        //            if (ModelState.IsValid)
        //            {
        //                certificate.SerialNo = cert.SerialNo;
        //                certificate.TamperSealNo = cert.TamperSealNo;
        //                certificate.EngineNo = cert.EngineNo;
        //                certificate.ChassisNo = cert.ChassisNo;
        //                certificate.MakeOfVehicle = cert.MakeOfVehicle;
        //                certificate.ManufactureYear = cert.ManufactureYear;
        //                certificate.ModelOfVehicle = cert.ModelOfVehicle;
        //                certificate.DealerId = cert.DealerId;
        //                certificate.Country = cert.Country;
        //                if (cert != null)
        //                {
        //                    cert.E_Stat = false;
        //                    db.Entry(cert).State = EntityState.Modified;
        //                    db.SaveChanges();
        //                }
        //                db.certificate_mst.Add(certificate);
        //                db.SaveChanges();

        //                CommonFlag comflag = db.CommonFlag.Find(1);
        //                comflag.Value = certnum;
        //                db.Entry(comflag).State = EntityState.Modified;
        //                db.SaveChanges();

        //                CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

        //                Country con = db.countries.Find(certificate.Country);
        //                CertificateView objcertificateview = new CertificateView
        //                {
        //                    CertificateNo = certificate.CertificateNo,
        //                    VehRegistrationNo = certificate.VehRegistrationNo,
        //                    OwnerName = certificate.OwnerName,
        //                    ChassisNo = certificate.ChassisNo,
        //                    EngineNo = certificate.EngineNo,
        //                    ManufactureYear = certificate.ManufactureYear,
        //                    MakeOfVehicle = certificate.MakeOfVehicle,
        //                    ModelOfVehicle = certificate.ModelOfVehicle,
        //                    TypeSpeedLimiter = certificate.TypeSpeedLimiter,
        //                    SetSpeed = certificate.SetSpeed,
        //                    speed2=certificate.speed2,
        //                    SerialNo = certificate.SerialNo,
        //                    TamperSealNo = certificate.TamperSealNo,
        //                    InstallDate = certificate.InstallDate,
        //                    ExpDate = certificate.ExpDate,
        //                    TechnicianName = certificate.TechnicianName,
        //                    ApplicableStandard = certificate.ApplicableStandard,
        //                    Country = certificate.Country,
        //                    Cdate = certificate.Cdate,
        //                    Cuser = certificate.Cuser,
        //                    Mdate = certificate.Mdate,
        //                    Muser = certificate.Muser,
        //                    E_Stat = certificate.E_Stat,
        //                    Duplicate = certificate.Duplicate,
        //                    DealerId = certificate.DealerId,
        //                    CompID = comp.CompID,
        //                    Name = comp.DisplayName,
        //                    Address = comp.Address,
        //                    PhoneNo = comp.PhoneNo,
        //                    CountryName = con.CountryName,
        //                    AdminName = ce.AdminName.ToUpper(),
        //                    AdminAddress = ce.AdminAddress,
        //                    AdminPhone = ce.AdminPhone,
        //                    AdminEmail = ce.AdminEmail
        //                    //valid=Convert.ToString(certificate.E_Stat)
        //                };

        //                FillImageUrl(objcertificateview, ce.AdminLogo);
        //                FillLogo(objcertificateview, comp.Logo);
        //                return this.ViewPdf("Certificate", ce.AdminTemplate, objcertificateview);

        //            }


        //    }

        //    dropdown();
        //    CompanyName();
        //    registrationdropdown();
        //    return View(certificate);
        //}

        private void FillImageUrl(CertificateView Certificate, string imageName)
        {
            //if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            //{
            //    string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            //    Certificate.ImageUrl = url + imageName;
            //}
            //else
            //{
            string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            Certificate.ImageUrl = url + imageName;
            //}
        }
        private void FillLogo(CertificateView Certificate, string imageName)
        {
            //if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            //{
            string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            Certificate.DealerLogo = url + imageName;
            //}
            //else
            //{
            //    string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            //    Certificate.DealerLogo = url + "Content/" + "Untitled.jpg";

            //}
        }


        /// <summary>
        ///   Getting the Engine No last 6 digits to generate certificateno
        /// </summary>
        /// <param name="source"></param>
        /// <param name="tail_length"></param>
        /// <returns></returns>
        [NonAction]
        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }
        //private void FillImage(CertificateView Certificate, string imageName1, string imageName2, string imageName3, string imageName4, string imageName5, string imageName6, string imageName7, string imageName8, string imageName9, string imageName10, string imageName11, string imageName12, string imageName13, string imageName14, string imageName15, string imageName16,string imageName17,string imageName18)
        //{

        //    string url = string.Format("{0}://{1}{2}{3}/{4}/", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"),"Images","Arabic Images");
        //    Certificate.SpeedLimiter_text = url + imageName1;
        //    Certificate.Orient_text = url + imageName2;
        //    Certificate.OwnerName_text = url + imageName3;
        //    Certificate.ResitrationNumber_text = url + imageName4;
        //    Certificate.ChassisNo_text = url + imageName5;
        //    Certificate.EngineNo_text = url + imageName6;
        //    Certificate.Modelofvehicle_text = url + imageName7;
        //    Certificate.yearofmanufacture_text = url + imageName8;

        //    Certificate.typeofspeedlimiter_text = url + imageName9;
        //    Certificate.SerialNo_text = url + imageName10;
        //    Certificate.DateofInstallation_text = url + imageName11;
        //    Certificate.Setspeedlimit_text = url + imageName12;
        //    Certificate.TechnicianName_text = url + imageName13;
        //    Certificate.DealerName_text = url + imageName14;
        //    Certificate.AddressPhone_text = url + imageName15;
        //    Certificate.ValidUpto_text = url + imageName16;
        //    Certificate.ThreeYear_text = url + imageName17;
        //    Certificate.DealerStamp_text = url + imageName18;

        //    //}
        //} 


        /// <summary>
        /// // Reissuing the certificate those who didnt gave 
        /// vehicle registration number at the time of creation 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ReIssue(string id = null)
        {
            using (DatContext db = new DatContext())
            {
                Certificate certificate = db.certificate_mst.Find(id);
                certificate.InstallDate = certificate.InstallDate;
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                dropdown();
                CompanyName();
                //ModelState.Clear();
                return PartialView(certificate);

            }

        }


        [ValidateAntiForgeryToken, Audit, Authorize, HttpPost]
        public ActionResult ReIssue(Certificate certificate)
        {
            using (DatContext db = new DatContext())
            {

                Certificate cer = db.certificate_mst.Find(certificate.CertificateNo);

                CertificateLog cerlog = new CertificateLog();
                cerlog.CertificateNo = cer.CertificateNo;
                cerlog.SerialNo = cer.SerialNo;
                cerlog.TamperSealNo = cer.TamperSealNo;
                cerlog.TechnicianName = cer.TechnicianName;
                cerlog.VehRegistrationNo = cer.VehRegistrationNo;
                cerlog.Mdate = Convert.ToDateTime(cer.Mdate);
                cerlog.Muser = cer.Muser;
                cerlog.ChassisNo = cer.ChassisNo;
                cerlog.Country = cer.Country;
                cerlog.ApplicableStandard = cer.ApplicableStandard;
                cerlog.EngineNo = cer.EngineNo;
                cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                cerlog.InstallDate = cer.InstallDate;
                cerlog.MakeOfVehicle = cer.MakeOfVehicle;
                cerlog.ModelOfVehicle = cer.ModelOfVehicle;
                cerlog.OwnerName = cer.OwnerName;
                cerlog.SetSpeed = cer.SetSpeed;
                cerlog.speed2 = cer.speed2;
                cerlog.ManufactureYear = cer.ManufactureYear;
                cerlog.TypeSpeedLimiter = cer.TypeSpeedLimiter;
                cerlog.E_Stat = cer.E_Stat;
                cerlog.Duplicate = cer.Duplicate;


                //cer.Cuser = SessionWrapper.AdminID;
                cer.Muser = SessionWrapper.AdminID;



                // cer.Cdate = DateTime.Now.Date;
                cer.Mdate = DateTime.UtcNow.Date;



                cer.E_Stat = true;
                var Exist_exp = from a in db.certificate_mst where a.ExpDate > DateTime.UtcNow select a;

                cer.VehRegistrationNo = certificate.VehRegistrationNo;
                if (cer.VehRegistrationNo == null)
                {
                    ModelState.AddModelError("VehRegistrationNo", "Vehicle Registration number required");
                }
                else if (cer.VehRegistrationNo != cerlog.VehRegistrationNo)
                {

                    if (Exist_exp.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 0)
                    {
                        ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                    }
                }
                //cer.OwnerName = certificate.OwnerName;
                //cer.ChassisNo = certificate.ChassisNo;
                //cer.EngineNo = certificate.EngineNo;
                //cer.ManufactureYear = certificate.ManufactureYear;
                //cer.MakeOfVehicle = certificate.MakeOfVehicle;
                //cer.ModelOfVehicle = certificate.ModelOfVehicle;
                //cer.SetSpeed = certificate.SetSpeed;
                //cer.TypeSpeedLimiter = certificate.TypeSpeedLimiter;
                //cer.SerialNo = certificate.SerialNo;
                //cer.TamperSealNo = certificate.TamperSealNo;
                //cer.TechnicianName = cer.TechnicianName;
                //cer.DealerId = certificate.DealerId;
                //cer.Duplicate = 0;
                //cer.Country = certificate.Country;

                //cer.ApplicableStandard = certificate.ApplicableStandard;
                //cer.InstallDate = certificate.InstallDate.Date;
                //if (cer.InstallDate.AddYears(1).Date < DateTime.Now.Date)
                //{

                //    cer.ExpDate = cer.Cdate.AddDays(-1).AddYears(1);
                //}
                //else
                //{

                //    cer.ExpDate = cer.InstallDate.AddDays(-1).AddYears(1);
                //}
                if (ModelState.IsValid)
                {

                    db.Entry(cer).State = EntityState.Modified;
                    db.SaveChanges();
                    db.certificate_log.Add(cerlog);
                    db.SaveChanges();
                    //SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                    //spd.E_Stat = true;
                    //db.Entry(spd).State = EntityState.Modified;
                    //db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            dropdown();
            CompanyName();
            return View(certificate);
        }
        //public ActionResult GetPdf(string filename)
        //{
        //    return File(filename, "application/pdf", Server.UrlEncode(filename));
        //}


        /// <summary>
        /// Getting the list of Admin Names in the dropdown
        /// </summary>
        [NonAction]
        private void AdminName()
        {
            using (DatContext db = new DatContext())
            {

                List<AdminMaster> admn = (from a in db.AdminMst where a.E_Stat == true select a).ToList();
                ViewBag.Admin = new SelectList(admn, "adminid", "Name");

            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="provinceId"></param>
        /// <returns></returns>
        public ActionResult AdmnCompNames(int provinceId)
        {
            DatContext db = new DatContext();
            var value = from admn in db.AdminMst
                        join comp in db.CompanyMst

                        on admn.AdminID equals comp.AdminID
                        //where admn.AdminID == SessionWrapper.AdminID
                        where admn.AdminID == provinceId
                        //&& dept.DeptID != SessionWrapper.DeptID
                        select new { comp.CompID, comp.Name };
            var comNames = new SelectList(value, "CompID", "Name");
            return Json(comNames, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Getting the list of company names in the dropdown
        /// </summary>
        [NonAction]
        private void CompanyName()
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    ViewBag.Dealerdd = new SelectList("", "CompID", "Name");
                    List<AdminMaster> admn = (from a in db.AdminMst where a.E_Stat == true select a).ToList();

                    var admin = from a in db.AdminMst where a.E_Stat == true select new { a.AdminID, a.Name };
                    ViewBag.AdminID = new SelectList(admn, "AdminID", "Name", "--Select company--");


                    List<CompanyMaster> Company = (from a in db.CompanyMst where a.E_Stat == true select a).ToList();
                    ViewBag.DealerId = new SelectList(Company, "CompID", "Name");

                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    List<CompanyMaster> Company = db.CompanyMst.ToList();
                    var comp = from compnay in Company where compnay.AdminID == SessionWrapper.AdminID && compnay.E_Stat == true select new { compnay.CompID, compnay.Name };
                    ViewBag.Dealerdd = new SelectList(comp, "CompID", "Name");
                    ViewBag.DealerId = new SelectList(comp, "CompID", "Name");
                    //from comp in db.CompanyMst where comp.AdminID == SessionWrapper.AdminID select new { comp.CompID, comp.Name };
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    List<CompanyMaster> Company = db.CompanyMst.ToList();
                    var cmp = db.InspectionMst.Find(SessionWrapper.CompanyID).AdminID;
                    var comp = from compnay in Company where compnay.AdminID == cmp && compnay.E_Stat == true select new { compnay.CompID, compnay.Name };
                    ViewBag.Dealerdd = new SelectList(comp, "CompID", "Name");


                }


            }
        }
        /// <summary>
        /// dropdown list for country,applicablestandard,type of speed limiter
        /// </summary>
        [NonAction]
        private void dropdown()
        {
            using (DatContext db = new DatContext())
            {
                List<Country> country = db.countries.ToList();
                ViewBag.country = new SelectList(country, "CountryCode", "CountryName");
            }
            var app = new[]
            {
                new SelectListItem { Text="AIS-018",Value ="AIS-018" },
                new SelectListItem { Text = "BS-217", Value = "BS-217" },
                new SelectListItem { Text = "EEC-92/24", Value = "EEC-92/24" },
                new SelectListItem { Text = "GSO-1626/2002", Value = "GSO-1626/2002" },
                new SelectListItem {Text="KS 2295-2011", Value="KS 2295-2011"},
                new SelectListItem {Text="NIS 755-1:2014", Value="NIS 755-1:2014"},
                 new SelectListItem {Text="RSB-STANDARDS", Value="RSB-STANDARDS"}

            };
            ViewBag.AppStd = new SelectList(app, "Text", "Value", "-select-");

            var typespeed = new[]
            {
                new SelectListItem{Text="Cable Control",Value="Cable Control"},
                new SelectListItem{Text="Fuel Control",Value="Fuel Control"},
                new SelectListItem{Text="Electronic Pedal",Value="Electronic Pedal"},
            };
            ViewBag.SpeedLimiterType = new SelectList(typespeed, "Text", "Value");



        }
        /// <summary>
        /// Renewal Vehicle registration numbers dropdown
        /// </summary>

        //////public JsonResult ent()
        //////{
        //////    using (DatContext db = new DatContext())
        //////    {
        //////        var result = from certi in db.certificate_mst
        //////                     where
        //////                         EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
        //////                         &&
        //////                         certi.E_Stat == true
        //////                     select certi.VehRegistrationNo;

        //////        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
        //////    }
        //////}

        private void registrationdropdown()
        {
            List<string> res;
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var result = from certi in db.certificate_mst
                                 where
                                     EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                       && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                     &&
                                     certi.E_Stat == true
                                 select certi.VehRegistrationNo;
                    res = result.ToList();
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var result = from certi in db.certificate_mst
                                 join com in db.CompanyMst on certi.DealerId equals com.CompID
                                 where
                                 com.AdminID == SessionWrapper.AdminID &&
                                     EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                         && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                     &&
                                     certi.E_Stat == true
                                 select certi.VehRegistrationNo;
                    res = result.ToList();
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                    var result = from certi in db.certificate_mst
                                 join com in db.CompanyMst on certi.DealerId equals com.CompID
                                 where
                                 com.CompID == SessionWrapper.CompanyID &&
                                     EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                        && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                     &&
                                     certi.E_Stat == true
                                 select certi.VehRegistrationNo;
                    res = result.ToList();
                }
                else
                {
                    var result = from certi in db.certificate_mst
                                 where
                                 certi.Cuser == SessionWrapper.UserID &&
                                     EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                      && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                     &&
                                     certi.E_Stat == true
                                 select certi.VehRegistrationNo;
                    res = result.ToList();
                }
            }

            ViewBag.VehRegistrationNo = new SelectList(res);
        }

        /// <summary>
        /// For Generating the Certificate Number
        /// </summary>Not using now
        /// <param name="certificate"></param>
        /// <param name="Uniquecode"></param>
        /// <returns></returns>
        [NonAction]
        private string GenerateCertificateNo(Certificate certificate)
        {

            using (DatContext db = new DatContext())
            {
                return string.Format("{0}-{1}-{2}", certificate.Country, certificate.VehRegistrationNo, db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value.PadLeft(4, '0'));
            }
        }



        /// <summary>
        /// Certificate Generation duplicate print 
        /// </summary>
        /// <param name="CertificateNo"></param>
        /// <returns></returns>

        [Authorize, Audit, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Generate(string id)
        {
            using (DatContext db = new DatContext())
            {


                //Certificate certificate = db.certificate_mst.Find(id);
                //Country con = db.countries.Find(certificate.Country);
                //CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);
                //Certificate certificate = result.SingleOrDefault(a => a.CertificateNo ==certificate.CertificateNo);
                //certificate.Duplicate = (Convert.ToInt32(db.certificate_mst.SingleOrDefault(a => a.CertificateNo == certificate.CertificateNo).Duplicate) + 1);
                var result = from certificate in db.certificate_mst
                             join con in db.countries
                             on certificate.Country equals con.CountryCode
                             join comp in db.CompanyMst on
                             certificate.DealerId equals comp.CompID
                             join admn in db.AdminMst on
                             comp.AdminID equals admn.AdminID
                             join temp in db.Template on
                             admn.TemplateID equals temp.ID
                             where certificate.CertificateNo == id
                             select
              new CertificateView
              {
                  CertificateNo = certificate.CertificateNo,
                  VehRegistrationNo = certificate.VehRegistrationNo,
                  OwnerName = certificate.OwnerName.ToUpper(),
                  ChassisNo = certificate.ChassisNo,
                  EngineNo = certificate.EngineNo,
                  ManufactureYear = certificate.ManufactureYear,
                  MakeOfVehicle = certificate.MakeOfVehicle,
                  ModelOfVehicle = certificate.ModelOfVehicle,
                  TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                  SetSpeed = certificate.SetSpeed,
                  speed2 = certificate.speed2,
                  SerialNo = certificate.SerialNo,
                  TamperSealNo = certificate.TamperSealNo,
                  InstallDate = certificate.InstallDate,
                  ExpDate = certificate.ExpDate,
                  TechnicianName = certificate.TechnicianName,
                  ApplicableStandard = certificate.ApplicableStandard,
                  Country = certificate.Country,
                  Cdate = certificate.Cdate,
                  Cuser = certificate.Cuser,
                  Mdate = certificate.Mdate,
                  Muser = certificate.Muser,
                  E_Stat = certificate.E_Stat,
                  Duplicate = certificate.Duplicate,
                  DealerId = certificate.DealerId,
                  CompID = comp.CompID,
                  Name = comp.Name,
                  Address = comp.Address,
                  PhoneNo = comp.PhoneNo,
                  MobileNo = comp.PhoneNo.Trim() != comp.MobileNo.Trim() ? comp.MobileNo : null,
                  CountryName = con.CountryName,
                  DealerLogo = comp.Logo,
                  AdminName = admn.DisplayName.ToUpper(),
                  AdminAddress = admn.Address,
                  AdminPhone = admn.PhoneNo,
                  AdminEmail = admn.Email,
                  AdminLogo = admn.Logo,
                  AdminTemplate = temp.ViewName,
                  CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate
              };
                Certificate cert = db.certificate_mst.Find(id);
                cert.Duplicate = (Convert.ToInt32(cert.Duplicate + 1));
                db.Entry(cert).State = EntityState.Modified;
                db.SaveChanges();
                CertificateView objcertificateview = result.SingleOrDefault(a => a.CertificateNo == id);
                FillImageUrl(objcertificateview, objcertificateview.AdminLogo);
                FillLogo(objcertificateview, objcertificateview.DealerLogo);
                return this.ViewPdf("Certificate", objcertificateview.AdminTemplate, objcertificateview);
            }
        }
        /// <summary>
        /// Getting the expired registration numbers for Renewal 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetExpiredRegistrationNumber()
        {
            try
            {
                using (DatContext db = new DatContext())
                {
                    List<string> res;

                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var result = from certi in db.certificate_mst
                                     where
                                         EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                          && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                         &&
                                         certi.E_Stat == true
                                     select certi.VehRegistrationNo;
                        res = result.ToList();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var result = from certi in db.certificate_mst
                                     join com in db.CompanyMst on certi.DealerId equals com.CompID
                                     where
                                     com.AdminID == SessionWrapper.AdminID &&
                                         EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                         && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                         &&
                                         certi.E_Stat == true
                                     select certi.VehRegistrationNo;
                        res = result.ToList();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    {
                        var result = from certi in db.certificate_mst
                                     join com in db.CompanyMst on certi.DealerId equals com.CompID
                                     where
                                     com.CompID == SessionWrapper.CompanyID &&
                                         EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                          && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                         &&
                                         certi.E_Stat == true
                                     select certi.VehRegistrationNo;
                        res = result.ToList();
                    }
                    else
                    {
                        var result = from certi in db.certificate_mst
                                     where
                                     certi.Cuser == SessionWrapper.UserID &&
                                         EntityFunctions.DiffDays(certi.ExpDate, DateTime.UtcNow) > -30
                                           && !certi.VehRegistrationNo.Contains(ForRegnValue)
                                         &&
                                         certi.E_Stat == true
                                     select certi.VehRegistrationNo;
                        res = result.ToList();
                    }
                    var resi = res.Distinct().ToList();



                    return Json(resi, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetRegistrationNumber()
        {
            try
            {
                using (DatContext db = new DatContext())
                {


                    var res = db.certificate_mst.Select(u => u.VehRegistrationNo).Distinct().ToList();



                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Certificate details
        /// </summary> Not using now
        /// <param name="RegNo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult CertificateDetails(string RegNo = null)
        {
            try
            {

                using (DatContext db = new DatContext())
                {
                    if (!string.IsNullOrEmpty(RegNo))
                    {


                        var result = from certificate in db.certificate_mst
                                     join country in db.countries
                                     on certificate.Country equals country.CountryCode
                                     join company in db.CompanyMst on
                                     certificate.DealerId equals company.CompID
                                     where certificate.E_Stat == true && certificate.CertificateNo.Trim() == RegNo.Trim()
                                     select new CertificateView
                                     {
                                         CertificateNo = certificate.CertificateNo,
                                         VehRegistrationNo = certificate.VehRegistrationNo,
                                         OwnerName = certificate.OwnerName,
                                         ChassisNo = certificate.ChassisNo,
                                         EngineNo = certificate.EngineNo,
                                         ManufactureYear = certificate.ManufactureYear,
                                         MakeOfVehicle = certificate.MakeOfVehicle,
                                         ModelOfVehicle = certificate.ModelOfVehicle,
                                         TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                                         SetSpeed = certificate.SetSpeed,
                                         speed2 = certificate.speed2,
                                         SerialNo = certificate.SerialNo,
                                         TamperSealNo = certificate.TamperSealNo,
                                         InstallDate = certificate.InstallDate,
                                         ExpDate = certificate.ExpDate,
                                         TechnicianName = certificate.TechnicianName,
                                         ApplicableStandard = certificate.ApplicableStandard,
                                         Country = certificate.Country,
                                         Cdate = certificate.Cdate,
                                         Cuser = certificate.Cuser,
                                         Mdate = certificate.Mdate,
                                         Muser = certificate.Muser,
                                         E_Stat = certificate.E_Stat,
                                         Duplicate = certificate.Duplicate,
                                         DealerId = certificate.DealerId,
                                         CompID = company.CompID,
                                         Name = company.Name,
                                         Address = company.Address,
                                         PhoneNo = company.PhoneNo,
                                         CountryName = country.CountryName,
                                         Expired = EntityFunctions.DiffDays(certificate.ExpDate, DateTime.UtcNow) > 0 ? "Expired" : "Valid"

                                     };
                        //CertificateView res = result.SingleOrDefault(a => a.VehRegistrationNo == RegNo);

                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("not found", JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

        }


        public JsonResult CheckSerialNo(string ser)
        {
            using (DatContext db = new DatContext())
            {
                if (ser != null)
                {
                    if (db.SpeedLimiterSerialNo.Where(a => a.SerialNo == ser).Count() > 0)
                    {
                        // return Content("Available", "text/plain");
                        //TempData["serialno"] = "Genuine";
                        //TempData["serialnostatus"] = "Available";

                        var res = from a in db.SpeedLimiterSerialNo
                                  join b in db.AdminMst on
                                     a.DealerId equals b.AdminID
                                  where a.SerialNo == ser
                                  select new
                                  {
                                      dealer = b.DisplayName,
                                      date = a.Date,
                                      status = a.E_Stat == false ? "Availble" : "Not Available",
                                      genuine = true
                                  };
                        return Json(res.ToList(), JsonRequestBehavior.AllowGet);
                    }
                    //else if (db.SpeedLimiterSerialNo.Where(a => (a.SerialNo == ser) && (a.E_Stat == false)).Count() > 0)
                    //{
                    //    TempData["serialno"] = "Genuine";
                    //    TempData["serialnostatus"] = "Not Available";
                    //}
                    else
                    {
                        var res = new { genuine = false };
                        return Json(res, JsonRequestBehavior.AllowGet);


                    }
                   
                }
                else
                {
                    return null;

                }
            }


        }

        /// <summary>
        /// getting certificate details for  Search and Renewal
        /// </summary>
        /// <param name="RegNo"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetCertificateDetails(string RegNo = null)
        {
            try
            {

                using (DatContext db = new DatContext())
                {
                    if (!string.IsNullOrEmpty(RegNo))
                    {


                        var result = from certificate in db.certificate_mst
                                     join country in db.countries
                                     on certificate.Country equals country.CountryCode
                                     join company in db.CompanyMst on
                                     certificate.DealerId equals company.CompID
                                     where certificate.E_Stat == true && certificate.VehRegistrationNo.Trim().Replace(" ", "") == RegNo.Trim().Replace(" ", "")
                                     select new CertificateView
                                     {
                                         CertificateNo = certificate.CertificateNo,
                                         VehRegistrationNo = certificate.VehRegistrationNo,
                                         OwnerName = certificate.OwnerName,
                                         ChassisNo = certificate.ChassisNo,
                                         EngineNo = certificate.EngineNo,
                                         ManufactureYear = certificate.ManufactureYear,
                                         MakeOfVehicle = certificate.MakeOfVehicle,
                                         ModelOfVehicle = certificate.ModelOfVehicle,
                                         TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                                         SetSpeed = certificate.SetSpeed,
                                         speed2 = certificate.speed2,
                                         SerialNo = certificate.SerialNo,
                                         TamperSealNo = certificate.TamperSealNo,
                                         InstallDate = certificate.InstallDate,
                                         CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate,
                                         ExpDate = certificate.ExpDate,
                                         TechnicianName = certificate.TechnicianName,
                                         ApplicableStandard = certificate.ApplicableStandard,
                                         Country = certificate.Country,
                                         Cdate = certificate.Cdate,
                                         Cuser = certificate.Cuser,
                                         Mdate = certificate.Mdate,
                                         Muser = certificate.Muser,
                                         E_Stat = certificate.E_Stat,
                                         Duplicate = certificate.Duplicate,
                                         DealerId = certificate.DealerId,
                                         CompID = company.CompID,
                                         Name = company.Name,
                                         Address = company.Address,
                                         PhoneNo = company.PhoneNo,
                                         CountryName = country.CountryName,
                                         Expired = EntityFunctions.DiffDays(certificate.ExpDate, DateTime.UtcNow) > 0 ? "Expired" : "Valid"

                                     };
                        //CertificateView res = result.SingleOrDefault(a => a.VehRegistrationNo == RegNo);

                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("not found", JsonRequestBehavior.AllowGet);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Providing Repair Certificate for vehicles which have any issues.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Repair(string id = null)
        {
            using (DatContext db = new DatContext())
            {
                Certificate certificate = db.certificate_mst.Find(id);
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                dropdown();
                CompanyName();
                return PartialView(certificate);

            }
        }

        // POST: /Certificate/Edit/5

        [HttpPost]
        [Authorize, Audit]
        [ValidateAntiForgeryToken]
        [NoDirectAccess]
        public ActionResult Repair(Certificate certificate)
        {
            using (DatContext db = new DatContext())
            {

                Certificate cer = db.certificate_mst.Find(certificate.CertificateNo);


                CertificateLog cerlog = new CertificateLog();
                cerlog.CertificateNo = cer.CertificateNo;
                cerlog.SerialNo = cer.SerialNo;
                cerlog.TamperSealNo = cer.TamperSealNo;
                cerlog.TechnicianName = cer.TechnicianName;
                cerlog.VehRegistrationNo = cer.VehRegistrationNo;
                cerlog.Mdate = Convert.ToDateTime(cer.Mdate);
                cerlog.Muser = cer.Muser;
                cerlog.ChassisNo = cer.ChassisNo;
                cerlog.Country = cer.Country;
                cerlog.ApplicableStandard = cer.ApplicableStandard;
                cerlog.EngineNo = cer.EngineNo;
                cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                cerlog.InstallDate = cer.InstallDate;
                cerlog.MakeOfVehicle = cer.MakeOfVehicle;
                cerlog.ModelOfVehicle = cer.ModelOfVehicle;
                cerlog.OwnerName = cer.OwnerName;
                cerlog.SetSpeed = cer.SetSpeed;
                cerlog.ManufactureYear = cer.ManufactureYear;
                cerlog.TypeSpeedLimiter = cer.TypeSpeedLimiter;
                cerlog.E_Stat = cer.E_Stat;
                cerlog.Duplicate = cer.Duplicate;

                var Exist_exp = from a in db.certificate_mst where a.ExpDate > DateTime.UtcNow select a;
                var Exist_Estat = from a in db.certificate_mst where a.E_Stat == true select a;
                var Exist_serial = from a in db.SpeedLimiterSerialNo where a.E_Stat == false select a;
                var Exist_serialNo = from a in db.SpeedLimiterSerialNo where a.E_Stat == true select a;
                //var Exist_tampseal = from a in db.TamperSeal where a.E_Stat == false select a;
                // var Exist_tampsealNo = from a in db.TamperSeal where a.E_Stat == true select a;

                if (certificate.SerialNo != null && cer.SerialNo != certificate.SerialNo)
                {

                    if (Exist_serial.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                    {
                        if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                        }


                    }
                    else if (Exist_serialNo.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                    {

                        ModelState.AddModelError("SerialNo", "SerialNo already created ");
                    }
                    else
                    {
                        ModelState.AddModelError("SerialNo", "SerialNo does not exist in the available list.");
                    }


                }
                //if (certificate.TamperSealNo != null && cer.TamperSealNo != certificate.TamperSealNo)
                //{
                //    //if (Exist_tampseal.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                //    //{

                //    if (Exist_exp.Where(t => t.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                //    {
                //        ModelState.AddModelError("TamperSealNo", "TamperSealNo already Exists");
                //    }


                //    //}
                //    //else if (Exist_tampsealNo.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                //    //{
                //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo already created");
                //    //}
                //    //else
                //    //{
                //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo doesnot Exists");
                //    //}



                //}

                CompanyMaster comp = db.CompanyMst.Find(cer.DealerId);
                cer.Mdate = DateTime.UtcNow;
                cer.SerialNo = certificate.SerialNo;
                cer.TamperSealNo = certificate.TamperSealNo;
                cer.TechnicianName = certificate.TechnicianName;
                cer.Duplicate = (Convert.ToInt32(db.certificate_mst.SingleOrDefault(a => a.CertificateNo == certificate.CertificateNo).Duplicate) + 1);

                AdminMaster admn = db.AdminMst.Find(SessionWrapper.AdminID);
                CertificateView obj = new CertificateView
                {
                    CertificateNo = certificate.CertificateNo,
                    VehRegistrationNo = certificate.VehRegistrationNo,
                    Mdate = DateTime.UtcNow,
                    ExpDate = certificate.ExpDate,
                    SerialNo = certificate.SerialNo,
                    TamperSealNo = certificate.TamperSealNo,
                    InstallDate = certificate.InstallDate,
                    TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                    SetSpeed = certificate.SetSpeed,
                    TechnicianName = certificate.TechnicianName,
                    DealerLogo = comp.Logo,
                    AdminLogo = admn.Logo
                };
                if (ModelState.IsValid)
                {


                    db.certificate_log.Add(cerlog);
                    db.SaveChanges();

                    db.Entry(cer).State = EntityState.Modified;
                    //db.Entry(certificate).State = EntityState.Modified;
                    db.SaveChanges();
                    SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                    spd.E_Stat = true;
                    db.Entry(spd).State = EntityState.Modified;
                    // TamperSeal tamp = db.TamperSeal.SingleOrDefault(a => a.TamperSealNo == certificate.TamperSealNo);
                    //tamp.E_Stat = true;
                    //db.Entry(tamp).State = EntityState.Modified;
                    db.SaveChanges();
                    dropdown();
                    CompanyName();

                    FillImageUrl(obj, admn.Logo);
                    FillLogo(obj, obj.DealerLogo);
                    return this.ViewPdf("Certificate", "CertificateRepair", obj);
                }
                return View(certificate);


            }
        }


        /// <summary>
        /// Providing Edit for Admin to edit all details of certificate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        [NoDirectAccess]
        public ActionResult Edit(string id = null)
        {
            using (DatContext db = new DatContext())
            {
                Certificate certificate = db.certificate_mst.Find(id);
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                dropdown();
                CompanyName();
                return PartialView(certificate);

            }
        }

        // POST: /Certificate/Edit/5

        [HttpPost]
        [Authorize, Audit]
        [ValidateAntiForgeryToken]
        [NoDirectAccess]
        public ActionResult Edit(Certificate certificate)
        {

            using (DatContext db = new DatContext())
            {
                if (Certificate_AdminPanel.SessionWrapper.AdminLevel == AdminLevel.Admin || Certificate_AdminPanel.SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    Certificate cer = db.certificate_mst.Find(certificate.CertificateNo);

                    CertificateLog cerlog = new CertificateLog();
                    cerlog.CertificateNo = cer.CertificateNo;
                    cerlog.SerialNo = cer.SerialNo;
                    cerlog.TamperSealNo = cer.TamperSealNo;
                    cerlog.TechnicianName = cer.TechnicianName;
                    cerlog.VehRegistrationNo = cer.VehRegistrationNo;
                    cerlog.Mdate = Convert.ToDateTime(cer.Mdate);
                    cerlog.Muser = cer.Muser;
                    cerlog.ChassisNo = cer.ChassisNo;
                    cerlog.Country = cer.Country;
                    cerlog.ApplicableStandard = cer.ApplicableStandard;
                    cerlog.EngineNo = cer.EngineNo;
                    cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                    cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                    cerlog.InstallDate = cer.InstallDate;
                    cerlog.MakeOfVehicle = cer.MakeOfVehicle;
                    cerlog.ModelOfVehicle = cer.ModelOfVehicle;
                    cerlog.OwnerName = cer.OwnerName;
                    cerlog.SetSpeed = cer.SetSpeed;
                    cerlog.speed2 = cer.speed2;
                    cerlog.ManufactureYear = cer.ManufactureYear;
                    cerlog.TypeSpeedLimiter = cer.TypeSpeedLimiter;
                    cerlog.E_Stat = cer.E_Stat;
                    cerlog.Duplicate = cer.Duplicate;


                    //cer.Cuser = SessionWrapper.AdminID;
                    cer.Muser = SessionWrapper.AdminID;



                    // cer.Cdate = DateTime.Now.Date;
                    cer.Mdate = DateTime.UtcNow.Date;



                    cer.E_Stat = true;
                    var Exist_exp = from a in db.certificate_mst where a.ExpDate > DateTime.UtcNow select a;
                    cer.Duplicate = cer.VehRegistrationNo == ForRegnValue ? -1 : 0;
                    string cer_veh = cer.VehRegistrationNo;
                    cer.VehRegistrationNo = certificate.VehRegistrationNo;
                    if (cer.VehRegistrationNo == null)
                    {
                        ModelState.AddModelError("VehRegistrationNo", "Vehicle Registration number required");
                    }
                    else if (cer.VehRegistrationNo != cerlog.VehRegistrationNo)
                    {

                        if (Exist_exp.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 0)
                        {
                            ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        }
                    }
                    if (certificate.VehRegistrationNo == ForRegnValue)
                    {
                        ModelState.AddModelError("VehRegistrationNo", "Please Enter VehRegistrationNo");
                    }
                   

                    cer.OwnerName = certificate.OwnerName;
                    cer.ChassisNo = certificate.ChassisNo;
                    cer.EngineNo = certificate.EngineNo;
                    cer.ManufactureYear = certificate.ManufactureYear;
                    cer.MakeOfVehicle = certificate.MakeOfVehicle;
                    cer.ModelOfVehicle = certificate.ModelOfVehicle;
                    cer.SetSpeed = certificate.SetSpeed;
                    cer.speed2 = certificate.speed2;
                    cer.TypeSpeedLimiter = certificate.TypeSpeedLimiter;
                    cer.SerialNo = certificate.SerialNo;
                    cer.TamperSealNo = certificate.TamperSealNo;
                    cer.TechnicianName = certificate.TechnicianName;
                    cer.DealerId = certificate.DealerId;
                    
                    cer.Country = certificate.Country;
                    cer.Mdate = DateTime.Now;
                    cer.ApplicableStandard = certificate.ApplicableStandard;
                    cer.InstallDate = certificate.InstallDate.Date;
                    if (cer.InstallDate.AddYears(1).Date < DateTime.Now.Date)
                    {

                        cer.ExpDate = cer.Cdate.AddDays(-1).AddYears(1);
                    }
                    else
                    {

                        cer.ExpDate = cer.InstallDate.AddDays(-1).AddYears(1);
                    }
                    if (ModelState.IsValid)
                    {

                        db.Entry(cer).State = EntityState.Modified;
                        db.SaveChanges();
                        db.certificate_log.Add(cerlog);
                        db.SaveChanges();
                        //SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                        //spd.E_Stat = true;
                        //db.Entry(spd).State = EntityState.Modified;
                        //db.SaveChanges();
                        return RedirectToAction("Index");
                    }

                    dropdown();
                    CompanyName();
                    return View(certificate);
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            using (DatContext db = new DatContext())
            {
                db.Dispose();
                base.Dispose(disposing);
            }
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [Audit]
        public ActionResult Search()
        {
            return View();

        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]

        public ActionResult GetDet(string searchString)
        {

            using (DatContext db = new DatContext())
            {

                var obj = from certificate in db.certificate_mst
                          join country in db.countries
                          on certificate.Country equals country.CountryCode
                          join company in db.CompanyMst on
                          certificate.DealerId equals company.CompID
                          where certificate.E_Stat == true && certificate.CertificateNo.Trim() == searchString.Trim()
                          select new CertificateView
                          {
                              CertificateNo = certificate.CertificateNo,
                              VehRegistrationNo = certificate.VehRegistrationNo,
                              OwnerName = certificate.OwnerName,
                              ChassisNo = certificate.ChassisNo,
                              EngineNo = certificate.EngineNo,
                              ManufactureYear = certificate.ManufactureYear,
                              MakeOfVehicle = certificate.MakeOfVehicle,
                              ModelOfVehicle = certificate.ModelOfVehicle,
                              TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                              SetSpeed = certificate.SetSpeed,
                              speed2 = certificate.speed2,
                              SerialNo = certificate.SerialNo,
                              TamperSealNo = certificate.TamperSealNo,
                              InstallDate = certificate.InstallDate,
                              CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate,
                              ExpDate = certificate.ExpDate,
                              TechnicianName = certificate.TechnicianName,
                              ApplicableStandard = certificate.ApplicableStandard,
                              Country = certificate.Country,
                              Cdate = certificate.Cdate,
                              Cuser = certificate.Cuser,
                              Mdate = certificate.Mdate,
                              Muser = certificate.Muser,
                              E_Stat = certificate.E_Stat,
                              Duplicate = certificate.Duplicate,
                              DealerId = certificate.DealerId,
                              CompID = company.CompID,
                              Name = company.Name,
                              Address = company.Address,
                              PhoneNo = company.PhoneNo,
                              CountryName = country.CountryName,
                              Expired = EntityFunctions.DiffDays(certificate.ExpDate, DateTime.UtcNow) > 0 ? "Expired" : "Valid"

                          };

                CertificateView cobj = obj.SingleOrDefault(u => u.CertificateNo == searchString);

                return View(cobj);

            }


        }

        /// <summary>
        /// Serialno Create and index
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Delivery()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                format();
                AdminName();
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        [ValidateAntiForgeryToken, Audit, HttpPost, Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Delivery(SpeedLimiterSerialNo spdLmtrDelivery, bool chk1, bool chk2, int sercount = 0, int tampcount = 0, string TamperSealNo = null, int Total = 0, string except = null, int TamperTotal = 0, string TamperExcept = null)
        //public ActionResult Delivery(SpdLmtrDelivery spdLmtrDelivery,TamperSeal tamperSeal)
        {

            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    try
                    {
                        if (chk1 == true || chk2 == true)
                        {
                            if (chk1 == true)
                            {
                                spdLmtrDelivery.Date = DateTime.UtcNow;
                                spdLmtrDelivery.User = (int)SessionWrapper.AdminID;
                                spdLmtrDelivery.E_Stat = false;
                                if (spdLmtrDelivery.SerialNo == null)
                                {
                                    ModelState.AddModelError("SerialNo", "SerialNo required");
                                }
                                if (Total == 0)
                                {
                                    ModelState.AddModelError("", "Total field required");
                                }
                                //spdLmtrDelivery.SerialNo = (string.Format("{0}{1}", spdLmtrDelivery.SerialNo, sercount)).ToUpper();
                                var serial = (string.Format("{0}{1}", spdLmtrDelivery.SerialNo, sercount)).ToUpper();
                                if (db.SpeedLimiterSerialNo.Where(a => a.SerialNo == serial).Count() > 0)
                                {
                                    ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                                }



                                if (ModelState.IsValid)
                                {
                                    //int extractserial = int.Parse(ExtractNumber(spdLmtrDelivery.SerialNo));

                                    string serialstr = spdLmtrDelivery.SerialNo.ToUpper();

                                    int serialEnd = sercount + Total - 1;

                                    //int tamperEnd = extracttamp + TamperTotal - 1;


                                    for (int a = sercount; a <= serialEnd; a++)
                                    {
                                        spdLmtrDelivery.SerialNo = string.Format("{0}{1}", serialstr, a);

                                        string ex = string.Format("{0}{1}", serialstr, except);


                                        if (spdLmtrDelivery.SerialNo != ex)
                                        {
                                            db.SpeedLimiterSerialNo.Add(spdLmtrDelivery);
                                            db.SaveChanges();
                                        }

                                    }
                                }
                                else
                                {
                                    AdminName();
                                    return View(spdLmtrDelivery);
                                }
                            }
                            if (chk2 == true)
                            {
                                if (TamperSealNo != "")
                                {
                                    var tamper = (string.Format("{0}{1}", TamperSealNo, tampcount)).ToUpper();
                                    if (db.TamperSeal.Where(a => a.TamperSealNo == tamper).Count() > 0)
                                    {
                                        ModelState.AddModelError("", "TamperSealNo already Exists");
                                    }

                                    if (TamperTotal == 0)
                                    {
                                        ModelState.AddModelError("", "TamperSeal total field required");
                                    }
                                    // TamperSealNo = string.Format("{0}{1}", TamperSealNo, tampcount).ToUpper();
                                    //int extracttamp = int.Parse(ExtractNumber(TamperSealNo));
                                    string tamperstr = TamperSealNo.ToUpper();
                                    //int tamperEnd = extracttamp + TamperTotal - 1;
                                    int tamperEnd = tampcount + TamperTotal - 1;
                                    TamperSeal tamperseal = new TamperSeal();
                                    tamperseal.Date = DateTime.UtcNow;
                                    tamperseal.E_Stat = false;
                                    tamperseal.User = (int)SessionWrapper.AdminID;
                                    for (int a = tampcount; a <= tamperEnd; a++)
                                    {
                                        tamperseal.TamperSealNo = string.Format("{0}{1}", tamperstr, a);
                                        string exc = string.Format("{0}{1}", tamperstr, TamperExcept);
                                        if (tamperseal.TamperSealNo != exc)
                                        {
                                            db.TamperSeal.Add(tamperseal);
                                            db.SaveChanges();
                                        }
                                    }
                                }

                            }
                            TempData["Message"] = "Serial No. added succesfully!!!";
                            AdminName();
                            return View(spdLmtrDelivery);//return RedirectToAction("Deliverydetails");
                        }
                        format();
                        AdminName();
                        return View(spdLmtrDelivery);

                    }

                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                    }
                    return View();

                }
                else
                {
                    return View("UnAuthenticated");
                }
            }

        }


        [Authorize, Audit, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ViewResult Deliverydetails(SpdLmtrDeliveryView spd)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    if (spd.fromdate != null && spd.todate != null && spd.Dealerdd != null)
                    {

                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == true select d).Count();
                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());
                    }
                    else if (spd.fromdate != null && spd.todate != null)
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == true select d).Count();
                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());

                    }
                    else if (spd.fromdate == null && spd.todate == null && spd.Dealerdd != null)
                    {

                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where spd.Dealerdd == d.DealerId && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where spd.Dealerdd == d.DealerId && d.E_Stat == true select d).Count();
                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where spd.Dealerdd == b.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());
                    }
                    else
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.E_Stat == true select d).Count();

                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     //join c in db.certificate_mst on a.SerialNo equals c.SerialNo into cer
                                     //from sp in cer.DefaultIfEmpty()
                                     // where sp.E_Stat==true
                                     //where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         //Used=(db.certificate_mst.FirstOrDefault(z=>z.SerialNo==a.SerialNo).DealerId)!=null?b.Name:"NILL",
                                         //Used=(sp!=null?(db.CompanyMst.FirstOrDefault(l=>l.CompID==sp.DealerId)).Name:string.Empty),
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        //return View(result.ToList());
                        CommonData.JsonSpeedLimiterDevicesPush(result.ToList());
                        return View();
                    }
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    if (spd.fromdate != null && spd.todate != null && spd.Dealerdd != null)
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();
                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID && b.AdminID == SessionWrapper.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }
                    else if (spd.fromdate == null && spd.todate == null && spd.Dealerdd != null)
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where spd.Dealerdd == d.DealerId && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where spd.Dealerdd == d.DealerId && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();

                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID && b.AdminID == SessionWrapper.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }

                    else if (spd.fromdate != null && spd.todate != null)
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();

                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && b.AdminID == SessionWrapper.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }
                    else
                    {
                        ViewBag.Availablecnt = (from d in db.SpeedLimiterSerialNo where d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.SpeedLimiterSerialNo where d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();
                        var result = from a in db.SpeedLimiterSerialNo
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     //where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     where b.AdminID == SessionWrapper.AdminID
                                     select new SpdLmtrDeliveryView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        //Offline storage
                        CommonData.JsonSpeedLimiterDevicesPush(result.ToList());
                        return View();
                        //return View(result.ToList());

                    }

                }
                else
                {
                    return View("UnAuthenticated");
                }
              
            }
        }

        /// <summary>
        /// TO Edit the SerialNo 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        //GET:/Certificate/SerialNoEdit/
        //[Audit]
        public ActionResult SerialNoEdit(int id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.Find(id);
                    return PartialView(spd);
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }

        //POST:/Certificate/SerialNoEdit/

        [ValidateAntiForgeryToken, Audit, HttpPost, Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SerialNoEdit(SpeedLimiterSerialNo _speedlimiterserialno)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    SpeedLimiterSerialNo _spd = db.SpeedLimiterSerialNo.Find(_speedlimiterserialno.Recno);
                    _spd.SerialNo = _speedlimiterserialno.SerialNo;
                    if (_spd.E_Stat == true)
                    {
                        ModelState.AddModelError("", "Certificate already created with this Serialno");
                    }
                    if (ModelState.IsValid)
                    {
                        _spd.Date = DateTime.Now;
                        db.Entry(_spd).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Deliverydetails");
                    }
                    return View();
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }

        public JsonResult Deliverydetails_json()
        {
            using (DatContext db = new DatContext())
            {
                var x = from a in db.SpeedLimiterSerialNo select a;
                return this.Json(x.ToList(), JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Deliverydelete_json(int id)
        {
            using (DatContext db = new DatContext())
            {
                Template temp = db.Template.Find(id);
                db.Template.Remove(temp);
                db.SaveChanges();
                return Json(new { name = temp.ID });
            }
        }

        private string ExtractNumber(string original)
        {
            return new string(original.Where(c => Char.IsDigit(c)).ToArray());
        }
        private string Str(string original)
        {
            return new string(original.Where(c => !Char.IsDigit(c)).ToArray());
        }

        [NonAction]
        private void format()
        {
            using (DatContext db = new DatContext())
            {
                var typespeed = new[]
            {
                new SelectListItem{Text="Cable Control",Value="Cable Control"},
                new SelectListItem{Text="Fuel Control",Value="Fuel Control"},
                new SelectListItem{Text="Electronic Pedal",Value="Electronic Pedal"},
            };
                ViewBag.SpeedLimiter = new SelectList(typespeed, "Text", "Value");
            }
        }


        //public ViewResult TestSession()
        //{

        //    Session["item1"] = "wow!";

        //    return View();

        //}


        //[HttpGet]
        //public ActionResult DailyReport()
        //{
        //    return View();
        //}
        //[HttpPost]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [Authorize, Audit]
        public ActionResult DailyReport(CertificateView cer)
        {
            using (DatContext db = new DatContext())
            {
                if (cer.fromdate == null)
                {
                    cer.fromdate = DateTime.UtcNow.AddDays(-1);
                }
                if (cer.todate == null)
                {
                    cer.todate = DateTime.UtcNow;
                }

                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {

                    ViewBag.totalcnt = (from d in db.certificate_mst where EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) == 0 select d).Count();
                    ViewBag.totalweek = (from d in db.certificate_mst where EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 7 select d).Count();
                    ViewBag.totalmonth = (from d in db.certificate_mst where EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 30 select d).Count();

                    //DateTime strdate =Convert.ToDateTime(fromdate);
                    //DateTime enddate = Convert.ToDateTime(todate);
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID//EntityFunctions.DiffDays(a.Cdate, DateTime.Now) == 0
                             where a.Cdate >= cer.fromdate && a.Cdate <= cer.todate
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,
                                 ManufactureYear = a.ManufactureYear,
                                 MakeOfVehicle = a.MakeOfVehicle,
                                 ModelOfVehicle = a.ModelOfVehicle,
                                 TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 SetSpeed = a.SetSpeed,
                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.Cuser,
                                 Mdate = a.Mdate,
                                 Muser = a.Muser,
                                 E_Stat = a.E_Stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName,

                             };

                    return View(cn.OrderByDescending(z => z.Cdate).ToList());
                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    ViewBag.totalcnt = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == SessionWrapper.AdminID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) == 0 select d).Count();
                    ViewBag.totalweek = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == SessionWrapper.AdminID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 7 select d).Count();
                    ViewBag.totalmonth = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == SessionWrapper.AdminID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 30 select d).Count();

                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where a.Cdate >= cer.fromdate && a.Cdate <= cer.todate && b.AdminID == SessionWrapper.AdminID
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,
                                 ManufactureYear = a.ManufactureYear,
                                 MakeOfVehicle = a.MakeOfVehicle,
                                 ModelOfVehicle = a.ModelOfVehicle,
                                 TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 SetSpeed = a.SetSpeed,
                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.Cuser,
                                 Mdate = a.Mdate,
                                 Muser = a.Muser,
                                 E_Stat = a.E_Stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName

                             };
                    return View(cn.OrderByDescending(z => z.Cdate).ToList());
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    ViewBag.totalcnt = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID) && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) == 0 select d).Count();
                    ViewBag.totalweek = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID) && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 7 select d).Count();
                    ViewBag.totalmonth = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID) && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 30 select d).Count();

                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where a.Cdate >= cer.fromdate && a.Cdate <= cer.todate && b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,
                                 ManufactureYear = a.ManufactureYear,
                                 MakeOfVehicle = a.MakeOfVehicle,
                                 ModelOfVehicle = a.ModelOfVehicle,
                                 TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 SetSpeed = a.SetSpeed,
                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.Cuser,
                                 Mdate = a.Mdate,
                                 Muser = a.Muser,
                                 E_Stat = a.E_Stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName

                             };
                    return View(cn.OrderByDescending(z => z.Cdate).ToList());
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                    ViewBag.totalcnt = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.CompID == SessionWrapper.CompanyID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) == 0 select d).Count();
                    ViewBag.totalweek = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.CompID == SessionWrapper.CompanyID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 7 select d).Count();
                    ViewBag.totalmonth = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where b.CompID == SessionWrapper.CompanyID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 30 select d).Count();

                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where a.Cdate >= cer.fromdate && a.Cdate <= cer.todate && b.CompID == SessionWrapper.CompanyID
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,
                                 ManufactureYear = a.ManufactureYear,
                                 MakeOfVehicle = a.MakeOfVehicle,
                                 ModelOfVehicle = a.ModelOfVehicle,
                                 TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 SetSpeed = a.SetSpeed,
                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.Cuser,
                                 Mdate = a.Mdate,
                                 Muser = a.Muser,
                                 E_Stat = a.E_Stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName

                             };
                    return View(cn.OrderByDescending(z => z.Cdate).ToList());

                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    ViewBag.totalcnt = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where d.Cuser == SessionWrapper.UserID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) == 0 select d).Count();
                    ViewBag.totalweek = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where d.Cuser == SessionWrapper.UserID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 7 select d).Count();
                    ViewBag.totalmonth = (from d in db.certificate_mst join b in db.CompanyMst on d.DealerId equals b.CompID where d.Cuser == SessionWrapper.UserID && EntityFunctions.DiffDays(d.Cdate, DateTime.UtcNow) < 30 select d).Count();

                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID

                             where a.Cdate >= cer.fromdate && a.Cdate <= cer.todate && a.Cuser == SessionWrapper.UserID
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,
                                 ManufactureYear = a.ManufactureYear,
                                 MakeOfVehicle = a.MakeOfVehicle,
                                 ModelOfVehicle = a.ModelOfVehicle,
                                 TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 SetSpeed = a.SetSpeed,
                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.Cuser,
                                 Mdate = a.Mdate,
                                 Muser = a.Muser,
                                 E_Stat = a.E_Stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName

                             };
                    return View(cn.OrderByDescending(z => z.Cdate).ToList());


                }
                else
                {
                    return View("LogOn");
                }

            }

        }
        [Audit]
        public ActionResult ExpiredReport()
        {
            using (DatContext db = new DatContext())
            {
                try
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     //CompID=comp.CompID,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID
                                 && a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                 && a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    {
                        //var cert = from a in db.certificate_mst join c in db.countries
                        //           on a.Country equals c.CountryCode join b in db.CompanyMst
                        //           on a.DealerId equals b.CompID select new 
                        //           CertificateView{}
                        //var cert = db.certificate_mst.Include(c => c.con);
                        //cert = from a in cert where a.DealerId == SessionWrapper.CompanyID select a;
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.CompID == SessionWrapper.CompanyID &&
                                 a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View(cn.ToList());

                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.Cuser == SessionWrapper.UserID &&
                                 a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };

                        return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    }
                    else
                    {
                        return View("LogOn");
                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
                return View();
            }
        }
        [Audit]
        public ActionResult TobeExpiredReport()
        {
            using (DatContext db = new DatContext())
            {
                try
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > -8 && !(EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0)
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     //CompID=comp.CompID,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View("ExpiredReport", cn.ToList());
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID
                                 && a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > -8 && !(EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0)
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View("ExpiredReport", cn.ToList());
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                 && a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > -8 && !(EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0)
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View("ExpiredReport", cn.ToList());
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    {
                        //var cert = from a in db.certificate_mst join c in db.countries
                        //           on a.Country equals c.CountryCode join b in db.CompanyMst
                        //           on a.DealerId equals b.CompID select new 
                        //           CertificateView{}
                        //var cert = db.certificate_mst.Include(c => c.con);
                        //cert = from a in cert where a.DealerId == SessionWrapper.CompanyID select a;
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.CompID == SessionWrapper.CompanyID &&
                                 a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > -8 && !(EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0)
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };
                        return View("ExpiredReport", cn.ToList());

                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.Cuser == SessionWrapper.UserID &&
                                 a.E_Stat == true && EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > -8 && !(EntityFunctions.DiffDays(a.ExpDate, DateTime.UtcNow) > 0)
                                 select new CertificateView
                                 {
                                     CertificateNo = a.CertificateNo,
                                     VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                     OwnerName = a.OwnerName,
                                     ChassisNo = a.ChassisNo,
                                     EngineNo = a.EngineNo,
                                     ManufactureYear = a.ManufactureYear,
                                     MakeOfVehicle = a.MakeOfVehicle,
                                     ModelOfVehicle = a.ModelOfVehicle,
                                     TypeSpeedLimiter = a.TypeSpeedLimiter,
                                     SetSpeed = a.SetSpeed,
                                     SerialNo = a.SerialNo,
                                     TamperSealNo = a.TamperSealNo,
                                     InstallDate = a.InstallDate,
                                     ExpDate = a.ExpDate,
                                     TechnicianName = a.TechnicianName,
                                     ApplicableStandard = a.ApplicableStandard,
                                     //Country = a.Country,
                                     Cdate = a.Cdate,
                                     Cuser = a.Cuser,
                                     Mdate = a.Mdate,
                                     Muser = a.Muser,
                                     E_Stat = a.E_Stat,
                                     Duplicate = a.Duplicate,
                                     DealerId = a.DealerId,
                                     Name = b.DisplayName,
                                     CountryName = c.CountryName

                                 };

                        return View("ExpiredReport", cn.ToList());
                    }
                    else
                    {
                        return View("LogOn");
                    }

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                }
                return View();
            }
        }
        // [Audit]
         [NoDirectAccess]
        public ActionResult Delete(string id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                using (DatContext db = new DatContext())
                {

                    Certificate cer = db.certificate_mst.Find(id);
                    return PartialView(cer);
                }
            }
            else
            {
                return View("LogOn");
            }
        }
         [NoDirectAccess]
        [HttpPost, Audit, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    Certificate cer = db.certificate_mst.Find(id);

                    CertificateLog cerlog = new CertificateLog();
                    cerlog.CertificateNo = cer.CertificateNo;
                    cerlog.SerialNo = cer.SerialNo;
                    cerlog.TamperSealNo = cer.TamperSealNo;
                    cerlog.TechnicianName = cer.TechnicianName;
                    cerlog.VehRegistrationNo = cer.VehRegistrationNo;
                    cerlog.Mdate = Convert.ToDateTime(cer.Mdate);
                    cerlog.Muser = cer.Muser;
                    cerlog.ChassisNo = cer.ChassisNo;
                    cerlog.Country = cer.Country;
                    cerlog.ApplicableStandard = cer.ApplicableStandard;
                    cerlog.EngineNo = cer.EngineNo;
                    cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                    cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                    cerlog.InstallDate = cer.InstallDate;
                    cerlog.MakeOfVehicle = cer.MakeOfVehicle;
                    cerlog.ModelOfVehicle = cer.ModelOfVehicle;
                    cerlog.OwnerName = cer.OwnerName;
                    cerlog.SetSpeed = cer.SetSpeed;
                    cerlog.speed2 = cer.speed2;
                    cerlog.ManufactureYear = cer.ManufactureYear;
                    cerlog.TypeSpeedLimiter = cer.TypeSpeedLimiter;
                    cerlog.E_Stat = cer.E_Stat;
                    cerlog.Duplicate = cer.Duplicate;


                    SpeedLimiterSerialNo ser = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == cer.SerialNo);
                    ser.E_Stat = false;
                    db.Entry(ser).State = EntityState.Modified;
                    db.certificate_log.Add(cerlog);
                    db.certificate_mst.Remove(cer);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }


        // Dealer name, Certificates

        public JsonResult PieChart()
        {

            DatContext db = new DatContext();

            //var c = db.certificate_mst.GroupBy(a => a.DealerId).Select(l => new { ky = l.Key, cnt = l.Count() }).ToDictionary(group=>group.ky,group=>group.cnt);
            //return Json(c, JsonRequestBehavior.AllowGet);
            //var res = db.certificate_mst.GroupBy(a => a.DealerId).Select(a => new { word = a.Key, count = a.Count() });
            //return Json(res.ToList(), JsonRequestBehavior.AllowGet);


            //        new { date = new DateTime(s.read_date.Year, s.read_date.Month, 1) } into g
            //select new
            //{
            //    read_date = g.Key.date,
            //    T1 = g.Sum(x => x.T1),
            //    T2 = g.Sum(x => x.T2)
            //};
            try
            {
                //var x = from a in db.certificate_mst
                //        orderby a.DealerId
                //        group a by a.DealerId into grp
                //        select new { key = grp.Key, cnt = grp.Count() };
                //var z = from a in db.certificate_mst
                //        join b in db.AdminMst on a.DealerId equals b.AdminID
                //        select new { b.Name, a.CertificateNo };
                //var c = z.GroupBy(i => i.Name).Select(i => new { word = i.Key, count = i.Count() });
                // var sd = db.certificate_mst.GroupBy(i=>i.DealerId).Select(i => new { word = i.Key, count = i.Count() });


                //abcd = db.certificate_mst.GroupBy(i => i.DealerId).Select(i => new { wodsfrd = i.Key, count = i.Count() }).ToDictionary(
                //foreach (var line in db.certificate_mst.GroupBy(info => info.DealerId)
                //            .Select(group => new
                //            {
                //                ky = group.Key,
                //                cnt = group.Count()
                //            })) ;

                //return Json(c.ToList(), JsonRequestBehavior.AllowGet);
                //var res = db.certificate_mst.GroupBy(a => a.DealerId).Select(l => new { ky = l.Key, cnt = l.Count() }).ToDictionary(group => group.ky, group => group.cnt);
                //return Json(c.ToList(), JsonRequestBehavior.AllowGet);
                if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var res = from a in db.certificate_mst join b in db.CompanyMst on a.DealerId equals b.CompID join c in db.AdminMst on b.AdminID equals c.AdminID where c.AdminID == SessionWrapper.AdminID && a.E_Stat == true select new { b.Name, a.CertificateNo, a.Cdate };
                    var result = res.GroupBy(i => new { i.Name}).Select(i => new { word = i.Key, count = i.Count()});
                    return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var res = from a in db.certificate_mst
                              join b in db.CompanyMst on a.DealerId equals b.CompID
                              join c in db.AdminMst on b.AdminID equals c.AdminID
                              where a.E_Stat == true
                              select new { c.Name, a.CertificateNo, a.Cdate };
                    var result = res.GroupBy(a => new { a.Name}).Select(a => new { word = a.Key, count = a.Count()});
                    return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    var res = from a in db.certificate_mst
                              join b in db.CompanyMst on a.DealerId equals b.CompID
                              where a.E_Stat == true && b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                              select new { b.Name, a.CertificateNo };
                    var result = res.GroupBy(l => l.Name).Select(l => new { word = l.Key, count = l.Count() });
                    return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                }

                else
                {
                    //var result = from s in db.certificate_mst group s by new { date = new DateTime(s.Cdate.Year, s.Cdate.Month, 1) } into a select new { month = a.Key.date, count = a.Count() };
                    //var result = from s in db.certificate_mst group s by s.Cdate.Month into a select new { month = a.Key, count = a.Count() };
                    //var result = db.certificate_mst.GroupBy(a => a.Cdate.ToString("yyyy-mm")).OrderBy(a => a.Key).Select(a => new
                    //    {
                    //        month = a.Key,
                    //        count = a.Count()
                    //    });

                    //var x = from d in db.certificate_mst group d by d.DealerId select d; 
                    ////////var res = db.certificate_mst.GroupBy(a => new { a.Cdate.Year,a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                    //////////var result = res.ToDictionary(g => string.Format("{0}-{1}", g.Year, g.Month), g => g.count);
                    ////////var result = res.Select(a =>new{word= string.Format("{0}-{1}",a.Year,a.Month), count =a.count });
                    ////////return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    return Json(null, JsonRequestBehavior.DenyGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get data for line chart
        /// </summary>
        /// <returns></returns>
        public ActionResult linechart(int provinceId = 0)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    if (provinceId == 0)
                    {
                        var admn = from a in db.certificate_mst where a.E_Stat == true select a;
                        var res = admn.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        //var result = res.ToDictionary(g => string.Format("{0}-{1}", g.Year, g.Month), g => g.count);
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        var admn = from a in db.certificate_mst join b in db.CompanyMst on a.DealerId equals b.CompID join c in db.AdminMst on b.AdminID equals c.AdminID where c.AdminID == provinceId && a.E_Stat == true select a;
                        var res = admn.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        //var result = res.ToDictionary(g => string.Format("{0}-{1}", g.Year, g.Month), g => g.count);
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    }
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    if (provinceId == 0)
                    {
                        var adm = from a in db.certificate_mst join b in db.CompanyMst on a.DealerId equals b.CompID join c in db.AdminMst on b.AdminID equals c.AdminID where c.AdminID == SessionWrapper.AdminID && a.E_Stat == true select a;
                        var res = adm.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        //var result = res.ToDictionary(g => string.Format("{0}-{1}", g.Year, g.Month), g => g.count);
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        var comp = from a in db.certificate_mst join b in db.CompanyMst on a.DealerId equals b.CompID where b.CompID == provinceId && a.E_Stat == true select a;
                        var res = comp.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        //var result = res.ToDictionary(g => string.Format("{0}-{1}", g.Year, g.Month), g => g.count);
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);

                    }
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    if (provinceId == 0)
                    {
                        var admn = from a in db.certificate_mst
                                   join b in db.CompanyMst on a.DealerId equals b.CompID
                                   where b.AdminID == (db.InspectionMst.FirstOrDefault(i => i.InspID == SessionWrapper.CompanyID).AdminID) && a.E_Stat == true
                                   select a;
                        var res = admn.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var admn = from a in db.certificate_mst
                                   join b in db.CompanyMst on a.DealerId equals b.CompID
                                   where b.AdminID == (db.InspectionMst.FirstOrDefault(i => i.InspID == SessionWrapper.CompanyID).AdminID) && b.CompID == provinceId && a.E_Stat == true
                                   select a;
                        var res = admn.GroupBy(a => new { a.Cdate.Year, a.Cdate.Month }).Select(g => new { g.Key.Year, g.Key.Month, count = g.Count() }).OrderBy(x => x.Year).ThenBy(x => x.Month).ToList();
                        var result = res.Select(a => new { word = string.Format("{0}-{1}", a.Year, a.Month), count = a.count });
                        return Json(result.ToList(), JsonRequestBehavior.AllowGet);
                    }

                }
                else
                {
                    return Json(null, JsonRequestBehavior.DenyGet);
                }

            }

        }

        //public ActionResult ExpiredReport()
        //{
        //    return RedirectToAction("Index");
        //}

        //public ActionResult Update(string searchString)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        return View(db.certificate_mst.Find(searchString));
        //    }      

        //}


        //public ActionResult Update(Certificate certificate)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        Certificate cer = db.certificate_mst.Find(certificate.CertificateNo);
        //        cer.SerialNo = certificate.SerialNo;
        //        cer.TamperSealNo = certificate.TamperSealNo;
        //        if (ModelState.IsValid)
        //        {
        //            db.certificate_mst.Add(certificate);
        //            db.SaveChanges();
        //            db.Entry(cer).State = EntityState.Modified;
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }


        //        return View(certificate);
        //    }
        //}

        //
        // GET: /Certificate/Create
        [Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ForRegnCreate()
        {
            if (SessionWrapper.IsAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
            {
                dropdown();
                CompanyName();
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        //
        // POST: /Certificate/Create


        [ValidateAntiForgeryToken, Authorize, Audit, HttpPost, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ForRegnCreate(Certificate certificate)
        {
            using (DatContext db = new DatContext())
            {
                var Exist_exp = from a in db.certificate_mst where a.ExpDate > DateTime.UtcNow select a;
                var Exist_Estat = from a in db.certificate_mst where a.E_Stat == true select a;
               // Certificate cert = Exist_Estat.SingleOrDefault(c => c.VehRegistrationNo == certificate.VehRegistrationNo);
               
                    if (certificate.ChassisNo != null)
                    {

                        if (Exist_exp.Where(c => c.ChassisNo == certificate.ChassisNo).Count() > 0)
                        {
                            ModelState.AddModelError("ChassisNo", "ChassisNo already Exists");
                        }
                    }
                    if (certificate.EngineNo != null)
                    {
                        if (Exist_exp.Where(e => e.EngineNo == certificate.EngineNo).Count() > 0)
                        {
                            ModelState.AddModelError("EngineNo", "EngineNo already Exists");
                        }
                    }

                    var Exist_serial = from a in db.SpeedLimiterSerialNo where a.E_Stat == false select a;
                    var Exist_serialNo = from a in db.SpeedLimiterSerialNo where a.E_Stat == true select a;
                    //var Exist_tampseal = from a in db.TamperSeal where a.E_Stat == false select a;
                    // var Exist_tampsealNo = from a in db.TamperSeal where a.E_Stat == true select a;
                    if (certificate.SerialNo != null)
                    {

                        if (Exist_serial.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                            }


                        }
                        else if (Exist_serialNo.Where(a => a.SerialNo == certificate.SerialNo).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.SerialNo == certificate.SerialNo).Count() > 0)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already created ");
                            }

                            var z = db.certificate_mst.FirstOrDefault(a => a.SerialNo == certificate.SerialNo);
                            if (z.ChassisNo != certificate.ChassisNo)
                            {
                                ModelState.AddModelError("SerialNo", "SerialNo already created with another device");

                            }
                            //if (db.certificate_mst.Where(l=>l.CertificateNo==certificate.CertificateNo).Count()>0)
                            //{
                            //    ModelState.AddModelError("SerialNo", "SerialNo already created with another device");
                            //}
                            //if()
                        }
                        else
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo does not exist in the available list.");
                        }


                    }
                    string certnum = (Convert.ToInt32(db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();
                    if (certificate.EngineNo != null)
                    {
                        certificate.CertificateNo = string.Format("{0}-{1}-{2}", certificate.Country, GetLast(certificate.EngineNo, 6), certnum.PadLeft(4, '0'));
                    }
                    CertificateView ce = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        certificate.Cuser = SessionWrapper.AdminID;
                        certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminEmail = adminmaster.Email;
                        ce.AdminTemplate = temp.ViewName;
                        certificate.Cuser = SessionWrapper.UserID;
                        certificate.Muser = SessionWrapper.UserID;
                    }

                    if (!SessionWrapper.IsAdmin)
                    {
                        certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    certificate.Cdate = DateTime.UtcNow;
                    certificate.Mdate = DateTime.UtcNow;

                    if (certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                    {

                        certificate.ExpDate = certificate.Cdate.AddDays(-1).AddMonths(3);
                    }
                    else
                    {
                        certificate.ExpDate = certificate.InstallDate.AddDays(-1).AddMonths(3);
                    }

                    certificate.E_Stat = true;

                    if (ModelState.IsValid)
                    {

                        certificate.VehRegistrationNo = ForRegnValue;
                        db.certificate_mst.Add(certificate);
                        db.SaveChanges();
                        SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                        spd.E_Stat = true;
                        db.Entry(spd).State = EntityState.Modified;
                        //TamperSeal tamp = db.TamperSeal.SingleOrDefault(a => a.TamperSealNo == certificate.TamperSealNo);
                        //tamp.E_Stat = true;
                        //db.Entry(tamp).State = EntityState.Modified;
                        CommonFlag comflag = db.CommonFlag.Find(1);
                        comflag.Value = certnum;
                        db.Entry(comflag).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);

                        Country con = db.countries.Find(certificate.Country);
                        CertificateView objcertificateview = new CertificateView
                        {
                            CertificateNo = certificate.CertificateNo,
                            VehRegistrationNo = ForRegnValue,
                            OwnerName = (certificate.OwnerName).ToUpper(),
                            ChassisNo = certificate.ChassisNo,
                            EngineNo = certificate.EngineNo,
                            ManufactureYear = certificate.ManufactureYear,
                            MakeOfVehicle = certificate.MakeOfVehicle,
                            ModelOfVehicle = certificate.ModelOfVehicle,
                            TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                            SetSpeed = certificate.SetSpeed,
                            speed2 = certificate.speed2,
                            SerialNo = certificate.SerialNo,
                            TamperSealNo = certificate.TamperSealNo,
                            InstallDate = certificate.InstallDate,
                            ExpDate = certificate.ExpDate,
                            TechnicianName = certificate.TechnicianName,
                            ApplicableStandard = certificate.ApplicableStandard,
                            Country = certificate.Country,
                            Cdate = certificate.Cdate,
                            Cuser = certificate.Cuser,
                            Mdate = certificate.Mdate,
                            Muser = certificate.Muser,
                            E_Stat = certificate.E_Stat,
                            Duplicate = certificate.Duplicate,
                            DealerId = certificate.DealerId,
                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                            CountryName = con.CountryName,
                            AdminName = ce.AdminName,
                            AdminAddress = ce.AdminAddress,
                            AdminPhone = ce.AdminPhone,
                            AdminEmail = ce.AdminEmail,
                            CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };

                        FillImageUrl(objcertificateview, ce.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", ce.AdminTemplate, objcertificateview);

                    }
                }
                dropdown();
                CompanyName();
                return View(certificate);
            }
    }
}