﻿using Certificate_AdminPanel.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Certificate_AdminPanel.Controllers
{
    public class AuditController : Controller
    {
        //
        // GET: /Audit/
        // [Audit]
        [Authorize]
        [OutputCache(CacheProfile = "1MinuteCacheNoParameter")]
        public ActionResult Index()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {


                using (DatContext db = new DatContext())
                {
                    var res = from a in db.Audit
                              where a.UserName != "Anonymous"
                              select new AuditView
                              {
                                  AuditID = a.AuditID,
                                  IPAddress = a.IPAddress,
                                  UserName = a.UserName,
                                  URLAccessed = a.URLAccessed,
                                  TimeAccessed = EntityFunctions.AddMinutes(a.TimeAccessed, 330),
                              };

                    AuditJson(res.OrderByDescending(xx=>xx.TimeAccessed).ToList());
                    return View();
                }
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        public void AuditJson(List<AuditView> Results)
        {
            try
            {
                var path = Server.MapPath(Url.Content("~/JsonStorage/Audit.txt"));
                var info = new FileInfo(path);
                if ((!info.Exists) || info.Length == 0)
                {
                    // file is empty or non-existant  
                    string jsonData = JsonConvert.SerializeObject(Results.ToArray(), Formatting.Indented);
                    // Creates or overwrites the file with the contents of the JSON
                    System.IO.File.WriteAllText(path, jsonData);
                }
                else
                {
                    // Read existing json data
                    var jsonData = System.IO.File.ReadAllText(path);
                    List<AuditView> items = JsonConvert.DeserializeObject<List<AuditView>>(jsonData);
                    if (items != null && !(items.Count() == Results.Count()))
                    {

                        // Update json data string
                        string Data = JsonConvert.SerializeObject(Results.ToArray(), Formatting.Indented);
                        System.IO.File.WriteAllText(path, Data);
                    }
                    else if (items == null)
                    {
                        // Update json data string
                        string Data = JsonConvert.SerializeObject(Results.ToArray(), Formatting.Indented);
                        System.IO.File.WriteAllText(path, Data);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
        }
    }

    public class AuditAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Stores the Request in an Accessible object
            var request = filterContext.HttpContext.Request;

            //Generate an audit
            Audit audit = new Audit()
            {

                //IPAddress = System.Web.HttpContext.Current.Request.UserHostAddress,
                IPAddress = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.UserHostAddress,
                URLAccessed = request.RawUrl,
                TimeAccessed = DateTime.UtcNow,
                UserName = (request.IsAuthenticated) ? filterContext.HttpContext.User.Identity.Name : "Anonymous",
            };

            //Stores the Audit in the Database
            DatContext context = new DatContext();

            context.Audit.Add(audit);
            context.SaveChanges();

            base.OnActionExecuting(filterContext);
        }
    }
}
