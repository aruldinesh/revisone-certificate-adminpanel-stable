﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;
using System.Security.Cryptography;
using System.Text;
using PagedList;
using System.Data.SqlClient;

namespace Certificate_AdminPanel.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private DatContext db = new DatContext();
        List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
        //
        // GET: /Usermanage/
        [Audit]
        public ViewResult Index()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            //if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            //{

                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {

                    var loginmst = (from tab1 in db.LoginMst
                                    where tab1.E_Stat==true
                                    orderby tab1.UserID  
                                    select new
                                    {
                                        Name = tab1.UserMst.Name,
                                        Address = tab1.UserMst.Address,
                                        CDate = tab1.UserMst.CDate,
                                        //Created = db.UserMst.FirstOrDefault(u => u.UserID == tab1.UserMst.CUserID).Name,
                                        Company = db.CompanyMst.FirstOrDefault(u => u.CompID == tab1.UserMst.CompID).Name,
                                        E_Stat = tab1.UserMst.E_Stat,
                                        LoginName = tab1.LoginName,
                                        password=tab1.pwd,
                                        Type=tab1.Type,
                                        MDate = tab1.UserMst.MDate,
                                        Mobile = tab1.UserMst.Mobile,
                                        //Modified = db.UserMst.FirstOrDefault(u => u.UserID == tab1.UserMst.MUserID).Name,
                                        PhoneNo = tab1.UserMst.PhoneNo.Trim(),
                                        RegDate = tab1.UserMst.RegDate,
                                        UserID = tab1.UserID,
                                        isLogin = loggedInUsers.Contains(tab1.LoginName) ? true : false
                                    }).ToList()
                    .Select(a => new UserView
                    {
                        Name = a.Name,
                        Address = a.Address,
                        CDate = a.CDate,
                        //Created = a.Created,
                        Company = a.Company,
                        E_Stat = a.E_Stat,
                        LoginName = a.LoginName,
                        pwd=a.password,
                        MDate = a.MDate,
                        Type=a.Type,
                        Mobile = a.Mobile.Trim(),
                        //Modified = a.Modified,
                        PhoneNo = a.PhoneNo,
                        RegDate = a.RegDate,
                        UserID = a.UserID,
                          isLogin = a.isLogin
                    });

                    return View(loginmst);
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var loginmsts = (from loginmst in db.LoginMst
                                    
                                     join compmst in db.CompanyMst on loginmst.UserMst.CompID equals compmst.CompID
                                     where compmst.AdminID == SessionWrapper.AdminID && loginmst.E_Stat == true
                                     orderby loginmst.UserID
                                     select new
                                     {
                                         Name = loginmst.UserMst.Name,
                                         Address = loginmst.UserMst.Address,
                                         CDate = loginmst.UserMst.CDate,
                                         //Created = db.UserMst.FirstOrDefault(u => u.UserID == loginmst.UserMst.CUserID).Name,
                                         Company = compmst.Name,
                                         E_Stat = loginmst.UserMst.E_Stat,
                                         LoginName = loginmst.LoginName,
                                         password=loginmst.pwd,
                                         Type=loginmst.Type,
                                         MDate = loginmst.UserMst.MDate,
                                         Mobile = loginmst.UserMst.Mobile.Trim(),
                                         //Modified = db.UserMst.FirstOrDefault(u => u.UserID == loginmst.UserMst.MUserID).Name,
                                         PhoneNo = loginmst.UserMst.PhoneNo,
                                         RegDate = loginmst.UserMst.RegDate,
                                         
                                         UserID = loginmst.UserID,
                                         isLogin = loggedInUsers.Contains(loginmst.LoginName) ? true : false
                                     }).ToList()
                                    .Select(a => new UserView
                                    {
                                        Name = a.Name,
                                        Address = a.Address,
                                        CDate = a.CDate,
                                        //Created = a.Created,
                                        Company = a.Company,
                                        E_Stat = a.E_Stat,
                                        LoginName = a.LoginName,
                                        pwd=a.password,
                                        Type=a.Type,
                                        MDate = a.MDate,
                                        Mobile = a.Mobile.Trim(),
                                        //Modified = a.Modified,
                                        PhoneNo = a.PhoneNo,
                                        RegDate = a.RegDate,
                                       
                                        UserID = a.UserID,
                                        isLogin = a.isLogin
                                    });
                    return View(loginmsts);
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {

                    var loginmsts = (from loginmst in db.LoginMst
                                     
                                     join companymst in db.CompanyMst on loginmst.UserMst.CompID equals companymst.CompID
                                     where companymst.CompID.Equals(SessionWrapper.CompanyID) && (loginmst.Type == 1) && loginmst.E_Stat == true
                                     orderby loginmst.UserID
                                     select new
                                     {
                                         Name = loginmst.UserMst.Name,
                                         Address = loginmst.UserMst.Address,
                                         CDate = loginmst.UserMst.CDate,
                                         //Created = db.UserMst.FirstOrDefault(u => u.UserID == loginmst.UserMst.CUserID).Name,
                                         Company = companymst.Name,
                                         E_Stat = loginmst.UserMst.E_Stat,
                                         LoginName = loginmst.LoginName,
                                         password=loginmst.pwd,
                                         Type=loginmst.Type,
                                         MDate = loginmst.UserMst.MDate,
                                         Mobile = loginmst.UserMst.Mobile,
                                         //Modified = db.UserMst.FirstOrDefault(u => u.UserID == loginmst.UserMst.MUserID).Name,
                                         PhoneNo = loginmst.UserMst.PhoneNo,
                                         RegDate = loginmst.UserMst.RegDate,
                                         
                                         UserID = loginmst.UserID,
                                         isLogin = loggedInUsers.Contains(loginmst.LoginName) ? true : false
                                     }).ToList()
                                     .Select(a=>new UserView
                                     {
                                         Name = a.Name,
                                        Address = a.Address,
                                        CDate = a.CDate,
                                        //Created = a.Created,
                                        Company = a.Company,
                                        E_Stat = a.E_Stat,
                                        LoginName = a.LoginName,
                                        pwd=a.password,
                                        Type=a.Type,
                                        MDate = a.MDate,
                                        Mobile = a.Mobile,
                                        //Modified = a.Modified,
                                        PhoneNo = a.PhoneNo,
                                        RegDate = a.RegDate,
                                        
                                        UserID = a.UserID,
                                         isLogin = a.isLogin
                                    });
                                     
                    return View(loginmsts);

                }
                
            else
            {
                return View("UnAuthenticated");
            }
        }


        //
        // GET: /Usermanage/Details/5
       [Audit]
        public ActionResult Details(long? id)
        {
            if (SessionWrapper.IsAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            {
                UserMaster usr = db.UserMst.Find(id);
                LoginMaster lgn = db.LoginMst.Find(usr.UserID);
                return PartialView(new UserView
                                  {
                                      Name = usr.Name,
                                      Address = usr.Address,
                                      CDate = usr.CDate,
                                      pwd = lgn.pwd,
                                      Company = db.CompanyMst.FirstOrDefault(u => u.CompID == usr.CompID).Name,
                                      E_Stat = usr.E_Stat,
                                      LoginName = lgn.LoginName,
                                      MDate = usr.MDate,
                                      Mobile = usr.Mobile,

                                      PhoneNo = usr.PhoneNo,
                                      RegDate = usr.RegDate,
                                  });
            }
            else
            {
                return View("UnAuthenticated");
            }
            
        }

        //
        // GET: /Usermanage/Create
      
        public ActionResult Create()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin||SessionWrapper.AdminLevel==AdminLevel.CompanyAdmin)
            {
                getdropdows();
                return PartialView();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        //
        // POST: /Usermanage/Create

        [HttpPost,Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoginMaster loginmaster)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin||SessionWrapper.AdminLevel==AdminLevel.CompanyAdmin)
            {
                //if (loginmaster.UserMst.CompID == 0)
                //{
                //    ModelState.AddModelError("CompID", "Select Company ! ");
                //}
                if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    if (loginmaster.UserMst.CompID == 0)
                    {
                        ModelState.AddModelError("CompID", "Select Company ! ");
                    }
                }
                else
                {

                    loginmaster.UserMst.CompID = SessionWrapper.CompanyID;
                }
                //loginmaster.Type = 1;
                //loginmaster.E_Stat = true;

                ////normal user
                //loginmaster.RoleID = 2;
                if (ModelState.IsValid)
                {
                    //created date
                    DateTime cdate = DateTime.Now;
                    loginmaster.CDate = cdate;
                    loginmaster.UserMst.CDate = cdate;
                    loginmaster.UserMst.MDate = cdate;
                    loginmaster.UserMst.CType = (int)SessionWrapper.AdminLevel;
                    loginmaster.UserMst.MType = (int)SessionWrapper.AdminLevel;

                    if (SessionWrapper.IsAdmin)
                    {
                        loginmaster.UserMst.CUserID = SessionWrapper.AdminID;
                        loginmaster.UserMst.MUserID = SessionWrapper.AdminID;
                    }
                    else
                    {
                        loginmaster.UserMst.CUserID = SessionWrapper.UserID;
                        loginmaster.UserMst.MUserID = SessionWrapper.UserID;
                    }


                    loginmaster.pwd = loginmaster.Password;
                    //encript password
                    loginmaster.Password = GetMD5Hash(loginmaster.Password);
                    
                    //setting status
                    loginmaster.UserMst.E_Stat = true;
                    //if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    //{
                    //    loginmaster.UserMst.CompID = SessionWrapper.CompanyID;
                    //} 
                    loginmaster.Type = 1;
                    loginmaster.E_Stat = true;

                    //normal user
                    loginmaster.RoleID = 2;
                   
                    LoginMaster login = loginmaster;
                    db.UserMst.Add(loginmaster.UserMst);
                    db.SaveChanges();
                    //userid to loginmaster
                    loginmaster.UserID = loginmaster.UserMst.UserID;
                    db.LoginMst.Add(loginmaster);
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }
                getdropdows();
                return View(loginmaster);
            }
            else
            {
                return View("UnAuthenticated");
            }

        }
        public ActionResult Loginvalid(string LoginName, string UserID)
        {
            if (UserID == "undefined" || UserID == null)
            {
               // var z = (db.LoginMst.Select(p=>p.LoginName==LoginName).Count());

                //var l = db.LoginMst.Count(j => j.LoginName==LoginName);
                //LoginMaster ln = new LoginMaster();
                //if (ln.LoginName==LoginName)
                JsonResult a = Json(!(db.LoginMst.Count(e => e.LoginName == LoginName) > 0), JsonRequestBehavior.AllowGet);
                return a;
                
                //else
                //{
                //    JsonResult a = Json(!(false), JsonRequestBehavior.AllowGet);
                //    return a;
                //}
            }
            else
            {
                JsonResult a = Json(!(false), JsonRequestBehavior.AllowGet);
                return a;
            }
        }
        //
        // GET: /Usermanage/Edit/5
      
        public ActionResult Edit(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            {
            getEditdropdows(id);

           

                LoginMaster loginmaster = db.LoginMst.SingleOrDefault(l => l.UserID == id);
                //loginmaster.UserMst.PhoneNo = loginmaster.UserMst.PhoneNo.Trim();
                //loginmaster.UserMst.Mobile = loginmaster.UserMst.Mobile.Trim();
                return PartialView(loginmaster);
            }
            else
            {
                return View("UnAuthenticated");
            }

        }

        //
        // POST: /Usermanage/Edit/5

        [HttpPost,Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoginMaster loginmaster)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            {
                LoginMaster objloginmaster = db.LoginMst.Find(loginmaster.LoginID);

                //objloginmaster.LoginID = objloginmaster.LoginID;
                objloginmaster.LoginName = objloginmaster.LoginName;
                objloginmaster.Password = objloginmaster.Password;
                objloginmaster.Type = loginmaster.Type;
                objloginmaster.UserID = objloginmaster.UserID;
               
                objloginmaster.E_Stat = loginmaster.E_Stat;

                objloginmaster.UserMst.MDate = DateTime.UtcNow;
                objloginmaster.UserMst.CDate = objloginmaster.UserMst.CDate;
                objloginmaster.UserMst.CType = objloginmaster.UserMst.CType;
                objloginmaster.UserMst.MType = (int)SessionWrapper.AdminLevel;
                objloginmaster.UserMst.E_Stat = loginmaster.E_Stat;
                //objloginmaster.UserMst.E_Stat = true;
                objloginmaster.UserMst.CUserID = objloginmaster.UserMst.CUserID;
                if (SessionWrapper.IsAdmin)
                { objloginmaster.UserMst.MUserID = (int)SessionWrapper.AdminID; }
                else
                { objloginmaster.UserMst.MUserID = (int)SessionWrapper.UserID; }

                objloginmaster.UserMst.CompID = objloginmaster.UserMst.CompID;
                objloginmaster.UserMst.RegDate = objloginmaster.UserMst.RegDate;
                objloginmaster.UserMst.Address = loginmaster.UserMst.Address;
                objloginmaster.UserMst.Mobile = loginmaster.UserMst.Mobile;
                objloginmaster.UserMst.Name = loginmaster.UserMst.Name;
                objloginmaster.UserMst.PhoneNo = loginmaster.UserMst.PhoneNo;
                //objloginmaster.UserMst.Lat = loginmaster.UserMst.Lat;
                //objloginmaster.UserMst.Lng = loginmaster.UserMst.Lng;

                if (ModelState.IsValid)
                {
                    db.Entry(objloginmaster.UserMst).State = EntityState.Modified;
                    db.Entry(objloginmaster).State = EntityState.Modified;

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                getdropdows();
                return View(loginmaster);
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        //
        // GET: /Usermanage/Delete/5
     
        public ActionResult Delete(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin ||SessionWrapper.AdminLevel==AdminLevel.CompanyAdmin)
            {
                LoginMaster loginmaster = db.LoginMst.SingleOrDefault(l => l.UserID == id);
                return PartialView(loginmaster);
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        //
        // POST: /Usermanage/Delete/5

        [HttpPost,Audit, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            {
                LoginMaster loginmaster = db.LoginMst.Find(id);
                db.Database.ExecuteSqlCommand("exec sp_delete_usrlgn @userid", new SqlParameter("@userid", id));
                //loginmaster.UserMst.E_Stat = loginmaster.E_Stat;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public void getdropdows()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {

                //ViewBag.CompID = new SelectList(db.CompanyMst, "CompID", "Name", "--Select Department--");
                ViewBag.CompID = new SelectList("", "CompID", "Name");
                var com = from comp in db.CompanyMst where comp.AdminID == SessionWrapper.AdminID && comp.E_Stat==true select new { comp.CompID, comp.Name };
                ViewBag.CompID = new SelectList(com, "CompID", "Name", "--Select Company--");
                //ViewBag.DeptID = new SelectList("", "DeptID", "Name");
            }
            else if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                ViewBag.CompID = new SelectList(from a in db.CompanyMst where a.E_Stat==true select a, "CompID", "Name", "--Select Company--");
       
            }

        }

        public void getEditdropdows(long id)
        {
            //ViewBag.RoleID = new SelectList(db.RoleMst, "RoleID", "Name", "--Select Role--");
            if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                var com = from comp in db.CompanyMst where comp.AdminID == SessionWrapper.AdminID select new { comp.CompID, comp.Name };
                ViewBag.CompID = new SelectList(com, "CompID", "Name", "--Select Company--");
            }

        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}