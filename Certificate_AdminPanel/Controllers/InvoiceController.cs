﻿using ReportManagement;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Certificate_AdminPanel.Models;

//using RazorPDF;
namespace Certificate_AdminPanel.Controllers
{
    public class InvoiceController : PdfViewController
    {
        //
        // GET: /Invoice/
        public ActionResult Index()
        {
            using (DatContext db = new DatContext())
            {
                return View(db.invoice.ToList());
            }
        }

        public ActionResult Index_Create()
        {
            using (DatContext db = new DatContext())
            {
               // var tot = db.invoice.Where(d => d.I_stat == false).Select(a => a.UnitPrice).ToList().Aggregate(db.invoice.Where(s => s.I_stat == false).Select(m => m.ShippingPrice).First(), (a, b) => a + b);
                var res = from a in db.invoice
                          where a.I_stat == false
                          select new
                              InvoiceView
                          {
                              RecNo = a.RecNo,
                              InvoiceNo = a.InvoiceNo,
                              InvoiceDate = a.InvoiceDate,
                              PurchaseOrderNo = a.PurchaseOrderNo,
                              PurchaseDate = a.PurchaseDate,
                              QuantityOrdered = a.QuantityOrdered,
                              QuantityShipped = a.QuantityShipped,
                              UnitPrice = a.UnitPrice,
                              TotalUnit=(a.UnitPrice)*(a.QuantityShipped),
                              ShippingPrice = a.ShippingPrice,
                              Description = a.Description,
                              BuyerAddress = a.BuyerAddress,
                              //Total = db.invoice.Where(d => d.I_stat == false).Select(v => v.UnitPrice).ToList().Aggregate(db.invoice.Where(s => s.I_stat == false).Select(m => m.ShippingPrice).First(), (v, b) => v + b)
                              // public bool I_stat { get; set; }
                              //         };
                          };
                return View(res.ToList());
            }
 
        }
        /// <summary>
        /// After adding the materials, display the details ready to print invoice
        /// </summary>
        /// <returns></returns>
        public ActionResult Details()
        {
            using (DatContext db = new DatContext())
            {
                try
                {
                    var result1 = from a in db.invoice where a.I_stat == false select a;
                    //string result = db.invoice.Where(a => a.I_stat == false).Select(a => a.InvoiceNo).First();
                    //var r = from a in db.invoice where a.I_stat == true select a;
                    var tot = db.invoice.Where(d=>d.I_stat==false).Select(a => a.UnitPrice).ToList().Aggregate(db.invoice.Where(s=>s.I_stat==false).Select(m=>m.ShippingPrice).First(),(a, b) => a + b);
                    var res = from a in db.invoice
                              where a.I_stat == false
                              select new
                                  InvoiceView
                              {
                                  RecNo = a.RecNo,
                                  InvoiceNo = a.InvoiceNo,
                                  InvoiceDate = a.InvoiceDate,
                                  PurchaseOrderNo = a.PurchaseOrderNo,
                                  PurchaseDate = a.PurchaseDate,
                                  QuantityOrdered = a.QuantityOrdered,
                                  QuantityShipped = a.QuantityShipped,
                                  UnitPrice = a.UnitPrice,
                                  ShippingPrice = a.ShippingPrice,
                                  Description = a.Description,
                                  Buyer = a.Buyer,
                                  BuyerAddress = a.BuyerAddress,
                                  BuyerAddress1 = a.BuyerAddress1,
                                  BuyerAddress2 = a.BuyerAddress2,
                                 // ImageUrl=string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "/Content/AJOL1.jpg"),

                                  Total = tot
                                  // public bool I_stat { get; set; }
                                  //         };
                              };
                   
                        //var x=string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "/Content/AJOL1.jpg");

                        //InvoiceView invoice = res.FirstOrDefault(a => a.I_stat == db.invoice.Where(a => a.I_stat == false).First());
                    //invoice.ImageUrl = string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "/Content/AJOL1.jpg");
                    //FillImageUrl(res, "AJOL1.jpg");
                    // db.Database.ExecuteSqlCommand("update tbl_invoice set i_stat=1 where invoiceno="+result);
                    return View(res);
                    //return this.ViewPdf("certiifate", "Details", res);
                    //return new Rotativa.ActionAsPdf("Details",res);
                }
                catch(Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
                //return new PdfResult(result, "Index");
                return View();
            }
        }
        /// <summary>
        /// Invoice which is printing in pdf 
        /// </summary>
        /// <returns></returns>
        public ActionResult Invoice()
        {
            using (DatContext db = new DatContext())
            {

               // var tot = db.invoice.Where(d => d.I_stat == false).Select(a => a.UnitPrice).ToList().Aggregate(db.invoice.Where(s => s.I_stat == false).Select(m => m.ShippingPrice).First(), (a, b) => a + b);
                var res = from a in db.invoice
                          where a.I_stat == false
                          select new
                              InvoiceView
                          {
                              RecNo = a.RecNo,
                              InvoiceNo = a.InvoiceNo,
                              InvoiceDate = a.InvoiceDate,
                              PurchaseOrderNo = a.PurchaseOrderNo,
                              PurchaseDate = a.PurchaseDate,
                              QuantityOrdered = a.QuantityOrdered,
                              QuantityShipped = a.QuantityShipped,
                              UnitPrice = a.UnitPrice,
                              ShippingPrice = a.ShippingPrice,
                              Description = a.Description,
                              Buyer = a.Buyer,
                              BuyerAddress = a.BuyerAddress,
                              BuyerAddress1 = a.BuyerAddress1,
                              BuyerAddress2 = a.BuyerAddress2,
                              // ImageUrl=string.Format("{0}://{1}{2}{3}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"), "/Content/AJOL1.jpg"),

                              TotalUnit = a.UnitPrice*a.QuantityShipped
                              // public bool I_stat { get; set; }
                              //         };
                          };
                return View(res.ToList());
            }
        }

        public ActionResult Pdf()
        
        {
            using (DatContext db = new DatContext())
            {
                return new Rotativa.ActionAsPdf("Invoice");// { FileName="invoice.pdf"};
            }
        }


        public ActionResult Pdf1(int id)
        {
            using (DatContext db = new DatContext())
            {
                return new ActionAsPdf("reGenerate",new{id=id});
                //return new ActionAsPdf("reGenerate",new { id = id }) { FileName = "Invoice.pdf" };

            }
        }
        //public ActionResult Details1(int id)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        var res = from a in db.invoice where a.RecNo==id
        //                  select new
        //                      InvoiceView
        //                      {
        //                          RecNo = a.RecNo,
        //                          InvoiceNo = a.InvoiceNo,
        //                          InvoiceDate = a.InvoiceDate,
        //                          PurchaseOrderNo=a.PurchaseOrderNo,
        //                          PurchaseDate =a.PurchaseDate,
        //                          QuantityOrdered=a.QuantityOrdered,
        //                          QuantityShipped =a.QuantityShipped,
        //                          UnitPrice=a.UnitPrice,
        //                          ShippingPrice=a.ShippingPrice,
        //                          Description=a.Description,
        //                          BuyerAddress=a.BuyerAddress,
        //                          // public bool I_stat { get; set; }
        //                          //         };
        //                      };
       
        //        InvoiceView inv=res.SingleOrDefault(a=>a.RecNo==id);

        //        return this.ViewPdf("Invoice","Details",inv);
        //    }
        
        //}
        //public ActionResult Pdf()
        //{//DatContext db=new DatContext();

        //    var invoice = new Invoice();
        //    invoice.InvoiceNo ="123" ;
        //    invoice.PurchaseOrderNo ="123";
        //   // var pdfResult = new PdfResult(invoice, "Pdf");
        //   // pdfResult.ViewBag.Title = "Title";
        //    //return pdfResult;
        //}
        //
        //Get:Create
        public ActionResult Create()
        {
            dropdown();
            return View();
        }

        private void dropdown()
        {
            using (DatContext db = new DatContext())
            {
                var dba = from a in db.SpeedLimiterSerialNo where a.E_Stat ==false select a;
                var lid = db.SpeedLimiterSerialNo.Where(a => a.E_Stat == false).Select(l => l.SerialNo).ToList();
                ViewBag.cont = new MultiSelectList(dba.ToList(), "SerialNO", "SerialNO");
                //List<Country> country = db.countries.ToList();
                //ViewBag.country = new MultiSelectList(country, "CountryCode", "CountryName");
            }
        }
        [HttpPost]
        public ActionResult Create(InvoiceView invoice)
        {
            using (DatContext db = new DatContext())
            {
                Invoice _invoice = new Invoice();
                _invoice.BuyerAddress = invoice.BuyerAddress;
                _invoice.Buyer = invoice.Buyer;
                _invoice.BuyerAddress1 = invoice.BuyerAddress1;
                _invoice.BuyerAddress2 = invoice.BuyerAddress2;
                _invoice.Description = invoice.Description;               
               
                _invoice.PurchaseDate = invoice.PurchaseDate;
                _invoice.PurchaseOrderNo = invoice.PurchaseOrderNo;
                _invoice.QuantityOrdered = invoice.QuantityOrdered;
                _invoice.QuantityShipped = invoice.QuantityShipped;              
                _invoice.ShippingPrice = invoice.ShippingPrice;
                _invoice.UnitPrice = invoice.UnitPrice;
                int certnum = (Convert.ToInt32(db.invoice_flag.SingleOrDefault(a => a.Name == "inv").Value));
                if (db.invoice.Where(a => a.I_stat == false).Count() > 0)
                {
                    _invoice.InvoiceNo = db.invoice.Where(a => a.I_stat == false).Select(a => a.InvoiceNo).First();
                }
                else
                {
                    certnum=certnum + 1;
                    _invoice.InvoiceNo = string.Format("{0}/{1}/{2}", "AJ", DateTime.Now.Year.ToString().Substring(2, 2), certnum.ToString().PadLeft(3, '0'));
                }
                _invoice.I_stat = false;
                _invoice.InvoiceDate = DateTime.Now.Date;
                
                if (ModelState.IsValid)
                {                   
                    db.invoice.Add(_invoice);
                    db.SaveChanges();
                    InvoiceFlag inv=db.invoice_flag.Find(1);
                    inv.Value = certnum;
                    db.Entry(inv).State=EntityState.Modified;
                    db.SaveChanges();
                    if (invoice.SerialNO!=null)
                    {
                        foreach (var item in invoice.SerialNO)
                        {
                            db.Database.ExecuteSqlCommand("Update SpeedLimiterSerialNo set invoiceno=" + "'" + _invoice.InvoiceNo + "'" + "where SerialNo=" + "'" + item + "'");
                        }
                    }
                    return RedirectToAction("Index_Create");
                }
                dropdown();
                return View();
            }
        }
        //private void FillImageUrl(InvoiceView inv, string imageName)
        //{
           
        //    string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
        //    inv.ImageUrl = url + imageName;
        //    //}
        //}
        ////////////public ActionResult InvSerial()
        ////////////{
        ////////////    using(DatContext db=new DatContext())
        ////////////    {
        ////////////        var res = from a in db.SpeedLimiterSerialNo where a.DealerId == 12 && a.InvoiceNo!=null select a;
        ////////////        return View(res.ToList());
        ////////////    }
        ////////////}

        /// <summary>
        /// For Generating invoice again
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult reGenerate(int id)
        {
            using (DatContext db = new DatContext())
            { //string certnum = (Convert.ToInt32(db.CommonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();

                if (db.invoice.Where(a => a.RecNo == id).Count() > 0)
                {
                    var resi = db.invoice.Find(id).InvoiceNo;
                    //var tot = db.invoice.Where(l => l.InvoiceNo == resi).Select(a => a.UnitPrice).ToList().Aggregate(db.invoice.Find(id).ShippingPrice, (a, b) => a + b);
                    var res = from a in db.invoice
                              where a.InvoiceNo == resi
                              //select new
                              //{
                              //    a.RecNo,
                              //    a.PurchaseOrderNo,
                              //    a.PurchaseDate,
                              //    a.InvoiceNo,
                              //    a.InvoiceDate,
                              //    a.QuantityShipped,
                              //    a.QuantityOrdered,
                              //    a.UnitPrice,
                              //    a.ShippingPrice as ,
                              //    a.Description,
                              //    a.BuyerAddress
                              //}; 
                              select new
                              InvoiceView
                              {
                                  RecNo = a.RecNo,
                                  InvoiceNo = a.InvoiceNo,
                                  InvoiceDate = a.InvoiceDate,
                                  PurchaseOrderNo = a.PurchaseOrderNo,
                                  PurchaseDate = a.PurchaseDate,
                                  QuantityOrdered = a.QuantityOrdered,
                                  QuantityShipped = a.QuantityShipped,
                                  UnitPrice = a.UnitPrice,
                                  ShippingPrice = a.ShippingPrice,
                                  Description = a.Description,
                                  Buyer = a.Buyer,
                                  BuyerAddress = a.BuyerAddress,
                                  BuyerAddress1 = a.BuyerAddress1,
                                  BuyerAddress2 = a.BuyerAddress2,
                                  TotalUnit = a.QuantityShipped * a.UnitPrice
                              };
                    return View(res.ToList());
                }
                else
                {
                    @TempData["Message"] = "No Data Found";
                    return View("Error");
                    
                }
                //return this.ViewPdf("Invoice", "Details", res.ToList()); 
                //return View("pdf",res.ToList());
                //return RedirectToAction("pdf", new { id=});

            }
 
        }
        /// <summary>
        /// For Updating i_stat from 0 to 1 after invoice is generated
        /// </summary>
        /// <returns></returns>
        //[WebMethod]
        public ActionResult Update()
        {
            using (DatContext db = new DatContext())
            {
                db.Database.ExecuteSqlCommand("update tbl_invoice set i_stat=1 where i_stat=0");
                return RedirectToAction("index_create");
            }
        }

        //
        //Get: Edit/id
        public ActionResult Edit(long?id)
        {
            using (DatContext db = new DatContext())
            {
                var re=db.invoice.Find(id);
                return View(re);
            }
        }
        //
        //POST:Edit
        [HttpPost]
        public ActionResult Edit(Invoice inv)
        {
            using (DatContext db = new DatContext())
            {
                Invoice _inv = db.invoice.Find(inv.RecNo);
                if(ModelState.IsValid)
                {
                    db.Entry(_inv).State=EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(inv);
            }
        }
    }
}
