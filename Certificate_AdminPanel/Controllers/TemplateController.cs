﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;

namespace Certificate_AdminPanel.Controllers
{
    public class TemplateController : Controller
    {
        //private DatContext db = new DatContext();

        //
        // GET: /Template/
        [Audit]
        public ActionResult Index()
        {
            using(DatContext db=new DatContext())
            {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                
                return View(db.Template.ToList());
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        }

        //
        // GET: /Template/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    Template template = db.Template.Find(id);
        //    if (template == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(template);
        //}

        //
        // GET: /Template/Create
      
        public ActionResult Create()
        {
            using (DatContext db = new DatContext())
            {
                return PartialView();
            }
        }

        //
        // POST: /Template/Create

        [HttpPost,Audit]
        public ActionResult Create(Template template)
        {
            using (DatContext db = new DatContext())
            {

                if (ModelState.IsValid)
                {
                    db.Template.Add(template);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(template);
            }
        }

        //
        // GET: /Template/Edit/5
       
        public ActionResult Edit(int id = 0)
        {
            using (DatContext db = new DatContext())
            {
                Template template = db.Template.Find(id);
                if (template == null)
                {
                    return HttpNotFound();
                }
                return PartialView(template);
            }
        }

        //
        // POST: /Template/Edit/5

        [HttpPost,Audit]
        public ActionResult Edit(Template template)
        {
            using (DatContext db = new DatContext())
            {
                if (ModelState.IsValid)
                {
                    db.Entry(template).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(template);
            }
        }

        //
        // GET: /Template/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        Template template = db.Template.Find(id);
        //        if (template == null)
        //        {
        //            return HttpNotFound();
        //        }
        //        return View(template);
        //    }
        //}

        //
        // POST: /Template/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    using (DatContext db = new DatContext())
        //    {
        //        Template template = db.Template.Find(id);
        //        db.Template.Remove(template);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //}

        protected override void Dispose(bool disposing)
        {
            using (DatContext db = new DatContext())
            {
                db.Dispose();
                base.Dispose(disposing);
            }
        }
    }
}