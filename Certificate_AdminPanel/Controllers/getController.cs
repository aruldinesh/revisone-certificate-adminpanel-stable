﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;

namespace Certificate_AdminPanel.Controllers
{
    public class getController : Controller
    {
        //
        // GET: /get/

        public ActionResult Index()
        {
            return View();

        }


        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]

        public ActionResult GetDet(string searchString)
        {

            using (DatContext db = new DatContext())
            {

                var obj = from certificate in db.certificate_mst
                          join country in db.countries
                          on certificate.Country equals country.CountryCode
                          join company in db.CompanyMst on
                          certificate.DealerId equals company.CompID
                          where certificate.E_Stat == true && certificate.CertificateNo.Trim() == searchString.Trim()
                          select new CertificateView
                          {
                              CertificateNo = certificate.CertificateNo,
                              VehRegistrationNo = certificate.VehRegistrationNo,
                              OwnerName = certificate.OwnerName,
                              ChassisNo = certificate.ChassisNo,
                              EngineNo = certificate.EngineNo,
                              ManufactureYear = certificate.ManufactureYear,
                              MakeOfVehicle = certificate.MakeOfVehicle,
                              ModelOfVehicle = certificate.ModelOfVehicle,
                              TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                              SetSpeed = certificate.SetSpeed,
                              speed2 = certificate.speed2,
                              SerialNo = certificate.SerialNo,
                              TamperSealNo = certificate.TamperSealNo,
                              InstallDate = certificate.InstallDate,
                              CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate,
                              ExpDate = certificate.ExpDate,
                              TechnicianName = certificate.TechnicianName,
                              ApplicableStandard = certificate.ApplicableStandard,
                              Country = certificate.Country,
                              Cdate = certificate.Cdate,
                              Cuser = certificate.Cuser,
                              Mdate = certificate.Mdate,
                              Muser = certificate.Muser,
                              E_Stat = certificate.E_Stat,
                              Duplicate = certificate.Duplicate,
                              DealerId = certificate.DealerId,
                              CompID = company.CompID,
                              Name = company.Name,
                              Address = company.Address,
                              PhoneNo = company.PhoneNo,
                              CountryName = country.CountryName,
                              Expired = EntityFunctions.DiffDays(certificate.ExpDate, DateTime.UtcNow) > 0 ? "Expired" : "Valid"

                          };

                CertificateView cobj = obj.SingleOrDefault(u => u.CertificateNo == searchString);

                return View(cobj);

            }
        }
    }
}