﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Validation;
using System.Diagnostics;
using ReportManagement;
using Certificate_AdminPanel.Repository;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;

namespace Certificate_AdminPanel.Controllers
{
    public class GPSCertifcateController : PdfViewController
    {
        //
        // GET: /GPSCertifcate/
        [Authorize]
        public ActionResult Index()
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                         {
                             var cn = from a in db.gpscertificate
                                      join c in db.countries on a.Country equals c.CountryCode
                                      join b in db.CompanyMst on a.DealerId equals b.CompID
                                     // where a.DealerId ==cer.Dealerdd
                                      select new GPSCertificateView
                                            {
                                                CertificateNo = a.CertificateNo,
                                                VehicleNo = a.VehicleNo!=""?a.VehicleNo:"N/A",
                                                OwnerName = a.OwnerName,
                                                ChassisNo = a.ChassisNo,
                                                EngineNo = a.EngineNo,
                                                
                                                MakeOfVehicle = a.Makeofvehicle,
                                                ModelOfVehicle = a.modelofvehicle,
                                               // TypeSpeedLimiter = a.TypeSpeedLimiter,
                                                Maxspeed = a.Maxspeed,
                                                TerminalId = a.TerminalId,
                                               // TamperSealNo = a.TamperSealNo,
                                                InstallDate = a.InstallDate,
                                                ExpDate = a.ExpDate,
                                                TechnicianName = a.TechnicianName,
                                                ApplicableStandard = a.ApplicableStandard,
                                                //Country = a.Country,
                                                Cdate = a.Cdate,
                                                Cuser = a.CUser,
                                               
                                                E_stat = a.E_stat,
                                                Duplicate = a.Duplicate,
                                                DealerId = a.DealerId,
                                                //CompID=comp.CompID,
                                                Name = b.DisplayName,
                                                CountryName = c.CountryName,
                                                //CalibrationDate = a.CalibrationDate

                                            };
                             CompanyName();
                             return View(cn.OrderByDescending(z => z.Cdate).ToList());
                             //Offline storage
                             //CommonData.JsonGpsDPush(cn.OrderByDescending(z => z.Cdate).ToList());

                             //return View();
                         }

                         else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                         {
                             var cn = from a in db.gpscertificate
                                      join c in db.countries on a.Country equals c.CountryCode
                                      join b in db.CompanyMst on a.DealerId equals b.CompID
                                      where b.AdminID == SessionWrapper.AdminID //&& a.DealerId == cer.Dealerdd
                                      select new GPSCertificateView
                                             {
                                                 CertificateNo = a.CertificateNo,
                                                VehicleNo = a.VehicleNo!=""?a.VehicleNo:"N/A",
                                                OwnerName = a.OwnerName,
                                                ChassisNo = a.ChassisNo,
                                                EngineNo = a.EngineNo,
                                                
                                                MakeOfVehicle = a.Makeofvehicle,
                                                ModelOfVehicle = a.modelofvehicle,
                                               // TypeSpeedLimiter = a.TypeSpeedLimiter,
                                                Maxspeed = a.Maxspeed,
                                                TerminalId = a.TerminalId,
                                               // TamperSealNo = a.TamperSealNo,
                                                InstallDate = a.InstallDate,
                                                ExpDate = a.ExpDate,
                                                TechnicianName = a.TechnicianName,
                                                ApplicableStandard = a.ApplicableStandard,
                                                //Country = a.Country,
                                                Cdate = a.Cdate,
                                                Cuser = a.CUser,
                                               
                                                E_stat = a.E_stat,
                                                Duplicate = a.Duplicate,
                                                DealerId = a.DealerId,
                                                //CompID=comp.CompID,
                                                Name = b.DisplayName,
                                                CountryName = c.CountryName,

                                             };
                             CompanyName();
                             return View(cn.OrderByDescending(z => z.Cdate).ToList());
                             //Offline storage
                             //CommonData.JsonGpsDPush(cn.OrderByDescending(z => z.Cdate).ToList());

                             //return View();
                         }
                         else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                         {
                             var cn = from a in db.gpscertificate
                                      join c in db.countries on a.Country equals c.CountryCode
                                      join b in db.CompanyMst on a.DealerId equals b.CompID
                                      where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                     // && a.DealerId == cer.Dealerdd
                                      select new GPSCertificateView
                                      {
                                          CertificateNo = a.CertificateNo,
                                                VehicleNo = a.VehicleNo!=""?a.VehicleNo:"N/A",
                                                OwnerName = a.OwnerName,
                                                ChassisNo = a.ChassisNo,
                                                EngineNo = a.EngineNo,
                                                
                                                MakeOfVehicle = a.Makeofvehicle,
                                                ModelOfVehicle = a.modelofvehicle,
                                               // TypeSpeedLimiter = a.TypeSpeedLimiter,
                                                Maxspeed = a.Maxspeed,
                                                TerminalId = a.TerminalId,
                                               // TamperSealNo = a.TamperSealNo,
                                                InstallDate = a.InstallDate,
                                                ExpDate = a.ExpDate,
                                                TechnicianName = a.TechnicianName,
                                                ApplicableStandard = a.ApplicableStandard,
                                                //Country = a.Country,
                                                Cdate = a.Cdate,
                                                Cuser = a.CUser,
                                               
                                                E_stat = a.E_stat,
                                                Duplicate = a.Duplicate,
                                                DealerId = a.DealerId,
                                                //CompID=comp.CompID,
                                                Name = b.DisplayName,
                                                CountryName = c.CountryName,
                                      };
                             CompanyName();
                             return View(cn.OrderByDescending(z => z.Cdate).ToList());
                             //Offline storage
                             //CommonData.JsonGpsDPush(cn.OrderByDescending(z => z.Cdate).ToList());

                             //return View();
                         }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                         {
                             //var cert = from a in db.certificate_mst join c in db.countries
                             //           on a.Country equals c.CountryCode join b in db.CompanyMst
                             //           on a.DealerId equals b.CompID select new 
                             //           CertificateView{}
                             //var cert = db.certificate_mst.Include(c => c.con);
                             //cert = from a in cert where a.DealerId == SessionWrapper.CompanyID select a;
                             var cn = from a in db.gpscertificate
                                      join c in db.countries on a.Country equals c.CountryCode
                                      join b in db.CompanyMst on a.DealerId equals b.CompID
                                      where b.CompID == SessionWrapper.CompanyID
                                      select new GPSCertificateView
                                      {
                                          CertificateNo = a.CertificateNo,
                                          VehicleNo = a.VehicleNo != "" ? a.VehicleNo : "N/A",
                                          OwnerName = a.OwnerName,
                                          ChassisNo = a.ChassisNo,
                                          EngineNo = a.EngineNo,

                                          MakeOfVehicle = a.Makeofvehicle,
                                          ModelOfVehicle = a.modelofvehicle,
                                          // TypeSpeedLimiter = a.TypeSpeedLimiter,
                                          Maxspeed = a.Maxspeed,
                                          TerminalId = a.TerminalId,
                                          // TamperSealNo = a.TamperSealNo,
                                          InstallDate = a.InstallDate,
                                          ExpDate = a.ExpDate,
                                          TechnicianName = a.TechnicianName,
                                          ApplicableStandard = a.ApplicableStandard,
                                          //Country = a.Country,
                                          Cdate = a.Cdate,
                                          Cuser = a.CUser,

                                          E_stat = a.E_stat,
                                          Duplicate = a.Duplicate,
                                          DealerId = a.DealerId,
                                          //CompID=comp.CompID,
                                          Name = b.DisplayName,
                                          CountryName = c.CountryName,

                                      };

                            return View(cn.OrderByDescending(z => z.Cdate).ToList());
                             //Offline storage
                             //CommonData.JsonGpsDPush(cn.OrderByDescending(z => z.Cdate).ToList());

                             //return View();

                         }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID

                             where a.CUser == SessionWrapper.UserID
                             select new GPSCertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehicleNo = a.VehicleNo != "" ? a.VehicleNo : "N/A",
                                 OwnerName = a.OwnerName,
                                 ChassisNo = a.ChassisNo,
                                 EngineNo = a.EngineNo,

                                 MakeOfVehicle = a.Makeofvehicle,
                                 ModelOfVehicle = a.modelofvehicle,
                                 // TypeSpeedLimiter = a.TypeSpeedLimiter,
                                 Maxspeed = a.Maxspeed,
                                 TerminalId = a.TerminalId,
                                 // TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 TechnicianName = a.TechnicianName,
                                 ApplicableStandard = a.ApplicableStandard,
                                 //Country = a.Country,
                                 Cdate = a.Cdate,
                                 Cuser = a.CUser,

                                 E_stat = a.E_stat,
                                 Duplicate = a.Duplicate,
                                 DealerId = a.DealerId,
                                 //CompID=comp.CompID,
                                 Name = b.DisplayName,
                                 CountryName = c.CountryName,

                             };

                    return View(cn.OrderByDescending(z => z.Cdate).ToList());
                    //Offline storage
                    //CommonData.JsonGpsDPush(cn.OrderByDescending(z => z.Cdate).ToList());

                    //return View();
                }
                else
                {
                    return View("UnAuthenticated");
                }
                     
                   
                             //CompanyName();
                             //return View(cn.OrderByDescending(z => z.Cdate).ToList());
                        // }

                     //return View(db.gpscertificate.ToList());
            }

           
        }
        [Authorize]
        public ActionResult Create()
        {
            if (SessionWrapper.IsAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin || SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
            {
                dropdown();
                CompanyName();
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        [HttpPost,Authorize]
        public ActionResult Create(GPSCertificate gps_certificate)
        {
            using (DatContext db = new DatContext())
            {
                //if (ModelState.IsValid)
                //{
                //    db.gpscertificate.Add(gps_certificate);

                //    db.SaveChanges();
                //    return RedirectToAction("Index");
                //}

                var Exist_exp = from a in db.gpscertificate where a.ExpDate > DateTime.UtcNow select a;
                var Exist_Estat = from a in db.gpscertificate where a.E_stat == true select a;
                GPSCertificate cert = Exist_Estat.SingleOrDefault(c => c.VehicleNo == gps_certificate.VehicleNo);
              
                if (gps_certificate.VehicleNo != null && gps_certificate.VehicleNo.Trim().ToUpper() == "DEMO")
                                 
                    {
                        GPSCertificateView cv = new GPSCertificateView();

                        if (SessionWrapper.IsAdmin)
                        {
                            gps_certificate.CUser = SessionWrapper.AdminID;
                            ///gps_certificate.Muser = SessionWrapper.AdminID;
                            AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                            Template temp = db.Template.Find(adminmaster.TemplateID);
                            cv.AdminID = adminmaster.AdminID;
                            cv.AdminName = adminmaster.DisplayName.ToUpper();
                            cv.AdminAddress = adminmaster.Address;
                            cv.AdminLogo = adminmaster.Logo;
                            cv.AdminPhone = adminmaster.PhoneNo;
                            cv.AdminEmail = adminmaster.Email;
                            //cv.AdminTemplate = "CertificateGPS";
                            cv.AdminTemplate = SessionWrapper.AdminID == 8 ? "CertifcateGPSArabic" : "CertificateGPS";
                        }
                        else
                        {
                            var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                            AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                            Template temp = db.Template.Find(adminmaster.TemplateID);
                            cv.AdminID = adminmaster.AdminID;
                            cv.AdminName = adminmaster.DisplayName.ToUpper();
                            cv.AdminAddress = adminmaster.Address;
                            cv.AdminPhone = adminmaster.PhoneNo;
                            cv.AdminLogo = adminmaster.Logo;
                            cv.AdminEmail = adminmaster.Email;
                            cv.AdminTemplate = SessionWrapper.AdminID == 8 ? "CertifcateGPSArabic" : "CertificateGPS";
                            //cv.AdminTemplate = "CertificateGPS";//temp.ViewName;
                            gps_certificate.CUser = SessionWrapper.UserID;
                            //certificate.Muser = SessionWrapper.UserID;
                        }
                        if (!SessionWrapper.IsAdmin)
                        {
                            gps_certificate.DealerId = SessionWrapper.CompanyID;
                        }
                        gps_certificate.Cdate = DateTime.UtcNow;
                        //gps_certificate.Mdate = DateTime.UtcNow;

                        if (gps_certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                        {

                            gps_certificate.ExpDate = gps_certificate.Cdate.AddDays(-1).AddYears(1);
                        }
                        else
                        {

                            gps_certificate.ExpDate = gps_certificate.InstallDate.AddDays(-1).AddYears(1);
                        }

                        gps_certificate.E_stat = true;

                        GPSCertificate cr = db.gpscertificate.Find(gps_certificate.VehicleNo);
                      
                        //Certificate cr = new Certificate();
                        //cr.CertificateNo = cerr.CertificateNo;
                        if (ModelState.IsValid)
                        {
                            cr.TerminalId = gps_certificate.TerminalId;
                          
                            cr.TechnicianName = gps_certificate.TechnicianName;
                            cr.VehicleNo = gps_certificate.VehicleNo;
                           
                            
                            cr.ChassisNo = gps_certificate.ChassisNo;
                            //cr.Country = certificate.Country;
                          //  cr.ApplicableStandard = certificate.ApplicableStandard;
                            cr.EngineNo = gps_certificate.EngineNo;
                            cr.Cdate = Convert.ToDateTime(gps_certificate.Cdate);
                            cr.ExpDate = Convert.ToDateTime(gps_certificate.ExpDate);
                            cr.InstallDate = gps_certificate.InstallDate;
                            cr.Makeofvehicle = gps_certificate.Makeofvehicle;
                            cr.modelofvehicle = gps_certificate.modelofvehicle;
                            cr.OwnerName = gps_certificate.OwnerName;
                            cr.Maxspeed = gps_certificate.Maxspeed;

                            cr.ApplicableStandard = gps_certificate.ApplicableStandard;
                            cr.ApplicableStandard = gps_certificate.Country;
                            cr.E_stat = gps_certificate.E_stat;
                            cr.Duplicate = 0;


                            db.Entry(cr).State = EntityState.Modified;
                            db.SaveChanges();
                            gps_certificate.CertificateNo = cr.CertificateNo;
                            CompanyMaster comp = db.CompanyMst.Find(gps_certificate.DealerId);
                           // Country con = db.countries.Find(certificate.Country);
                         
                            GPSCertificateView objcertificateview = new GPSCertificateView
                            {
                                CertificateNo = gps_certificate.CertificateNo.ToString(),
                                VehicleNo = gps_certificate.VehicleNo,
                                OwnerName = (gps_certificate.OwnerName).ToUpper(),
                                ChassisNo = gps_certificate.ChassisNo,
                                EngineNo = gps_certificate.EngineNo,
                              
                                MakeOfVehicle = gps_certificate.Makeofvehicle,
                                ModelOfVehicle = gps_certificate.modelofvehicle,
                                IMEINO=gps_certificate.IMEINO,
                                SIMNO=gps_certificate.SIMNO,
                                Maxspeed = gps_certificate.Maxspeed,
                               
                                TerminalId = gps_certificate.TerminalId,
                               
                                InstallDate = gps_certificate.InstallDate,
                                ExpDate = gps_certificate.ExpDate,
                                TechnicianName = gps_certificate.TechnicianName,
                              
                                Cdate = gps_certificate.Cdate,
                                Cuser = gps_certificate.CUser,
                               
                                E_stat = gps_certificate.E_stat,
                                Duplicate = gps_certificate.Duplicate,
                                DealerId = gps_certificate.DealerId,
                                //CompID = comp.CompID,
                                Name = comp.DisplayName,
                                Address = comp.Address,
                                PhoneNo = comp.PhoneNo,
                               
                                AdminName = cv.AdminName,
                                AdminAddress = cv.AdminAddress,
                                AdminPhone = cv.AdminPhone,
                                AdminEmail = cv.AdminEmail,
                                ApplicableStandard=gps_certificate.ApplicableStandard,
                               // CountryName=con.CountryName
                               // CalibrationDate = certificate.CalibrationDate==null?certificate.InstallDate:certificate.CalibrationDate
                                //valid=Convert.ToString(certificate.E_Stat)
                                // Logo=x,
                            };
                            FillImageUrl(objcertificateview, cv.AdminLogo);
                            FillLogo(objcertificateview, comp.Logo);
                            return this.ViewPdf("Certificate", cv.AdminTemplate, objcertificateview);
                        }

                        dropdown();
                        CompanyName();
                        return View(gps_certificate);
                    }
                
                else
                {
                    if (gps_certificate.VehicleNo != null)
                    {

                        // if (db.certificate_mst.Where(v => v.VehRegistrationNo == certificate.VehRegistrationNo).Count() > 1)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");

                        // }
                        //else


                        if (Exist_exp.Where(v => v.VehicleNo == gps_certificate.VehicleNo).Count() > 0)
                        {
                            ModelState.AddModelError("VehicleNo", "VehRegistrationNo already Exists");
                        }

                        //if (db.certificate_mst.SingleOrDefault(a => a.VehRegistrationNo == certificate.VehRegistrationNo).E_Stat == true)
                        // {
                        //     ModelState.AddModelError("VehRegistrationNo", "VehRegistrationNo already Exists");
                        // }

                    }
                    else
                    {
                        gps_certificate.VehicleNo = "";
                    }
                    if (gps_certificate.ChassisNo != null)
                    {

                        if (Exist_exp.Where(c => c.ChassisNo == gps_certificate.ChassisNo).Count() > 0)
                        {
                            ModelState.AddModelError("ChassisNo", "ChassisNo already Exists");
                        }

                    }
                    if (gps_certificate.EngineNo != null)
                    {
                        if (Exist_exp.Where(e => e.EngineNo == gps_certificate.EngineNo).Count() > 0)
                        {
                            ModelState.AddModelError("EngineNo", "EngineNo already Exists");
                        }
                    }
                   

                    var Exist_serial = from a in db.gpsserialno where a.E_Stat == false select a;
                    var Exist_serialNo = from a in db.gpsserialno where a.E_Stat == true select a;
                    //var Exist_tampseal = from a in db.TamperSeal where a.E_Stat == false select a;
                    // var Exist_tampsealNo = from a in db.TamperSeal where a.E_Stat == true select a;
                    if (gps_certificate.TerminalId != null)
                    {

                        if (Exist_serial.Where(a => a.SerialNo == gps_certificate.TerminalId).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.TerminalId == gps_certificate.TerminalId).Count() > 0)
                            {
                                ModelState.AddModelError("TerminalId", "SerialNo already Exists");
                            }


                        }
                        else if (Exist_serialNo.Where(a => a.SerialNo == gps_certificate.TerminalId).Count() > 0)
                        {
                            if (Exist_exp.Where(s => s.TerminalId == gps_certificate.TerminalId).Count() > 0)
                            {
                                ModelState.AddModelError("TerminalId", "SerialNo already created ");
                            }

                             var z=  db.gpscertificate.FirstOrDefault(a=>a.TerminalId==gps_certificate.TerminalId);
                             if (z.ChassisNo!=gps_certificate.ChassisNo)
                             {
                                 ModelState.AddModelError("TerminalId", "SerialNo already created with another device");
 
                             }
                            //if (db.certificate_mst.Where(l=>l.CertificateNo==certificate.CertificateNo).Count()>0)
                            //{
                            //    ModelState.AddModelError("SerialNo", "SerialNo already created with another device");
                            //}
                            //if()
                        }
                        else
                        {
                            ModelState.AddModelError("TerminalId", "SerialNo does not exist in the available list.");
                        }


                    }
                    //if (certificate.TamperSealNo != null )
                    //{
                    //    //if (Exist_tampseal.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{

                    //        if (Exist_exp.Where(t => t.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //        {
                    //            ModelState.AddModelError("TamperSealNo", "TamperSealNo already Exists");
                    //        }


                    //    //}
                    //    //else if (Exist_tampsealNo.Where(a => a.TamperSealNo == certificate.TamperSealNo).Count() > 0)
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo already created");
                    //    //}
                    //    //else
                    //    //{
                    //    //    ModelState.AddModelError("TamperSealNo", "TamperSealNo doesnot Exists");
                    //    //}



                    //}


                    string certnum = (Convert.ToInt32(db.gps_commonFlag.SingleOrDefault(a => a.Name == "CertNo").Value) + 1).ToString();
                    if (gps_certificate.EngineNo != null)
                    {
                        gps_certificate.CertificateNo = string.Format("{0}-{1}-{2}", gps_certificate.Country, GetLast(gps_certificate.EngineNo, 6), certnum.PadLeft(4, '0'));
                    }
                    CertificateView ce = new CertificateView();

                    if (SessionWrapper.IsAdmin)
                    {
                        gps_certificate.CUser = SessionWrapper.AdminID;
                        //certificate.Muser = SessionWrapper.AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(SessionWrapper.AdminID);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminEmail = adminmaster.Email;
                        //ce.AdminTemplate ="CertificateGPS";// temp.ViewName;
                        ce.AdminTemplate = SessionWrapper.AdminID == 8 ? "CertifcateGPSArabic" : "CertificateGPS";
                    }
                    else
                    {
                        var AdmnId = db.CompanyMst.SingleOrDefault(a => a.CompID == SessionWrapper.CompanyID).AdminID;
                        AdminMaster adminmaster = db.AdminMst.Find(AdmnId);
                        Template temp = db.Template.Find(adminmaster.TemplateID);
                        ce.AdminID = adminmaster.AdminID;
                        ce.AdminName = adminmaster.DisplayName.ToUpper();
                        ce.AdminAddress = adminmaster.Address;
                        ce.AdminPhone = adminmaster.PhoneNo;
                        ce.AdminLogo = adminmaster.Logo;
                        ce.AdminEmail = adminmaster.Email;
                        //ce.AdminTemplate = "CertificateGPS";//temp.ViewName;/temp.ViewName;
                        ce.AdminTemplate = SessionWrapper.AdminID == 8 ? "CertifcateGPSArabic" : "CertificateGPS";
                        gps_certificate.CUser= SessionWrapper.UserID;
                       // certificate.Muser = SessionWrapper.UserID;
                    }

                          if (!SessionWrapper.IsAdmin)
                    {
                        gps_certificate.DealerId = SessionWrapper.CompanyID;
                    }
                    gps_certificate.Cdate = DateTime.UtcNow;
                    //certificate.Mdate = DateTime.UtcNow;
                    
                    if (gps_certificate.InstallDate.AddYears(1).Date <= DateTime.UtcNow.Date)
                    {

                        gps_certificate.ExpDate = gps_certificate.Cdate.AddDays(-1).AddYears(1);
                    }
                    else
                    {

                        gps_certificate.ExpDate = gps_certificate.InstallDate.AddDays(-1).AddYears(1);
                    }

                    gps_certificate.E_stat = true;
                   
                    if (ModelState.IsValid)
                    {
                        if (cert != null)
                        {
                            cert.E_stat = false;
                            db.Entry(cert).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                        db.gpscertificate.Add(gps_certificate);
                        db.SaveChanges();
                        GPSSerialNo spd = db.gpsserialno.SingleOrDefault(a => a.SerialNo == gps_certificate.TerminalId);
                        spd.E_Stat = true;
                        db.Entry(spd).State = EntityState.Modified;
                        //TamperSeal tamp = db.TamperSeal.SingleOrDefault(a => a.TamperSealNo == certificate.TamperSealNo);
                        //tamp.E_Stat = true;
                        //db.Entry(tamp).State = EntityState.Modified;
                        GPS_CommonFlag comflag = db.gps_commonFlag.Find(1);
                        comflag.Value = certnum;
                        db.Entry(comflag).State = EntityState.Modified;
                        db.SaveChanges();

                        CompanyMaster comp = db.CompanyMst.Find(gps_certificate.DealerId);

                      Country con = db.countries.Find(gps_certificate.Country);
                        GPSCertificateView objcertificateview = new GPSCertificateView
                        {
                            CertificateNo = gps_certificate.CertificateNo.ToString(),
                            VehicleNo= gps_certificate.VehicleNo,
                            OwnerName = (gps_certificate.OwnerName).ToUpper(),
                            ChassisNo = gps_certificate.ChassisNo,
                            EngineNo = gps_certificate.EngineNo,
                            SIMNO=gps_certificate.SIMNO,
                            IMEINO=gps_certificate.IMEINO,
                          //  ManufactureYear = gps_certificate.ManufactureYear,
                            MakeOfVehicle = gps_certificate.Makeofvehicle,
                            ModelOfVehicle = gps_certificate.modelofvehicle,
                          //  TypeSpeedLimiter = gps_certificate.TypeSpeedLimiter,
                            Maxspeed = gps_certificate.Maxspeed,
                         //   speed2=gps_certificate.speed2,
                            TerminalId = gps_certificate.TerminalId,
                            //TamperSealNo = gps_certificate.TamperSealNo,
                            InstallDate = gps_certificate.InstallDate,
                            ExpDate = gps_certificate.ExpDate,
                            TechnicianName = gps_certificate.TechnicianName,
                           ApplicableStandard = gps_certificate.ApplicableStandard,
                            Country = gps_certificate.Country,
                            Cdate = gps_certificate.Cdate,
                           Cuser = gps_certificate.CUser,
                           // Mdate = gps_certificate.Mdate,
                            //Muser = gps_certificate.Muser,
                            E_stat = gps_certificate.E_stat,
                            Duplicate = gps_certificate.Duplicate,
                            DealerId = gps_certificate.DealerId,
                            //CompID = comp.CompID,
                            Name = comp.DisplayName,
                            Address = comp.Address,
                            PhoneNo = comp.PhoneNo,
                        CountryName = con.CountryName,
                            AdminName = ce.AdminName,
                            AdminAddress = ce.AdminAddress,
                            AdminPhone = ce.AdminPhone,
                            AdminEmail = ce.AdminEmail,
                          //  ApplicableStandard=ce.ApplicableStandard
                           // CalibrationDate = certificate.CalibrationDate==null?certificate.InstallDate:certificate.CalibrationDate
                            //valid=Convert.ToString(certificate.E_Stat)
                            // Logo=x,
                        };

                       FillImageUrl(objcertificateview, ce.AdminLogo);
                        FillLogo(objcertificateview, comp.Logo);
                        return this.ViewPdf("Certificate", ce.AdminTemplate, objcertificateview);
                       
                    }


                }

                 dropdown();
                CompanyName();
                return View(gps_certificate);
            }
               
               
           
        }

        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]

        public ActionResult GetDet(string searchString)
        {

            using (DatContext db = new DatContext())
            {

                var obj = from certificate in db.gpscertificate
                          join country in db.countries
                          on certificate.Country equals country.CountryCode
                          join company in db.CompanyMst on
                          certificate.DealerId equals company.CompID
                          where certificate.E_stat == true && certificate.CertificateNo.Trim() == searchString.Trim()
                          select new GPSCertificateView
                          {
                              CertificateNo = certificate.CertificateNo,
                              VehicleNo = certificate.VehicleNo,
                              OwnerName = certificate.OwnerName,
                              ChassisNo = certificate.ChassisNo,
                              EngineNo = certificate.EngineNo,
                              //ManufactureYear = certificate.ManufactureYear,
                              MakeOfVehicle = certificate.Makeofvehicle,
                              ModelOfVehicle = certificate.modelofvehicle,
                              //TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                              Maxspeed = certificate.Maxspeed,
                              SIMNO=certificate.SIMNO,
                              IMEINO=certificate.IMEINO,
                              SOSNO=certificate.SOSNO,
                              TerminalId = certificate.TerminalId,
                              //TamperSealNo = certificate.TamperSealNo,
                              InstallDate = certificate.InstallDate,
                             // CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate,
                              ExpDate = certificate.ExpDate,
                              TechnicianName = certificate.TechnicianName,
                              ApplicableStandard = certificate.ApplicableStandard,
                              Country = certificate.Country,
                              Cdate = certificate.Cdate,
                              Cuser = certificate.CUser,
                             // Mdate = certificate.Mdate,
                             // Muser = certificate.Muser,
                              E_stat = certificate.E_stat,
                              Duplicate = certificate.Duplicate,
                              DealerId = certificate.DealerId,
                              CompID = company.CompID,
                              Name = company.Name,
                              Address = company.Address,
                              PhoneNo = company.PhoneNo,
                              CountryName = country.CountryName,
                              Expired = EntityFunctions.DiffDays(certificate.ExpDate, DateTime.UtcNow) > 0 ? "Expired" : "Valid"

                          };

                GPSCertificateView cobj = obj.SingleOrDefault(u => u.CertificateNo == searchString);

                return View(cobj);

            }


        }
        [NonAction]
        public static string GetLast(string source, int tail_length)
        {
            if (tail_length >= source.Length)
                return source;
            return source.Substring(source.Length - tail_length);
        }

        [Authorize]
        public ActionResult Edit(string id = null)
        {
            using (DatContext db = new DatContext())
            {
                GPSCertificate certificate = db.gpscertificate.Find(id);
                if (certificate == null)
                {
                    return HttpNotFound();
                }
                dropdown();
                CompanyName();
                return PartialView(certificate);

            }
        }

        // POST: /Certificate/Edit/5

        [HttpPost]
        [Authorize, Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(GPSCertificate certificate)
        {

            using (DatContext db = new DatContext())
            {
                if (Certificate_AdminPanel.SessionWrapper.AdminLevel == AdminLevel.Admin || Certificate_AdminPanel.SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    GPSCertificate cer = db.gpscertificate.Find(certificate.CertificateNo);

                    GPSCertificateLog cerlog = new GPSCertificateLog();
       

                    cerlog.CertificateNo = cer.CertificateNo;
                    cerlog.TerminalId = cer.TerminalId;
                    //cerlog.ta = cer.TamperSealNo;
                    cerlog.TechnicianName = cer.TechnicianName;
                    cerlog.VehicleNo = cer.VehicleNo;
                    cerlog.ApplicableStandard = cer.ApplicableStandard;
                  
                    cerlog.CUser = cer.CUser;
                    cerlog.Muser = Convert.ToInt32(SessionWrapper.AdminID);
                    cerlog.Mdate = DateTime.UtcNow;
                    cerlog.ChassisNo = cer.ChassisNo;
                    cerlog.Country = cer.Country;
                   // cerlog.ApplicableStandard = cer.ApplicableStandard;
                    cerlog.EngineNo = cer.EngineNo;
                    cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                    cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                    cerlog.InstallDate = cer.InstallDate;
                    cerlog.Makeofvehicle = cer.Makeofvehicle;
                    cerlog.modelofvehicle = cer.modelofvehicle;
                    cerlog.OwnerName = cer.OwnerName;
                    cerlog.Maxspeed = cer.Maxspeed;
                    cerlog.SOSNO = cer.SOSNO;
                    cerlog.OdometerReading = cer.OdometerReading;
                    cerlog.fuelmileageratio = cer.fuelmileageratio;
                    cerlog.E_stat = cer.E_stat;
                    cerlog.Duplicate = cer.Duplicate;
                    cerlog.SIMNO=cer.SIMNO;
                    cerlog.IMEINO = cer.IMEINO;
                    cerlog.Country = cer.Country;
                    cerlog.OwnerPhone = cer.OwnerPhone;
                    cerlog.DriverName = cer.DriverName;
                    cerlog.DriverPhone = cer.DriverPhone;

                    //cer.Cuser = SessionWrapper.AdminID;
                  


                    var Exist_exp = from a in db.gpscertificate where a.ExpDate > DateTime.UtcNow select a;

                    cer.VehicleNo = certificate.VehicleNo;
                    if (cer.VehicleNo == null)
                    {
                        ModelState.AddModelError("VehicleNo", "Vehicle Registration number required");
                    }
                    else if (cer.VehicleNo != cerlog.VehicleNo)
                    {

                        if (Exist_exp.Where(v => v.VehicleNo == certificate.VehicleNo).Count() > 0)
                        {
                            ModelState.AddModelError("VehicleNo", "VehRegistrationNo already Exists");
                        }
                    }
                    cer.OwnerName = certificate.OwnerName;
                    cer.ChassisNo = certificate.ChassisNo;
                    cer.EngineNo = certificate.EngineNo;
                    cer.Maxspeed = certificate.Maxspeed;
                    cer.modelofvehicle = certificate.modelofvehicle;
                    cer.Makeofvehicle = certificate.Makeofvehicle;
                  
                    cer.TerminalId = certificate.TerminalId;
                    cer.SIMNO = certificate.SIMNO;
                    cer.IMEINO = certificate.IMEINO;
                    cer.SOSNO = certificate.SOSNO;
                    cer.OdometerReading = certificate.OdometerReading;
                    cer.fuelmileageratio = cer.fuelmileageratio;
                    cer.TechnicianName = certificate.TechnicianName;
                    cer.DealerId = certificate.DealerId;
                    cer.Duplicate = 0;
                    cer.Country = certificate.Country;
                    cer.OwnerPhone = certificate.OwnerPhone;
                    cer.DriverPhone = certificate.DriverPhone;
                    cer.DriverName = certificate.DriverName;

                    cer.ApplicableStandard = certificate.ApplicableStandard;
                    //cer.InstallDate = certificate.InstallDate.Date;
                    //if (cer.InstallDate.AddYears(1).Date < DateTime.Now.Date)
                    //{

                    //    cer.ExpDate = cer.Cdate.AddDays(-1).AddYears(1);
                    //}
                    //else
                    //{

                    //    cer.ExpDate = cer.InstallDate.AddDays(-1).AddYears(1);
                    //}
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            db.Entry(cer).State = EntityState.Modified;
                            db.SaveChanges();
                            db.gpscertificatelog.Add(cerlog);
                            db.SaveChanges();
                            //SpeedLimiterSerialNo spd = db.SpeedLimiterSerialNo.SingleOrDefault(a => a.SerialNo == certificate.SerialNo);
                            //spd.E_Stat = true;
                            //db.Entry(spd).State = EntityState.Modified;
                            //db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Trace.TraceInformation("Property: {0} Error: {1}",
                                                            validationError.PropertyName,
                                                            validationError.ErrorMessage);
                                }
                            }
                        }
                    }

                    dropdown();
                    CompanyName();
                    return View(certificate);
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        public ActionResult Delete(string id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                using (DatContext db = new DatContext())
                {

                    GPSCertificate cer = db.gpscertificate.Find(id);
                    return PartialView(cer);
                }
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        [HttpPost, Audit, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    GPSCertificate cer = db.gpscertificate.Find(id);

                    GPSCertificateLog cerlog = new GPSCertificateLog();
                    cerlog.CertificateNo = cer.CertificateNo;
                    cerlog.TerminalId = cer.TerminalId;
                    //cerlog.ta = cer.TamperSealNo;
                    cerlog.TechnicianName = cer.TechnicianName;
                    cerlog.VehicleNo = cer.VehicleNo;
                    cerlog.ApplicableStandard = cer.ApplicableStandard;
                  
                    cerlog.CUser = cer.CUser;
                    cerlog.Muser = Convert.ToInt32(SessionWrapper.AdminID);
                    cerlog.Mdate = DateTime.UtcNow;
                    cerlog.ChassisNo = cer.ChassisNo;
                    cerlog.Country = cer.Country;
                   // cerlog.ApplicableStandard = cer.ApplicableStandard;
                    cerlog.EngineNo = cer.EngineNo;
                    cerlog.Cdate = Convert.ToDateTime(cer.Cdate);
                    cerlog.ExpDate = Convert.ToDateTime(cer.ExpDate);
                    cerlog.InstallDate = cer.InstallDate;
                    cerlog.Makeofvehicle = cer.Makeofvehicle;
                    cerlog.modelofvehicle = cer.modelofvehicle;
                    cerlog.OwnerName = cer.OwnerName;
                    cerlog.Maxspeed = cer.Maxspeed;
                    cerlog.SOSNO = cer.SOSNO;
                    cerlog.OdometerReading = cer.OdometerReading;
                    cerlog.fuelmileageratio = cer.fuelmileageratio;
                    cerlog.E_stat = cer.E_stat;
                    cerlog.Duplicate = cer.Duplicate;
                    cerlog.SIMNO=cer.SIMNO;
                    cerlog.IMEINO = cer.IMEINO;
                    cerlog.Country = cer.Country;
                    cerlog.OwnerPhone = cer.OwnerPhone;
                    cerlog.DriverName = cer.DriverName;
                    cerlog.DriverPhone = cer.DriverPhone;

                    GPSSerialNo ser = db.gpsserialno.SingleOrDefault(a => a.SerialNo == cer.TerminalId);
                    ser.E_Stat = false;
                    db.Entry(ser).State = EntityState.Modified;
                    db.gpscertificatelog.Add(cerlog);
                    db.gpscertificate.Remove(cer);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }


     private void FillImageUrl(GPSCertificateView Certificate, string imageName)
        {
            //if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser || SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
            //{
            //    string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            //    Certificate.ImageUrl = url + imageName;
            //}
            //else
            //{
                string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                Certificate.ImageUrl = url  + imageName;
            //}
            }
        private void FillLogo(GPSCertificateView Certificate, string imageName)
        {
            //if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            //{
                string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
                Certificate.DealerLogo = url + imageName;               
            //}
            //else
            //{
            //    string url = string.Format("{0}://{1}{2}", Request.Url.Scheme, Request.Url.Authority, Url.Content("~"));
            //    Certificate.DealerLogo = url + "Content/" + "Untitled.jpg";
                
            //}
        }
        /// <summary>
        /// Serialno Create and index
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SerialNoAdd()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
              
                AdminName();
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        [  HttpPost, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SerialNoAdd(GPSSerialNo gpsserialno, int sercount = 0, int Total = 0, string except = null)
        //public ActionResult Delivery(SpdLmtrDelivery spdLmtrDelivery,TamperSeal tamperSeal)
        {

            using (DatContext db = new DatContext())
            {
                //if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                //{
                    try
                    {

                        gpsserialno.Date = DateTime.UtcNow;
                        gpsserialno.User = (int)SessionWrapper.AdminID;
                        gpsserialno.E_Stat = false;
                        if (gpsserialno.SerialNo == null)
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo required");
                        }
                        if (Total == 0)
                        {
                            ModelState.AddModelError("", "Total field required");
                        }
                        //spdLmtrDelivery.SerialNo = (string.Format("{0}{1}", spdLmtrDelivery.SerialNo, sercount)).ToUpper();
                        var serial = (string.Format("{0}{1}", gpsserialno.SerialNo, sercount)).ToUpper();
                        if (db.gpsserialno.Where(a => a.SerialNo == serial).Count() > 0)
                        {
                            ModelState.AddModelError("SerialNo", "SerialNo already Exists");
                        }



                        if (ModelState.IsValid)
                        {
                            //int extractserial = int.Parse(ExtractNumber(spdLmtrDelivery.SerialNo));

                            string serialstr = gpsserialno.SerialNo.ToUpper();

                            int serialEnd = sercount + Total - 1;

                            //int tamperEnd = extracttamp + TamperTotal - 1;


                            for (int a = sercount; a <= serialEnd; a++)
                            {
                                gpsserialno.SerialNo = string.Format("{0}{1}", serialstr, a);

                                string ex = string.Format("{0}{1}", serialstr, except);


                                if (gpsserialno.SerialNo != ex)
                                {
                                    db.gpsserialno.Add(gpsserialno);
                                    db.SaveChanges();
                                }
                                TempData["Message"] = "Serial No. added succesfully!!!";
                                AdminName();
                            } return View();
                        }
                        else
                        {
                            AdminName();
                            return View(gpsserialno);
                        }
                    }


                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.ToString());
                    }
                    return View();
                //}
                //else
                //{
                //    return View("UnAuthenticated");
                //}
            }
        }
        [Authorize, Audit, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Generate(string id)
        {
            using (DatContext db = new DatContext())
            {


                //Certificate certificate = db.certificate_mst.Find(id);
                //Country con = db.countries.Find(certificate.Country);
                //CompanyMaster comp = db.CompanyMst.Find(certificate.DealerId);
                //Certificate certificate = result.SingleOrDefault(a => a.CertificateNo ==certificate.CertificateNo);
                //certificate.Duplicate = (Convert.ToInt32(db.certificate_mst.SingleOrDefault(a => a.CertificateNo == certificate.CertificateNo).Duplicate) + 1);
                var result = from certificate in db.gpscertificate
                             join con in db.countries
                             on certificate.Country equals con.CountryCode
                             join comp in db.CompanyMst on
                             certificate.DealerId equals comp.CompID
                             join admn in db.AdminMst on
                             comp.AdminID equals admn.AdminID
                             join temp in db.Template on
                             admn.TemplateID equals temp.ID
                             where certificate.CertificateNo == id
                             select
              new GPSCertificateView
              {
                  CertificateNo = certificate.CertificateNo,
                  VehicleNo = certificate.VehicleNo,
                  OwnerName = certificate.OwnerName.ToUpper(),
                  ChassisNo = certificate.ChassisNo,
                  EngineNo = certificate.EngineNo,
                  SIMNO=certificate.SIMNO,
                  IMEINO=certificate.IMEINO,
                  OdometerReading=certificate.OdometerReading,
                  fuelmileageratio=certificate.fuelmileageratio,
                  OwnerPhone=certificate.OwnerPhone,
                  
                 // ManufactureYear = certificate.ManufactureYear,
                  MakeOfVehicle = certificate.Makeofvehicle,
                  ModelOfVehicle = certificate.modelofvehicle,
                 // TypeSpeedLimiter = certificate.TypeSpeedLimiter,
                  Maxspeed = certificate.Maxspeed,
                //  speed2 = certificate.speed2,
                  TerminalId = certificate.TerminalId,
                 // TamperSealNo = certificate.TamperSealNo,
                  InstallDate = certificate.InstallDate,
                  ExpDate = certificate.ExpDate,
                  TechnicianName = certificate.TechnicianName,
                  ApplicableStandard = certificate.ApplicableStandard,
                  Country = certificate.Country,
                  Cdate = certificate.Cdate,
                  Cuser = certificate.CUser,
               //   Mdate = certificate.Mdate,
                 // Muser = certificate.Muser,
                  E_stat = certificate.E_stat,
                  Duplicate = certificate.Duplicate,
                  DealerId = certificate.DealerId,
                  CompID = comp.CompID,
                  Name = comp.Name,
                  Address = comp.Address,
                  PhoneNo = comp.PhoneNo,
                  CountryName = con.CountryName,
                  DealerLogo = comp.Logo,
                  AdminName = admn.DisplayName.ToUpper(),
                  AdminAddress = admn.Address,
                  AdminPhone = admn.PhoneNo,
                  AdminEmail = admn.Email,
                  AdminLogo = admn.Logo,
                  AdminTemplate = SessionWrapper.AdminID==8? "CertifcateGPSArabic":"CertificateGPS"
                   //AdminTemplate = "CertifcateGPSArabic"
                 // CalibrationDate = certificate.CalibrationDate == null ? certificate.InstallDate : certificate.CalibrationDate
              };
                GPSCertificate cert = db.gpscertificate.Find(id);
                cert.Duplicate = (Convert.ToInt32(cert.Duplicate + 1));
                db.Entry(cert).State = EntityState.Modified;
                db.SaveChanges();
                GPSCertificateView objcertificateview = result.SingleOrDefault(a => a.CertificateNo == id);
                FillImageUrl(objcertificateview, objcertificateview.AdminLogo);
                FillLogo(objcertificateview, objcertificateview.DealerLogo);
                return this.ViewPdf("Certificate", objcertificateview.AdminTemplate, objcertificateview);
            }
        }

        public ViewResult GPSSerialNo(GPSCertificateSerialNoView spd)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    if (spd.fromdate != null && spd.todate != null && spd.Dealerdd != null)
                    {

                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == true select d).Count();
                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());
                    }
                    else if (spd.fromdate != null && spd.todate != null)
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == true select d).Count();
                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());

                    }
                    else if (spd.fromdate == null && spd.todate == null && spd.Dealerdd != null)
                    {

                        ViewBag.Availablecnt = (from d in db.gpsserialno where spd.Dealerdd == d.DealerId && d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where spd.Dealerdd == d.DealerId && d.E_Stat == true select d).Count();
                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where spd.Dealerdd == b.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        return View(result.ToList());
                    }
                    else
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.E_Stat == false select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.E_Stat == true select d).Count();

                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID

                                     //join c in db.certificate_mst on a.SerialNo equals c.SerialNo into cer
                                     //from sp in cer.DefaultIfEmpty()
                                     // where sp.E_Stat==true
                                     //where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         //Used=(db.certificate_mst.FirstOrDefault(z=>z.SerialNo==a.SerialNo).DealerId)!=null?b.Name:"NILL",
                                         //Used=(sp!=null?(db.CompanyMst.FirstOrDefault(l=>l.CompID==sp.DealerId)).Name:string.Empty),
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        AdminName();
                        CommonData.JsonGpsDevicesPush(result.OrderByDescending(z => z.Date).ToList());

                        return View();
                       // return View(result.ToList());
                    }
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    if (spd.fromdate != null && spd.todate != null && spd.Dealerdd != null)
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && spd.Dealerdd == d.DealerId && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();
                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID && b.AdminID == SessionWrapper.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }
                    else if (spd.fromdate == null && spd.todate == null && spd.Dealerdd != null)
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where spd.Dealerdd == d.DealerId && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where spd.Dealerdd == d.DealerId && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();

                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && spd.Dealerdd == b.AdminID && b.AdminID == SessionWrapper.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }

                    else if (spd.fromdate != null && spd.todate != null)
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.Date >= spd.fromdate && d.Date <= spd.todate && d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();

                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     where a.Date >= spd.fromdate && a.Date <= spd.todate && b.AdminID == SessionWrapper.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        return View(result.ToList());
                    }
                    else
                    {
                        ViewBag.Availablecnt = (from d in db.gpsserialno where d.E_Stat == false && d.DealerId == SessionWrapper.AdminID select d).Count();
                        ViewBag.NotAvailablecnt = (from d in db.gpsserialno where d.E_Stat == true && d.DealerId == SessionWrapper.AdminID select d).Count();
                        var result = from a in db.gpsserialno
                                     join b in db.AdminMst
                                     on a.DealerId equals b.AdminID
                                     //where a.Date >= spd.fromdate && a.Date <= spd.todate
                                     where b.AdminID == SessionWrapper.AdminID
                                     select new GPSCertificateSerialNoView
                                     {
                                         Recno = a.Recno,
                                         SerialNo = a.SerialNo,
                                         Date = a.Date,
                                         DealerId = a.DealerId,
                                         DealerName = b.Name,
                                         Used = db.CompanyMst.FirstOrDefault(z => z.CompID == (db.gpscertificate.FirstOrDefault(l => l.TerminalId == a.SerialNo).DealerId)) != null ? db.CompanyMst.FirstOrDefault(z => z.CompID == (db.certificate_mst.FirstOrDefault(l => l.SerialNo == a.SerialNo).DealerId)).Name : string.Empty,
                                         Status = a.E_Stat ? "Not Available" : "Available"
                                         //User = a.User
                                     };
                        CompanyName();
                        //Offline storage
                        CommonData.JsonGpsDevicesPush(result.OrderByDescending(z => z.Date).ToList());

                        return View();
                       // return View(result.ToList());
                    }

                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        //GET:/GPSCertificate/SerialNoEdit/
        //[Audit]
        public ActionResult SerialNoEdit(int id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    GPSSerialNo spd = db.gpsserialno.Find(id);
                    return PartialView(spd);
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }

        //POST:/GPSCertificate/SerialNoEdit/

        [ValidateAntiForgeryToken, Audit, HttpPost, Authorize, OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SerialNoEdit(GPSSerialNo _serialno)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    SpeedLimiterSerialNo _spd = db.SpeedLimiterSerialNo.Find(_serialno.Recno);
                    _spd.SerialNo = _serialno.SerialNo;
                    if (_spd.E_Stat == true)
                    {
                        ModelState.AddModelError("", "Certificate already created with this Serialno");
                    }
                    if (ModelState.IsValid)
                    {
                        db.Entry(_spd).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("gpsserialno");
                    }
                    return View();
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }



        [NonAction]
        private void dropdown()
        {
            using (DatContext db = new DatContext())
            {
                List<Country> country = db.countries.ToList();
                ViewBag.country = new SelectList(country, "CountryCode", "CountryName");
            }
            var app = new[]
            {
                new SelectListItem { Text="AIS-018",Value ="AIS-018" },
                new SelectListItem { Text = "BS-217", Value = "BS-217" },
                new SelectListItem { Text = "EEC-92/24", Value = "EEC-92/24" },
                new SelectListItem { Text = "GSO-1626/2002", Value = "GSO-1626/2002" },
                new SelectListItem {Text="KS 2295-2011", Value="KS 2295-2011"},
                new SelectListItem {Text="NIS 755-1:2014", Value="NIS 755-1:2014"}

            };
            ViewBag.AppStd = new SelectList(app, "Text", "Value", "-select-");



        }
        [NonAction]
        private void AdminName()
        {
            using (DatContext db = new DatContext())
            {

                List<AdminMaster> admn = (from a in db.AdminMst where a.E_Stat == true && a.GPS_stat==true select a).ToList();
                ViewBag.Admin = new SelectList(admn, "adminid", "Name");

            }
        }

        /// <summary>
        /// Getting the list of company names in the dropdown
        /// </summary>
        [NonAction]
        private void CompanyName()
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    ViewBag.Dealerdd = new SelectList("", "CompID", "Name");
                    List<AdminMaster> admn = (from a in db.AdminMst where a.E_Stat == true && a.GPS_stat==true select a).ToList();

                    var admin = from a in db.AdminMst where a.E_Stat == true && a.GPS_stat==true  select new { a.AdminID, a.Name };
                    ViewBag.AdminID = new SelectList(admn, "AdminID", "Name", "--Select company--");


                    List<CompanyMaster> Company = (from a in db.CompanyMst where a.E_Stat == true && a.GPS_stat==true  select a).ToList();
                    ViewBag.DealerId = new SelectList(Company, "CompID", "Name");

                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    List<CompanyMaster> Company = db.CompanyMst.ToList();
                    var comp = from compnay in Company where compnay.AdminID == SessionWrapper.AdminID && compnay.E_Stat == true && compnay.GPS_stat==true select new { compnay.CompID, compnay.Name };
                    ViewBag.Dealerdd = new SelectList(comp, "CompID", "Name");
                    ViewBag.DealerId = new SelectList(comp, "CompID", "Name");
                    //from comp in db.CompanyMst where comp.AdminID == SessionWrapper.AdminID select new { comp.CompID, comp.Name };
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    List<CompanyMaster> Company = db.CompanyMst.ToList();
                    var cmp = db.InspectionMst.Find(SessionWrapper.CompanyID).AdminID;
                    var comp = from compnay in Company where compnay.AdminID == cmp && compnay.E_Stat == true && compnay.GPS_stat==true select new { compnay.CompID, compnay.Name };
                    ViewBag.Dealerdd = new SelectList(comp, "CompID", "Name");


                }


            }
        }
    }
}
