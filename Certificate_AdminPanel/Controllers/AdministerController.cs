﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Certificate_AdminPanel.Models;
using System.Web.Helpers;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Net;

using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

using System.Configuration;
using System.Data.SqlClient;
using Certificate_AdminPanel.ViewModels;



namespace Certificate_AdminPanel.Controllers
{


    public class AdministerController : Controller
    {

        private DatContext db = new DatContext();
        List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInUsers"];
        string logo_path;
        //
        // GET: /Admin/
        [Authorize, Audit]
       
        public ActionResult Index()
        {
            //using(DatContext db=new DatContext())
            //{
            //Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9_@.-]*$");
            //bool sts = objAlphaPattern.IsMatch(username);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            //Response.Cache.SetNoStore();
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                //System.Threading.Thread.Sleep(5000);
                //var adminmst = db.AdminLogin.Include(a => a.AdminMaster);
                var adminmst = (from a in db.AdminLogin
                                join b in db.AdminMst on a.AdminID equals b.AdminID
                                join c in db.GMT_ZONE on b.TimeZoneID equals c.tab_id
                                where a.E_Stat == true
                                select new admin
                                    {
                                        LoginID = a.LoginID,
                                        AdminID = a.AdminID,
                                        Name = b.Name,
                                        LoginName = a.LoginName,
                                        Password = a.Password,
                                        pwd = a.pwd,
                                        DisplayName = b.DisplayName,
                                        Address = b.Address,
                                        PhoneNo = b.PhoneNo.Trim(),
                                        MobileNo = b.MobileNo.Trim(),
                                        Email = b.Email,
                                        TimeZoneID = b.TimeZoneID,
                                        Logo = b.Logo,
                                        MDate = b.MDate,
                                        ExpDate = b.ExpDate,
                                        CDate = a.CDate,
                                        Type = a.Type,
                                        E_Stat = a.E_Stat,
                                        RoleID = a.RoleID,
                                        Gmt_minute = c.Gmt_minute,
                                        time_zone = c.time_zone,
                                        TemplateName = db.Template.FirstOrDefault(xx => xx.ID == b.TemplateID) != null ? db.Template.FirstOrDefault(xx=>xx.ID==b.TemplateID).TemplateName : "N/A",
                                        isLogin = loggedInUsers.Any(xx=>xx.Contains(a.LoginName))?true:false

                                    }).ToList().OrderByDescending(xx => xx.CDate).ToList(); ;

                //adminmst = from dat in adminmst orderby dat.AdminID select dat;
                return View(adminmst);
            }

            //else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            //{
            //    var admn = from a in db.AdminLogin
            //               join b in db.AdminMst on a.AdminID equals b.AdminID
            //               join c in db.GMT_ZONE on b.TimeZoneID equals c.tab_id where a.E_Stat==true
            //               select new admin
            //               {
            //                   Name = b.Name,
            //                   LoginName = a.LoginName,
            //                   DisplayName = b.DisplayName,
            //                   Address = b.Address,
            //                   PhoneNo = b.PhoneNo,
            //                   MobileNo = b.MobileNo,
            //                   Email = b.Email,
            //                   TimeZoneID = b.TimeZoneID,
            //                   Logo=b.Logo,
            //                   MDate = b.MDate,
            //                   ExpDate = b.ExpDate,
            //                   CDate = a.CDate,
            //                   Type = a.Type,
            //                   time_zone = c.time_zone,


            //               };
            //    return View(admn);

            //var adminmst = db.AdminLogin.Include(a => a.AdminMaster);
            //adminmst = from dat in adminmst where dat.AdminID == SessionWrapper.AdminID orderby dat.AdminID select dat;
            //return View(adminmst);
            //}

            return View("LogOn");

            //}
        }


        //GET: /Admin/Details/5
        [Authorize, Audit]
        public ActionResult Details(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                AdminLogin adminlogin = db.AdminLogin.Find(id);
                AdminMaster admn = db.AdminMst.Find(adminlogin.AdminMaster.AdminID);
                GmtZone GMT = db.GMT_ZONE.Find(adminlogin.AdminMaster.TimeZoneID);
                return PartialView(new admin
                {
                    //LoginID = adminlogin.LoginID,
                    //AdminID = adminlogin.AdminID,
                    Name = admn.Name,
                    LoginName = adminlogin.LoginName,
                    Password = adminlogin.Password,
                    pwd = adminlogin.pwd,
                    DisplayName = admn.DisplayName,
                    Address = admn.Address,
                    PhoneNo = admn.PhoneNo.Trim(),
                    MobileNo = admn.MobileNo.Trim(),
                    Email = admn.Email,
                    TimeZoneID = GMT.tab_id,
                    time_zone = GMT.time_zone,
                    Logo = admn.Logo,
                    MDate = admn.MDate,
                    ExpDate = admn.ExpDate,
                    CDate = admn.CDate,
                    Type = admn.Type,
                    E_Stat = admn.E_Stat,


                });
            } return View("UnAuthenticated");
        }

        //
        // GET: /Admin/Create
        [Authorize]
        public ActionResult Create()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                getdropdowns();
                return PartialView();
            }
            return View("UnAuthenticated");
        }

        //
        // POST: /Admin/Create
        [Authorize]
        [HttpPost]
        public ActionResult Create(AdminLogin adminlogin, HttpPostedFileBase file)
        {
            //using (DatContext db = new DatContext())
            //{
            var t = this.ModelState.Keys.SelectMany(key => (this.ModelState[key].Errors));
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                if (db.AdminLogin.Count(a => a.LoginName == adminlogin.LoginName) > 0)
                {
                    ModelState.AddModelError("LoginName", "login name already exists !!");

                }
                //else if (db.AdminMst.Count(a => a.Name == adminlogin.AdminMaster.Name) > 0)
                //{
                //    ModelState.AddModelError("Name", "admin name already exists !!");
                //}
                //else if (adminlogin.AdminMaster.ExpDate < DateTime.Now)
                //{
                //    ModelState.AddModelError("AdminMaster.ExpDate", "date should be graiter than current date !!");
                //}
                else if (file == null)
                { ModelState.AddModelError("AdminMaster_logo", "please select logo "); }
                DateTime cdate = DateTime.UtcNow;
                adminlogin.CDate = cdate;
                adminlogin.AdminMaster.CDate = cdate;
                adminlogin.AdminMaster.MDate = cdate;
                adminlogin.AdminMaster.UserID = 1;
                // adminlogin.AdminMaster.UserID = db.AdminLogin.SingleOrDefault(a => a.LoginName == HttpContext.User.Identity.Name).AdminID;
                adminlogin.AdminMaster.UpperAdminID = adminlogin.AdminMaster.UserID;
                adminlogin.AdminMaster.Type = 1;
                adminlogin.Type = 1;
                adminlogin.RoleID = 1;
                adminlogin.AdminMaster.E_Stat = true;
                adminlogin.E_Stat = true;
                if (adminlogin.Password != null)
                {
                    adminlogin.pwd = adminlogin.Password;
                    adminlogin.Password = GetMD5Hash(adminlogin.Password);

                }
                adminlogin.AdminID = adminlogin.AdminMaster.AdminID;
                adminlogin.AdminMaster.TemplateID = adminlogin.AdminMaster.TemplateID;
                if (ModelState.IsValid)
                {
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        if (file.ContentType.Equals("image/jpeg"))
                        {
                            var guid = Guid.NewGuid();
                            try
                            {
                                file.SaveAs(Server.MapPath("/Content/images/logo/" + string.Format(@"{0}", guid) + file.FileName));
                            }
                            catch (DirectoryNotFoundException)
                            {
                                //System.IO.Directory.CreateDirectory(Server.MapPath("~") + "Content/images/logo/Admin/");
                                //file.SaveAs("/Content/images/logo/" + string.Format(@"{0}", Guid.NewGuid()) + file.FileName);
                            }
                            adminlogin.AdminMaster.Logo = "/Content/images/logo/" + string.Format(@"{0}", guid) + file.FileName;
                            db.AdminMst.Add(adminlogin.AdminMaster);
                            db.SaveChanges();


                            db.AdminLogin.Add(adminlogin);
                            db.SaveChanges();
                            return RedirectToAction("Index");
                        }
                        else
                        {

                            ModelState.AddModelError("logo", "Please upload image/jpeg files");
                        }
                    }
                    else
                    {
                        adminlogin.AdminMaster.Logo = "default";
                        db.AdminMst.Add(adminlogin.AdminMaster);
                        db.SaveChanges();


                        db.AdminLogin.Add(adminlogin);
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                getdropdowns();
                return View(adminlogin);
            }
            return View("UnAuthenticated");
            //}
        }

        /// <summary>
        /// To check login name exists or not
        /// </summary>
        /// <param name="LoginName"></param>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public ActionResult Loginvalid(string LoginName, string LoginID)
        {
            if (LoginID == "undefined" || LoginID == null)
            {
                JsonResult a = Json(!(db.AdminLogin.Count(e => e.LoginName == LoginName) > 0), JsonRequestBehavior.AllowGet);
                return a;
            }
            else
            {
                JsonResult a = Json(!(false), JsonRequestBehavior.AllowGet);
                return a;
            }
        }
        public ActionResult Namevalid(string Name, string AdminID)
        {
            if (AdminID == "undefined" || AdminID == null)
            {
                JsonResult a = Json(!(db.AdminMst.Count(e => e.Name == Name) > 0), JsonRequestBehavior.AllowGet);
                return a;
            }
            else
            {
                JsonResult a = Json(!(false), JsonRequestBehavior.AllowGet);
                return a;
            }
        }
        //
        // GET: /Admin/Edit/5 
        [Authorize]
        public ActionResult Edit(long? id)
        {
            //using(DatContext db=new DatContext())
            // {
            //HttpContext.Cache.Remove("logocache");
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                if (id != null)
                {

                    AdminLogin adminmaster = db.AdminLogin.Find(id);
                    //ViewBag.timezone = new SelectList(db.GMT_ZONE, "tab_id", "time_zone", adminmaster.AdminMaster.TimeZoneID);
                    ViewBag.timezone = new SelectList(db.GMT_ZONE, "tab_id", "time_zone");
                    HttpContext.Cache.Insert("logocache_edit", adminmaster.AdminMaster.Logo);

                    getdropdowns();
                    return PartialView(adminmaster);
                }
                return RedirectToAction("Index");
            }
            return View("UnAuthenticated");
            //}
        }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        [Authorize, Audit]
        public ActionResult Edit(AdminLogin adminlogin, HttpPostedFileBase file)
        {
            // using (DatContext db = new DatContext())
            //{
            var x = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);


            if (HttpContext.Cache["logocache_edit"].ToString() != null)
            {
                logo_path = HttpContext.Cache["logocache_edit"].ToString();
            }
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                if (db.AdminLogin.Count(a => a.LoginName == adminlogin.LoginName) > 0 && db.AdminLogin.Find(adminlogin.LoginID).LoginName != adminlogin.LoginName)
                {
                    ModelState.AddModelError("LoginName", "login name already exists !!");

                }
                //else if (adminlogin.AdminMaster.ExpDate < DateTime.Now)
                //{
                //    ModelState.AddModelError("AdminMaster_ExpDate", "date should be graiter than current date !!");
                //}
                //else if (file == null)
                //{ ModelState.AddModelError("AdminMaster_logo", "please select logo "); }
                AdminLogin adminlog = db.AdminLogin.Find(adminlogin.LoginID);

                adminlog.LoginName = adminlogin.LoginName;
                //adminlog.Password = adminlogin.Password;
                adminlog.E_Stat = adminlogin.E_Stat;

                adminlog.AdminMaster.Address = adminlogin.AdminMaster.Address;
                adminlog.AdminMaster.Name = adminlogin.AdminMaster.Name;
                adminlog.AdminMaster.PhoneNo = adminlogin.AdminMaster.PhoneNo;
                adminlog.AdminMaster.MobileNo = adminlogin.AdminMaster.MobileNo;
                adminlog.AdminMaster.Email = adminlogin.AdminMaster.Email;
                adminlog.AdminMaster.ExpDate = adminlogin.AdminMaster.ExpDate;
                adminlog.AdminMaster.E_Stat = adminlogin.E_Stat;


                adminlog.AdminMaster.DisplayName = adminlogin.AdminMaster.DisplayName;
                adminlog.AdminMaster.TimeZoneID = adminlogin.AdminMaster.TimeZoneID;
                adminlog.AdminMaster.TemplateID = adminlogin.AdminMaster.TemplateID;

                adminlog.AdminMaster.MDate = DateTime.UtcNow;

                if (ModelState.IsValid)
                {
                    if ((file != null) && (file.ContentLength > 0))
                    {
                        if (file.ContentType.Equals("image/jpeg"))
                        {
                            //string path1 = Server.MapPath(logo_path);
                            //FileInfo objfile = new FileInfo(path1);//deleting existing file in root folder
                            try
                            {
                                var guid = Guid.NewGuid();
                                file.SaveAs(Server.MapPath("/Content/images/logo/" + string.Format(@"{0}", guid) + file.FileName));
                                adminlog.AdminMaster.Logo = "/Content/images/logo/" + string.Format(@"{0}", guid) + file.FileName;
                                //adminlog.AdminMaster.Logo = path;


                                db.Entry(adminlog).State = EntityState.Modified;
                                db.Entry(adminlog.AdminMaster).State = EntityState.Modified;
                                db.SaveChanges();
                                //if (objfile.Exists)
                                //{
                                //    objfile.Delete();
                                //}

                                return RedirectToAction("Index");
                            }
                            catch
                            {
                                try
                                {
                                }
                                catch
                                {
                                }
                            }
                            finally
                            {
                                try
                                {
                                    // objfile = null;
                                }
                                catch
                                {
                                }


                            }
                        }
                        else
                        {
                            ModelState.AddModelError("logo", "Please upload file");

                        }
                    }
                    else
                    {
                        adminlog.AdminMaster.Logo = logo_path;
                        db.Entry(adminlog).State = EntityState.Modified;
                        db.Entry(adminlog.AdminMaster).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                getdropdowns();
                return View(adminlogin);
            }
            return View("UnAuthenticated");
            //}
        }

        //
        // GET: /Admin/Delete/5 
        [Authorize]
        public ActionResult Delete(long? id)
        {
            // using (DatContext db = new DatContext())
            //{
            if (SessionWrapper.AdminLevel == AdminLevel.Admin && SessionWrapper.AdminID != id)
            {
                AdminLogin adminmaster = db.AdminLogin.Find(id);
                return PartialView(adminmaster);
            } return View("UnAuthenticated");
            //}
        }
        //
        // POST: /Admin/Delete/5
        [Authorize, Audit]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            // using (DatContext db = new DatContext())
            //{
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                AdminLogin adminlogin = db.AdminLogin.Find(id);
                db.Database.ExecuteSqlCommand("exec sp_delete_Admin @AdminID", new SqlParameter("@AdminID", id));
                //AdminMaster adminmaster = db.AdminMst.Find(adminlogin.AdminID);
                //db.AdminMst.Remove(adminmaster);
                //db.AdminLogin.Remove(adminlogin);
                //adminlogin.E_Stat = false;
                //adminmaster.E_Stat = false;
                //db.Entry(adminlogin).State = EntityState.Modified;
                //db.Entry(adminmaster).State = EntityState.Modified;
                //db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View("UnAuthenticated");
            //}
        }
        [Audit]
        public ActionResult LogOn()
        {
            ViewBag.IsAdmin = true;
            if (!Request.IsAuthenticated)
            {


                return View();
            }
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost, Audit]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            ViewBag.IsAdmin = true;
            if (ModelState.IsValid)
            {
                if (Membership.Providers["AdminAuthendication"].ValidateUser(model.UserName, model.Password))
                {
                    setParameter(model.UserName);
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (HttpRuntime.Cache["LoggedInAdmins"] != null) //if the list exists, add this user to it
                    {
                        //get the list of logged in users from the cache
                        List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInAdmins"];
                        //add this user to the list
                        loggedInUsers.Add(model.UserName);
                        //add the list back into the cache
                        HttpRuntime.Cache["LoggedInAdmins"] = loggedInUsers;
                    }
                    else //the list does not exist so create it
                    {
                        //create a new list
                        List<string> loggedInUsers = new List<string>();
                        //add this user to the list
                        loggedInUsers.Add(model.UserName);
                        //add the list into the cache
                        HttpRuntime.Cache["LoggedInAdmins"] = loggedInUsers;
                    }
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                   && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                        {
                          
                            return RedirectToAction("Index", "Dashboard");
                        }
                        else
                        {
                            return RedirectToAction("Index", "Dashboard");
                        }
                    }
                }
                else
                {
                    ViewBag.IsAdmin = true;
                    ModelState.AddModelError("", "UserName or Password is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }



        //
        // GET: /Account/LogOff
        [Audit]
        public ActionResult LogOff()
        {
            string username = User.Identity.Name; //get the users username who is logged in
            if (HttpRuntime.Cache["LoggedInAdmins"] != null)//check if the list has been created
            {
                //the list is not null so we retrieve it from the cache
                List<string> loggedInUsers = (List<string>)HttpRuntime.Cache["LoggedInAdmins"];
                if (loggedInUsers.Contains(username))//if the user is in the list
                {
                    //then remove them
                    loggedInUsers.Remove(username);
                }
                // else do nothing
            }
            FormsAuthentication.SignOut();
            //var admn = from a in db.AdminMst where a.TemplateID == 2 select a.AdminID;
            //if (admn.Contains(SessionWrapper.AdminID))
            //{
            //    Session.Abandon();
            //    return RedirectToAction("LogOnESI");
            //}
            //else
            {
                Session.Abandon();
                return RedirectToAction("LogOn");
            }
        }
        //
        // GET: /Administer/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                return View();
            }
            else
            {
                return View("UnAuthenticated");
            }
        }

        //
        // POST: /Administer/ChangePassword

        [Authorize, Audit]
        [HttpPost]
        public ActionResult ChangePassword(string LoginName, string password)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin)
            {
                using (DatContext db = new DatContext())
                {
                    if (db.AdminLogin.Count(l => l.LoginName == LoginName) > 0)
                    {
                        AdminLogin _adminlogin = db.AdminLogin.SingleOrDefault(z => z.LoginName == LoginName);

                        _adminlogin.LoginName = LoginName;
                        _adminlogin.pwd = password;
                        _adminlogin.Password = GetMD5Hash(password);
                        if (ModelState.IsValid)
                        {
                            db.Entry(_adminlogin).State = EntityState.Modified;
                            db.SaveChanges();
                            TempData["Message1"] = "Change Password";
                            TempData["Message"] = "your Password has been changed successfully!!";
                            return RedirectToAction("ChangePasswordSuccess", "Account");
                        }
                        else
                        {
                            ModelState.AddModelError("", "The new password is invalid.");
                        }

                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid LoginName");
                    }
                    return View();
                }
            }
            else
            {
                return View("UnAuthenticated");
            }
        }
        private void setParameter(string UserName)
        {
            // using(DatContext db=new DatContext())
            //{
            try
            {
                SessionWrapper.IsAdmin = true;
                var admin = db.AdminLogin.SingleOrDefault(l => l.LoginName == UserName);
                if (admin.AdminMaster.Type == 0)
                    SessionWrapper.AdminLevel = AdminLevel.Admin;
                else
                { SessionWrapper.AdminLevel = AdminLevel.SubAdmin; }
                SessionWrapper.AdminID = admin.AdminID;
                //setting deffault value
                SessionWrapper.CompanyID = 0;
                SessionWrapper.UserID = 0;
                SessionWrapper.TimeZone = admin.AdminMaster.TimeZoneID;
                SessionWrapper.DisplayName = admin.AdminMaster.DisplayName;
                SessionWrapper.DisplayLogo = admin.AdminMaster.Logo;
            }
            catch (Exception)
            {
                throw;
            }
            //}
        }

        protected override void Dispose(bool disposing)
        {
            // using(DatContext db=new DatContext())
            //{
            db.Dispose();
            base.Dispose(disposing);
            //}
        }

        private void getdropdowns()
        {
            // using(DatContext db=new DatContext())
            //{
            ViewBag.timezone = new SelectList(db.GMT_ZONE, "tab_id", "time_zone", "--Select Zone--");
            ViewBag.AdminID = new SelectList(db.AdminMst, "AdminID", "Name", "--Select Admin--");
            ViewBag.templatelist = new SelectList(db.Template, "ID", "TemplateName");
            //}
            //using (DatContext db = new DatContext())
            //{
            //    List<GmtZone> Gmtzone = db.GMT_ZONE.ToList();
            //    ViewBag.Gmtzone = new SelectList(Gmtzone, "tab_id", "time_zone", "--Select Zone--");
            //    List<AdminMaster> AdminID = db.AdminMst.ToList();
            //    ViewBag.AdminID = new SelectList(AdminID, "AdminID", "Name", "--Select Admin--");
            //}
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }


    }
}