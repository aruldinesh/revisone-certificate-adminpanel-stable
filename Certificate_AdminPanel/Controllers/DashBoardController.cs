﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;
using System.Configuration;

namespace Certificate_AdminPanel.Controllers
{
    [Authorize, Audit]
    public class DashBoardController : Controller
    {
        //
        // GET: /DashBoard/
        private int RecentDays = ConfigurationManager.AppSettings["RecentDays"] != null ? Convert.ToInt16(ConfigurationManager.AppSettings["RecentDays"].ToString()) : 0;
        private int RenewalDays = ConfigurationManager.AppSettings["RenewalDays"] != null ? Convert.ToInt16(ConfigurationManager.AppSettings["RenewalDays"].ToString()) : 0;

        public ActionResult Index()
        {
            return View();
        }
        [OutputCache(CacheProfile = "10MinsCache")]
        public JsonResult SpeedLimiterCount(int? Dealerdd)
        {
            int totcount, daycount, weekCount, monthCount = 0;
            DateTime givenDate = DateTime.Today;
            DateTime startOfWeek = givenDate.AddDays(-1 * (int)givenDate.DayOfWeek);
            DateTime endOfWeek = startOfWeek.AddDays(7);
            using (DatContext db = new DatContext())
            {

                if (Dealerdd != null && Dealerdd != 0)
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.DealerId == Dealerdd && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID && a.DealerId == Dealerdd && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                                 && a.DealerId == Dealerdd && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }
                    else if (SessionWrapper.AdminID == null)
                    {
                        return Json("SessionClear", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("LogOn", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where a.E_Stat == true
                                 select a;

                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }

                    else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == SessionWrapper.AdminID && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID) &&
                                 a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                    {

                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID
                                 where b.CompID == SessionWrapper.CompanyID && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();

                    }
                    else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                    {
                        var cn = from a in db.certificate_mst
                                 join c in db.countries on a.Country equals c.CountryCode
                                 join b in db.CompanyMst on a.DealerId equals b.CompID

                                 where a.Cuser == SessionWrapper.UserID && a.E_Stat == true
                                 select a;
                        daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        weekCount = cn.Where(xx => xx.Cdate >= startOfWeek && xx.Cdate <= endOfWeek).Count();
                        monthCount = cn.Where(xx => EntityFunctions.DiffMonths(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                        totcount = cn.Count();
                    }
                    else if (SessionWrapper.AdminID == null)
                    {
                        return Json("SessionClear", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json("UnAuthenticated", JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { dayCount = daycount,weekCount=weekCount,monthCount=monthCount, totCount = totcount }, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(CacheProfile = "10MinsCache")]
        public JsonResult GPSCount()
        {
            int totcount, daycount = 0;
            using (DatContext db = new DatContext())
            {


                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             select a;
                    daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                    totcount = cn.Count();
                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.AdminID == SessionWrapper.AdminID
                             select a;
                    daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                    totcount = cn.Count();
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                             select a;
                    daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                    totcount = cn.Count();
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {

                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.CompID == SessionWrapper.CompanyID
                             select a;
                    daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                    totcount = cn.Count();

                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    var cn = from a in db.gpscertificate
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID

                             where a.CUser == SessionWrapper.UserID
                             select a;
                    daycount = cn.Where(xx => EntityFunctions.DiffDays(xx.Cdate, DateTime.UtcNow) <= 0).Count();
                    totcount = cn.Count();
                }
                else if (SessionWrapper.AdminID == null)
                {
                    return Json("SessionClear", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("UnAuthenticated", JsonRequestBehavior.AllowGet);
                }

            }

            return Json(new { gpsdayCount = daycount, gpstotCount = totcount }, JsonRequestBehavior.AllowGet);
        }
        [OutputCache(CacheProfile = "10MinsCache")]
        public JsonResult RecentActions()
        {
            DateTime startTime = DateTime.UtcNow.Date;
            DateTime endTime = DateTime.UtcNow;
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID

                             where a.E_Stat==true//  EntityFunctions.DiffDays(a.Cdate, DateTime.UtcNow) <= 5
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,

                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 Cdate = a.Cdate,

                                 CalibrationDate = a.CalibrationDate

                             };
                    //var cali = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList();
                 //   var rec = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList();
                    return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);

                    //var jsonResult = Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RecentDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);
                    //jsonResult.MaxJsonLength = int.MaxValue;
                    //return jsonResult;
                }

                else if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.AdminID == SessionWrapper.AdminID && a.E_Stat==true //&& EntityFunctions.DiffDays(a.Cdate, DateTime.UtcNow) <= 5
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,

                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 Cdate = a.Cdate,

                                 CalibrationDate = a.CalibrationDate

                             };
                   // return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RecentDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);
                    return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);


                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Inspection)
                {
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.AdminID == (db.InspectionMst.FirstOrDefault(z => z.InspID == SessionWrapper.CompanyID).AdminID)
                             && a.E_Stat==true
                             //&& EntityFunctions.DiffDays(a.Cdate, DateTime.UtcNow) <= 5
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,

                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 Cdate = a.Cdate,

                                 CalibrationDate = a.CalibrationDate

                             };
                   // return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RecentDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);
                    return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);


                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyAdmin)
                {
                    //var cert = from a in db.certificate_mst join c in db.countries
                    //           on a.Country equals c.CountryCode join b in db.CompanyMst
                    //           on a.DealerId equals b.CompID select new 
                    //           CertificateView{}
                    //var cert = db.certificate_mst.Include(c => c.con);
                    //cert = from a in cert where a.DealerId == SessionWrapper.CompanyID select a;
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where b.CompID == SessionWrapper.CompanyID
                            // && EntityFunctions.DiffDays(a.Cdate, DateTime.UtcNow) <= 5
                            && a.E_Stat==true
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,

                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 Cdate = a.Cdate,

                                 CalibrationDate = a.CalibrationDate

                             };


                    //return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RecentDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);
                    return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);


                }
                else if (SessionWrapper.AdminLevel == AdminLevel.CompanyUser)
                {
                    var cn = from a in db.certificate_mst
                             join c in db.countries on a.Country equals c.CountryCode
                             join b in db.CompanyMst on a.DealerId equals b.CompID
                             where a.Cuser == SessionWrapper.UserID
                             //&& EntityFunctions.DiffDays(a.Cdate, DateTime.UtcNow) <= 5
                             && a.E_Stat==true
                             select new CertificateView
                             {
                                 CertificateNo = a.CertificateNo,
                                 VehRegistrationNo = a.VehRegistrationNo != "" ? a.VehRegistrationNo : "N/A",
                                 OwnerName = a.OwnerName,

                                 SerialNo = a.SerialNo,
                                 TamperSealNo = a.TamperSealNo,
                                 InstallDate = a.InstallDate,
                                 ExpDate = a.ExpDate,
                                 Cdate = a.Cdate,
                                 CalibrationDate = a.CalibrationDate

                             };

                   // return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RecentDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);
                    return Json(new { Calibration = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) >= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList(), Recently = cn.Where(xx => EntityFunctions.DiffDays(xx.CalibrationDate, DateTime.Now) <= RecentDays && EntityFunctions.DiffDays(xx.InstallDate, xx.CalibrationDate) <= RenewalDays).OrderByDescending(xx => xx.Cdate).ToList() }, JsonRequestBehavior.AllowGet);


                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }

            }

        }
    }
}
