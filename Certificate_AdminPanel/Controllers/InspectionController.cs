﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Certificate_AdminPanel.Models;
using Certificate_AdminPanel.ViewModels;
namespace Certificate_AdminPanel.Controllers
{
    [Authorize]
    public class InspectionController : Controller
    {
        string password_create;
        string loginname_create;
        //
        // GET: /Inspection/
        [Audit]
        public ActionResult Index()
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    var InspList = (from a in db.InspectionMst
                                    join b in db.AdminMst on a.AdminID equals b.AdminID
                                    join c in db.GMT_ZONE on a.TimeZoneID equals c.tab_id
                                    join d in db.UserMst on a.InspID equals d.CompID
                                    join e in db.LoginMst on d.UserID equals e.UserID
                                    where a.E_Stat == true
                                    select new InspectionView
                                    {
                                        InspID = a.InspID,
                                        Name = a.Name,
                                        DisplayName = a.DisplayName,
                                        LoginName = e.LoginName,
                                        Password = e.pwd,
                                        MobileNo = a.MobileNo,
                                        Email = a.Email,
                                        CDate = a.CDate,
                                        MDate = a.MDate,
                                        ExpDate = a.ExpDate,
                                        UserID = a.UserID,
                                        TimeZoneID = a.TimeZoneID,
                                        time_zone = c.time_zone,
                                        AdminID = a.AdminID,
                                        AdminName = b.Name,
                                        E_Stat = a.E_Stat,
                                    }).ToList();
                    return View(InspList.ToList());
                }
                else
                {
                    return View("UnAuthenticated");
                }
            }
        }
        //
        //Details:/Inspection/Details/1
    [Audit]
        public ActionResult Details(long id)
        {
           using(DatContext db= new DatContext())
           {
               if (SessionWrapper.AdminLevel == AdminLevel.Admin)
               {
                   InspectionMaster _insp = db.InspectionMst.Find(id);

                   AdminMaster admn = db.AdminMst.Find(_insp.AdminID);
                   GmtZone GMT = db.GMT_ZONE.Find(_insp.TimeZoneID);
                   UserMaster usr = db.UserMst.FirstOrDefault(a => a.CompID == _insp.InspID);
                   LoginMaster lgn = db.LoginMst.Find(usr.UserID);


                   return PartialView(new InspectionView
                   {
                       InspID = _insp.InspID,
                       Name = _insp.Name,
                       DisplayName = _insp.DisplayName,
                       LoginName = lgn.LoginName,
                       Password = lgn.pwd,
                       MobileNo = _insp.MobileNo,
                       Email = _insp.Email,
                       CDate = _insp.CDate,
                       MDate = _insp.MDate,
                       ExpDate = _insp.ExpDate,
                       UserID = _insp.UserID,
                       TimeZoneID = _insp.TimeZoneID,
                       time_zone = GMT.time_zone,
                       AdminID = admn.AdminID,
                       AdminName = admn.Name,
                       E_Stat = _insp.E_Stat,
                   });
               }
               else
               {
                   return View("UnAuthenticated");
               }

           }
        }

        //
        // GET: /Inspection/Create
       
        public ActionResult Create()
        {
            
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                filldropdowns();
                return PartialView();
            }
            
            else
            {
                return View("UnAuthenticated");
            }
            
        }
        //
        // POST: /Inspection/Create

        [HttpPost,Audit]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InspectionMaster _inspectionMaster)
        {
            loginname_create = null;
            password_create = null;
            using (DatContext db = new DatContext())
            {
                var x = this.ModelState.Keys.SelectMany(key => this.ModelState[key].Errors);
                if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {

                    if (!string.IsNullOrEmpty(Request["loginname"].ToString()))
                    {
                        loginname_create = Request["loginname"].ToString();
                    }
                    else
                    {
                        ModelState.AddModelError("loginname", "Login name required");
                    }
                    if (!string.IsNullOrEmpty(Request["password"].ToString()))
                    {
                        password_create = Request["password"].ToString();
                    }
                    else
                    {
                        ModelState.AddModelError("password", "password  required");
                    }

                    //if (string.IsNullOrEmpty(lname))
                    //{ ModelState.AddModelError("loginname", "Login name required"); }
                    if (db.LoginMst.Count(l => l.LoginName == loginname_create) > 0)
                    {
                        ModelState.AddModelError("loginname", "Login name already exists");
                    }
                    else if (_inspectionMaster.CDate >= _inspectionMaster.ExpDate)
                    {
                        ModelState.AddModelError("ExpDate", "Expiry date should be after Created date !");
                    }

                    if (ModelState.IsValid)
                    {

                        //enter login name and password to login mst at the time of company creation
                        DateTime cdate = DateTime.UtcNow;
                        _inspectionMaster.MDate = cdate;
                        _inspectionMaster.UserID = SessionWrapper.AdminID;
                        _inspectionMaster.E_Stat = true;

                        if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                        {
                            _inspectionMaster.AdminID = SessionWrapper.AdminID;
                        }
                        LoginMaster objlogin = new LoginMaster();

                        UserMaster obj_usermst = new UserMaster();

                        //login details

                        objlogin.CDate = _inspectionMaster.CDate;
                        objlogin.E_Stat = _inspectionMaster.E_Stat;
                        objlogin.LoginName = loginname_create;
                        objlogin.Password = CompanyController.GetMD5Hash(password_create.ToString());
                        objlogin.pwd = password_create.ToString();
                        objlogin.RoleID = 3;
                        objlogin.Type = 2;
                        //Type= 0 for Company Admin
                        //Type=1 for Compnay User
                        //Type=2 for inspection 

                        //user details
                        obj_usermst.MDate = cdate;
                        obj_usermst.Mobile = _inspectionMaster.MobileNo;
                        obj_usermst.MUserID = _inspectionMaster.UserID;
                        obj_usermst.Name = _inspectionMaster.Name;
                        obj_usermst.PhoneNo = "000";
                        obj_usermst.RegDate = _inspectionMaster.CDate;
                        obj_usermst.CDate = _inspectionMaster.CDate;
                        obj_usermst.Address = "NILL";
                        obj_usermst.E_Stat = _inspectionMaster.E_Stat;

                        try
                        {


                            db.InspectionMst.Add(_inspectionMaster);
                            db.SaveChanges();
                            obj_usermst.CompID =_inspectionMaster.InspID;
                            db.UserMst.Add(obj_usermst);
                            db.SaveChanges();
                            objlogin.UserID = obj_usermst.UserID;
                            db.LoginMst.Add(objlogin);
                            db.SaveChanges();


                        }
                        catch
                        {
                            throw;
                        }

                        return RedirectToAction("Index");
                    }


                    filldropdowns();

                    return View(_inspectionMaster);
                }
                return View("UnAuthenticated");
            }
        }
          
      
        //
        // GET:/Inspection/Edit
       
        public ActionResult Edit(long? id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                using (DatContext db = new DatContext())
                {
                    InspectionMaster _inspectionMaster = db.InspectionMst.Find(id);
                    filldropdowns();
                    return PartialView(_inspectionMaster);
                }
            }
            else
                return View("UnAuthenticated");
        }
        //
        //POST:/Inspection/Edit
        [HttpPost,Audit,ValidateAntiForgeryToken]
        public ActionResult Edit(InspectionMaster _inspectionMaster)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                using (DatContext db = new DatContext())
                {
                    InspectionMaster _insp = db.InspectionMst.Find(_inspectionMaster.InspID);
                    _insp.Name = _inspectionMaster.Name;
                    _insp.DisplayName = _inspectionMaster.DisplayName;
                    _insp.MobileNo = _inspectionMaster.MobileNo;
                    // _insp.CDate = _inspectionMaster.CDate;
                    _insp.MDate = DateTime.UtcNow;
                    _insp.ExpDate = _inspectionMaster.ExpDate;
                    _insp.Email = _inspectionMaster.Email;
                    _insp.UserID = _inspectionMaster.UserID;
                    _insp.TimeZoneID = _inspectionMaster.TimeZoneID;
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        _insp.AdminID = _inspectionMaster.AdminID;
                    }

                    _insp.E_Stat = _inspectionMaster.E_Stat;
                    if (ModelState.IsValid)
                    {
                        db.Entry(_insp).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                    filldropdowns();
                    return View();

                }
            }
            else
                return View("UnAuthenticated");
        }

        //
        //GET:Inspection/Delete
       
        public ActionResult Delete(long id)
        {
            using (DatContext db = new DatContext())
            {
                if (SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
                {
                    InspectionMaster _inspectionMaster = db.InspectionMst.SingleOrDefault(c => c.InspID == id && c.AdminID == SessionWrapper.AdminID);
                    return PartialView(_inspectionMaster);
                }
                else if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                {
                    InspectionMaster _inspectionMaster = db.InspectionMst.Find(id);
                    return PartialView(_inspectionMaster);
                }
            }
            return View("UnAuthenticated");

        }
        //
        //POST:Inspection/Delete
        [Audit]
        [HttpPost,ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            if (SessionWrapper.AdminLevel == AdminLevel.Admin || SessionWrapper.AdminLevel == AdminLevel.SubAdmin)
            {
                using (DatContext db = new DatContext())
                {
                    InspectionMaster _inspectionMaster = db.InspectionMst.Find(id);

                    db.Database.ExecuteSqlCommand("exec sp_delete_Inspection @CompID", new SqlParameter("@CompID", id));
                    //db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View("UnAuthenticated");
        }
        [NonAction]
        private void  filldropdowns()
        {

            using (var db = new DatContext())
            {
                try
                {
                    ViewBag.bind_drop_zone = new SelectList(db.GMT_ZONE, "tab_id", "time_zone", "tab_id").ToList();
                    if (SessionWrapper.AdminLevel == AdminLevel.Admin)
                    {
                        var res = (from sd in db.AdminMst where sd.Type == 1 select sd).ToList();
                        ViewBag.AdminID = new SelectList(res, "AdminID", "Name");
                    }
                }

                catch { throw; }
            }
        }
    }
}
