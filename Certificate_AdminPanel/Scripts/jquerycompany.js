﻿$(document).ready(function () {
    
  //$("#example").dataTable({
  //     "bJQueryUI": true,
  //      "sScrollX": "100%",
  //      //"bFilter": false,
  //      //"bPaginate": false,
  //      //"bInfo":false,
  //      //"bLengthChange":false,
  //      //"bProcessing": true,
  //      "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
  //      "iDisplayLength": 10,
  //      //"bAutoWidth": true,
  //      "bAutoWidth": false,
  //      "bScrollAutoCss": true, "bScrollCollapse": true,
  //      "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
  //      "aoColumnDefs": [    
  //    { 'bSortable': false, 'aTargets': [4] }],
  //      "sPaginationType": "full_numbers",
  //      //"sDom": '<"top"if>rt<"bottom"lp><"clear">'
  //      //   "bPaginate":true,
  //      //"sPaginationType":"full_numbers",
  //     // "sDom": 'T<"clear">lfrtip',
  //     // "oColVis": { "activate": "mouseover", "buttonText": "Change columns", "aiExclude": [11] },
  //      //"oTableTools": {
  //      //    "sSwfPath": "../content/swf/copy_csv_xls_pdf.swf", "aButtons": ["csv", "xls", "pdf"]
  //      //}
  //})
  //    //.columnFilter({"sPlaceHolder": "head:before", "aoColumns": [{ "type": "text" }, { "type": "text" }, null, null, null, null, null, { "type": "text" }, { "type": "text" }, { "type": "text" }]
    
  //  //});
    
  // //new FixedHeader(oTable, { "bottom": true });
  $('#example').DataTable({
      responsive: true,
      scrollY: "400px",
      scrollX: true,
      scrollCollapse: true,
      paging: true,
      fixedColumns: {
          leftColumns: 1,
          rightColumns: 1
      },
      "sDom": 'T<"clear">lfrtip',
      "oTableTools": {
          "aButtons": [
              "xls",
              "print"],
          "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf"
      }

  });

    //$("#Create").click(function (e) {
    //    e.preventDefault();
    //    var url = $(this).attr('href');
    //    $("#dialog-create").dialog({
    //        draggable: false,
    //        resizable: false,
    //        height:530,
    //        width: 690,
    //        show: { effect: 'drop', direction: "up" },
    //        modal: true,
    //        draggable: false,
    //        open: function (event, ui) {
    //            $(this).load(url);
    //           //$.validate.unobtrusive.parse("myform");
       
    //            }, close: function (event, ui) {
    //                $(this).dialog('close');
    //        }
    //    });
    //    $("#dialog-create").dialog('open'); return false;
    //});
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 450,
            width: 625,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 190,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-delete").dialog('open');

        return false;
    });
    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 500,
            width: 365,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })

    
    //$('#Name').keypress(function (e) {
    //    if ($(this).val().length > 250) {
    //        e.preventDefault();
    //    }
    //});
    //$('#DisplayName').keypress(function (e) {
    //    if ($(this).val().length > 250) {
    //        e.preventDefault();
    //    }
    //});
    //$('#Address').keypress(function (e) {
    //    if ($(this).val().length > 400) {
    //        e.preventDefault();
    //    }
    //});
    //$('#PhoneNo').keypress(function (e) {
    //    if ($(this).val().length > 15) {
    //        e.preventDefault();
    //    }
    //});
    //$('#MobileNo').keypress(function (e) {
    //    if ($(this).val().length > 15) {
    //        e.preventDefault();
    //    }
    //});
    //$('#Email').keypress(function (e) {
    //    if ($(this).val().length > 40) {
    //        e.preventDefault();
    //    }
    //});
});

