﻿$(document).ready(function () {

    $("#example").dataTable({
        "bJQueryUI": true,
        "sScrollX": "100%",
        "sPaginationType": "full_numbers",
        "bDestroy": true,
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },

    })
});