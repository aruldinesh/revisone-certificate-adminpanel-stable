﻿
$(document).ready(function () {
    //$("#example").dataTable({
    //    "bJQueryUI": true,
    //    "sScrollX": "100%",
    //    "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
    //    "sPaginationType": "full_numbers",
    //    "bAutoWidth": false,
    //    "bScrollAutoCss": true, "bScrollCollapse": true,
    //    //"aocolumns": [{ "sWidth": "20%" }, { "sWidth": "20%" }, { "sWidth": "20%" }, null, null, null, null, { "sWidth": "20%" }, null, null, { "sWidth": "20%" }, { "sWidth": "20%" }],
    //    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [3] }],
    //})
    $('#example').DataTable({
        responsive: true,
        scrollY: "400px",
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        }
    });

    //$("#Create").click(function (e) {
    //    e.preventDefault();
    //    var url = $(this).attr('href');
    //    $("#dialog-create").dialog({
    //        draggable: false,
    //        resizable: false,
    //        height: 350,
    //        width: 350,
    //        show: { effect: 'drop', direction: "up" },
    //        modal: true,
    //        draggable: false,
    //        open: function (event, ui) {
    //            $(this).load(url);
    //        }, close: function (event, ui) {
    //            $(this).dialog('close');
    //        }
    //    });
    //    $("#dialog-create").dialog('open'); return false;
    //});
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 260,
            width: 320,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });
});