﻿function GetRegistrationDate(controlID) {
    $(document).ready(function () {
        var today = new Date();
        $(controlID).datepicker({ maxDate: today, dateFormat: "dd/M/yy", changeYear: true, changeMonth: true, showOn: 'button', buttonImageOnly: true, buttonImage: '../../Content/Images/DatetimePicker/icon_cal.png' });
    });
}


//
function GetExpDate(controlID) {
    $(document).ready(function () {

        var tomorrow = new Date(new Date().getTime() + (24 * 60 * 60 * 1000));
        $(controlID).datepicker({ minDate: tomorrow, dateFormat: "dd/M/yy", changeYear: true, changeMonth: true, showOn: 'button', buttonImageOnly: true, buttonImage: '../../Content/images/DatetimePicker/icon_cal.png' });
    });
}
$(window).load(function () {
    $('#loading').hide();
});