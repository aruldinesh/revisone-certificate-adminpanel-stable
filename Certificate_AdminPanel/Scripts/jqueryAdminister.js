﻿$(document).ready(function () {
    $("#example").dataTable({
        "bJQueryUI": true,
        "sScrollX": "100%",
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
           "bScrollAutoCss": true,"bScrollCollapse": true,
        //"aocolumns": [{ "sWidth": "20%" }, { "sWidth": "20%" }, { "sWidth": "20%" }, null, null, null, null, { "sWidth": "20%" }, null, null, { "sWidth": "20%" }, { "sWidth": "20%" }],
           "aoColumnDefs": [ { 'bSortable': false, 'aTargets': [4] }],
    })
        //.columnFilter({ "sPlaceHolder": "head:before", "aoColumns": [{ "type": "text" }, { "type": "text" }, { "type": "text" }, null, null, null, null, null, null, { "type": "text" }, { "type": "text" }, null, null] });
    $("#Create").click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $("#dialog-create").dialog({
            draggable: false,
            resizable: false,
            height: 465,
            width: 645,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-create").dialog('open'); return false;
    });
    $(document).on('click','.editimage',function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height:450,
            width: 605,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 190,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-delete").dialog('open');

        return false;
    });
    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 500,
            width: 365,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })


})
