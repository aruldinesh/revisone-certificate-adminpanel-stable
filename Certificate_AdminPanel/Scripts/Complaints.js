﻿$(document).ready(function () {
    //$("#example").dataTable({
    //    "bJQueryUI": true,
    //    //"aaSorting": [[1, "desc"]],
    //    "aoColumnDefs": [ { 'bSortable': false, 'aTargets': [0] }],
    //    "sScrollX": "100%",
    //    "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
    //    "sPaginationType": "full_numbers",
    //    "bAutoWidth": false,
    //    "bScrollAutoCss": true, "bScrollCollapse": true,
       
    //})
    $('#example').DataTable({
        responsive: true,
        scrollY: "400px",
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        },
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                "xls",
                "print"],
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf"
        }
      
    });

    $("#Create").click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $("#dialog-create").dialog({
            draggable: false,
            resizable: false,
            height: 530,
            width:380,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
                //$.validate.unobtrusive.parse("myform");

            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-create").dialog('open'); return false;
    });
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 475,
            width: 350,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 190,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-delete").dialog('open');

        return false;
    });
    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 500,
            width: 365,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })
});