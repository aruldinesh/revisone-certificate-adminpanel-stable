﻿$(document).ready(function () {
    
    $('#example').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": false,
        "aoColumns": [
                                   { "data": "SerialNo" },

                                   {
                                       "data": "Date",
                                       "render": function (data, type, row) {
                                           var date = new Date(data);
                                           return formatDate(date);
                                       }
                                   },

                                   { "data": "Status" },
                                   { "data": "DealerName" },
                                   { "data": "Used" },

               {
                   "render": function (data, type, row) {

                       var action = '<a href="/gpscertifcate/SerialNoEdit/' + row.Recno + '"title="Edit" class="iframedetails" style="text-decoration:none"> <span class="glyphicon glyphicon-edit"></span></a>'
                       return edit;
                   }
               },

        ],
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                "xls",
                "print"],
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf"
        },
        "rowCallback": function (row, data, displayIndex) {
            if ($.inArray(data.DT_RowId, selected) !== -1) {
                $(row).addClass('selected');
            }
        }
    });

    var selected = [];



    $('#example tbody').on('click', 'tr', function () {

        var id = this.id;

        var index = $.inArray(id, selected);

        if (index === -1) {
            selected.push(id);
        } else {
            selected.splice(index, 1);
        }

        $(this).toggleClass('selected');
    });

    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 265,
            width: 280,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });
});