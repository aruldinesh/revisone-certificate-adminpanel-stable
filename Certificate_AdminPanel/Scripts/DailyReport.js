﻿$(document).ready(function () {
    $.datepicker.regional[""].dateFormat = 'dd/M/yy';
    $.datepicker.setDefaults($.datepicker.regional['']);


    var table = $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "sDom": 'T<"clear">lfrtip',
        "oTableTools": {
            "aButtons": [
                "xls","pdf" ,

                "print"],
            "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf"
        }

    });
    new $.fn.dataTable.Responsive(table);
   
    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 690,
            width: 380,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })

})