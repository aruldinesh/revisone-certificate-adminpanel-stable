﻿function CompChange() {
    var selectedProvinceId = $('#AdminID option:selected').attr('value');
    //var selectedCompID = $('#DealerDD option:selected').attr('value');
    if (selectedProvinceId != "") {
        $.getJSON("../../../../../Certificate/AdmnCompNames", { provinceId: selectedProvinceId }, function (comNames) {
            // comNames.preventDefault();
            var citiesSelect = $('#Dealerdd');
            $('#Dealerdd').html("");
            $.each(comNames, function (index, item) {
                citiesSelect.append($('<option></option>').val(item.Value).html(item.Text));
            });
        });
    }
    else {
        $('#Dealerdd').html("");
    }

}
$(document).ready(function () {
    //$("#example").dataTable({
    //    "aaSorting": [[3, "desc"]],
    //    "aoColumnDefs": [{ 'bSortable': false, 'aTargets': [7] }],//{ "bVisible": false, "aTargets": [5] }
    //    "bJQueryUI": true,
    //    "sScrollX": "100%",
    //    "bAutoWidth": false,
    //    "bScrollAutoCss": true, "bScrollCollapse": true,
    //    "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    //    "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
    //    //"aoColumnDefs":[{"bsortable":false, "aTargets":[18]}],

    //    "sPaginationType": "full_numbers",

    //    //"oLanguage": { "sProcessing": "processing" }
    //});
    $('#example').DataTable({
        responsive: true,
        scrollY: "400px",
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: {
            leftColumns: 1,
            rightColumns: 1
        }
    });

    $(document).on('click', '.repair', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 310,
            width: 350,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit1").dialog({
            resizable: false,
            height: 590,
            width: 720,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit1").dialog('open');

        return false;
    });
    //$(".deleteimage").click(function () {
    //        $.fancybox({
    //            'width': '70%', // or whatever
    //            'height': '90%',
    //            'autoDimensions': false,
    //            'content': '<embed src="' + this.href + '#nameddest=self&page=1&view=FitH,0&zoom=80,0,0" type="application/pdf" height="99%" width="100%" />',
    //            'onClosed': function () {
    //                $("#fancybox-inner").empty();
    //            }
    //        });
    //        return false;
    //}); // pdf 

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 190,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-delete").dialog('open');

        return false;
    });
    $(document).on('click', '.reissue', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-reissue").dialog({
            resizable: false,
            height: 250,
            width: 350,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-reissue").dialog('open');

        return false;
    });

    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 690,
            width: 380,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })

});


