﻿$(document).ready(function () {
    $("#example").dataTable({
        //"bJQueryUI": true,
        "sScrollX": "100%",
        "bAutoWidth": false,
        "bScrollAutoCss": true, "bScrollCollapse": true,
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },


        "sPaginationType": "full_numbers",


    });


$("#Create").click(function (e) {
    e.preventDefault();
    var url = $(this).attr('href');
    $("#dialog-create").dialog({
        draggable: false,
        resizable: false,
        height: 530,
        width: 690,
        show: { effect: 'drop', direction: "up" },
        modal: true,
        draggable: false,
        open: function (event, ui) {
            $(this).load(url);
            //$.validate.unobtrusive.parse("myform");

        }, close: function (event, ui) {
            $(this).dialog('close');
        }
    });
    $("#dialog-create").dialog('open'); return false;
});
});