﻿$(document).ready(function () {

    $("#example").dataTable({
         "aaSorting": [[2, "desc"]],
        "bJQueryUI": true,
        "sScrollX": "100%",
        "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "bScrollAutoCss": true, "bScrollCollapse": true,
        //"sDom": 'T<"clear">lfrtip',
        //"oTableTools": {
        //    "aButtons": [
        //        "xls",
        //        "print"],
        //    "sSwfPath": "../../Content/swf/copy_csv_xls_pdf.swf"
        //},

        //"aocolumns": [{ "sWidth": "20%" }, { "sWidth": "20%" }, { "sWidth": "20%" }, null, null, null, null, { "sWidth": "20%" }, null, null, { "sWidth": "20%" }, { "sWidth": "20%" }],
        //"aoColumnDefs": [{ 'bSortable': false, 'aTargets': [3] }],
    });
$(document).on('click', '.editimage', function (e) {
    e.preventDefault();
    var url = $(this).parent().attr('href');
    $("#dialog-edit").dialog({
        resizable: false,
        height: 300,
        width: 560,
        show: { effect: 'drop', direction: "up" },
        modal: true,
        draggable: false,
        open: function (event, ui) {
            $(this).load(url);
        }, close: function (event, ui) {
            $(this).dialog('close');
        }
    });
    $("#dialog-edit").dialog('open');

    return false;
});
$(document).on('click', '.deleteimage', function (e) {
    e.preventDefault();
    var url = $(this).parent().attr('href');
    $("#dialog-delete").dialog({
        resizable: false,
        height: 180,
        width: 300,
        show: { effect: 'drop', direction: "up" },
        modal: true,
        draggable: false,
        open: function (event, ui) {
            $(this).load(url);
        },
        close: function (event, ui) {
            $(this).dialog('close');
        }
    });
    $("#dialog-delete").dialog('open');

    return false;
});
})