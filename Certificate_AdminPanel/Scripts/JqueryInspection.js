﻿$(document).ready(function () {
    $("#example").dataTable({
        "bJQueryUI": true,
        "sScrollX": "100%",
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
        "sPaginationType": "full_numbers",
        "bAutoWidth": false,
        "bScrollAutoCss": true, "bScrollCollapse": true,
        "aoColumnDefs": [
            //{ "sWidth": "180px", "aTargets": [0, 4, 6, 7] },
            { 'bSortable': false, 'aTargets': [5] }]
    })
    //.columnFilter({ "sPlaceHolder": "head:before", "aoColumns": [{ "type": "text" }, null, null, null, { "type": "text" }, null, { "type": "text" }, { "type": "text" }, null] });
    $("#Create").click(function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $("#dialog-create").dialog({
            draggable: true,
            resizable: false,
            height: 460,
            width: 680,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-create").dialog('open'); return false;
    });
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 360,
            width: 600,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 180,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-delete").dialog('open');

        return false;
    });
    $(document).on('click', '.details', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-details").dialog({
            resizable: false,
            height: 690,
            width: 380,
            show: { effect: 'clip', direction: "down" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-details").dialog('open');

        return false;
    })
    $('#UserMst_PhoneNo').keypress(function (e) {
        if ($(this).val().length > 15) {
            e.preventDefault();
        }
    });
    $('#UserMst_Mobile').keypress(function (e) {
        if ($(this).val().length > 15) {
            e.preventDefault();
        }
    });
 
});