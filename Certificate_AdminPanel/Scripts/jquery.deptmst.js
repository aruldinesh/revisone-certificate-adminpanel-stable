﻿function CompChange() {
    var selectedProvinceId = $('#CompID option:selected').attr('value');
    if (selectedProvinceId != "") {
        $.getJSON("../../../../Department/ComDeptNames", { provinceId: selectedProvinceId }, function (DeptNames) {
            var citiesSelect = $('#UpperDeptID');
            $('#UpperDeptID').html("");
            citiesSelect.append($('<option></option>').val("").html("-Select-"));
            $.each(DeptNames, function (index, item) {
                citiesSelect.append($('<option></option>').val(item.Value).html(item.Text));
            });
        });
    }
    else {
        $('#UpperDeptID').html("");
    }
}

$(document).ready(function () {
    $("#example").dataTable({
        "bJQueryUI": true,
        "sScrollX": "100%",
        //"bPaginate": false,
        "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "oLanguage": { "oPaginate": { "sFirst": "&lt&lt", "sLast": "&gt&gt", "sNext": "&gt", "sPrevious": "&lt" }, },
      "aoColumnDefs": [
      { "sWidth": "180px", "aTargets": [0, 1, 2, 4, 5, 8]},{'bSortable': false, 'aTargets': [9] }],
      //"aoColumnDefs":[{'bSortable': false, 'aTargets': [9]}],
        //"bAutoWidth":true,
        "iDisplayLength": 10,
        "sPaginationType": "full_numbers",
    }).columnFilter({ "sPlaceHolder": "head:before", "aoColumns": [{ "type": "text" }, { "type": "text" }, { "type": "text" }, null, { "type": "text" }, { "type": "text" }, null, null, { "type": "text" }] });

    $("#Create").click(function (e) {
        //e.preventDefault(); 
        var url = $(this).attr('href');
        $("#dialog-create").dialog({
            resizable: false,
            height:380,
            width: 710,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-create").dialog('open'); return false;
    });
    //$(".editimage").click(function(e){
    $(document).on('click', '.editimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-edit").dialog({
            resizable: false,
            height: 380,
            width: 580,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
            open: function (event, ui) {
                $(this).load(url);
            }, close: function (event, ui) {
                $(this).dialog('close');
            }
        });
        $("#dialog-edit").dialog('open');

        return false;
    });

    $(document).on('click', '.deleteimage', function (e) {
        e.preventDefault();
        var url = $(this).parent().attr('href');
        $("#dialog-delete").dialog({
            resizable: false,
            height: 190,
            width: 300,
            show: { effect: 'drop', direction: "up" },
            modal: true,
            draggable: false,
           
            open: function (event, ui) {
                $(this).load(url);
            },
            close: function (event, ui) {
                $(this).dialog('close');
            },
            
        });

        $("#dialog-delete").dialog('open');

        return false;

    });
    $('#Name').keypress(function (e) {
        if ($(this).val().length > 95) {
            e.preventDefault();
        }
    });
    $('#ContPerson').keypress(function (e) {
        if ($(this).val().length > 95) {
            e.preventDefault();
        }
    });
    $('#Email').keypress(function (e) {
        if ($(this).val().length > 95) {
            e.preventDefault();
        }
    });
    $('#PhoneNo').keypress(function (e) {

        if ($(this).val().length > 18) {
            e.preventDefault();
        }
    });
    $('#MobileNo').keypress(function (e) {
        if ($(this).val().length > 18) {
            e.preventDefault();
        }
    });

});





