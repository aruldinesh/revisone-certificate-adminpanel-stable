﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Certificate_AdminPanel.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Certificate_AdminPanel.ViewModels
{
    //[Table("certificate_mst")]
    public class CertificateView
    {
        [Required,Key,DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CertificateNo { get; set; }

        //[Required]
        [DataType(DataType.Text)]
        [Display(Name = "Vehicle Reg No")]
        public string VehRegistrationNo { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Owner Name")]
        public string OwnerName { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Chassis No")]
        public string ChassisNo { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Engine No")]
        public string EngineNo { get; set; }

        [Required]
        [Display(Name = "Manufacture Year")]
        public Int32 ManufactureYear { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Make Of Vehicle")]
        public string MakeOfVehicle { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Model Of Vehicle")]
        public string ModelOfVehicle { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Type Of Speed Limiter")]
        public string TypeSpeedLimiter { get; set; }

        [Required]
        [Display(Name = "Set Speed")]
        public Int32 SetSpeed { get; set; }
        [Display(Name="Speed")]
        public Int32? speed2 { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Serial No")]
        public string SerialNo { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Tamper Seal No")]
        public string TamperSealNo { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]
        [Display(Name = "Install Date")]
        public DateTime InstallDate { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Expiry Date")]
        public DateTime ExpDate { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Technician Name")]
        public string TechnicianName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Applicable Standard")]
        public string ApplicableStandard { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        [Required]
        [Display(Name = "Dealer ID")]
        public Int64 DealerId { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Created Date")]
        public DateTime Cdate { get; set; }

        [Required]
        [Display(Name = "C User")]
        public Int64 Cuser { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Modified Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        public DateTime Mdate { get; set; }

        [Required]
        [Display(Name = "M User")]
        public Int64 Muser { get; set; }

        [Required]
        [Display(Name = "EStat")]
        public bool E_Stat { get; set; }

        [Required]
        [Display(Name = "Duplicate")]
        public Int32 Duplicate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CalibrationDate { get; set; }
        public string CountryName { get; set; }
        [Key]
        public Int64 CompID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNo { get; set; }

        public string MobileNo
        {
            get;
            set;

        }

        public string ImageUrl { get; set; }

        public string Expired { get; set; }

        public string DealerLogo { get; set; }

        public DateTime? fromdate { get; set; }
        
        public DateTime? todate { get; set; }
        public Int64 AdminID { get; set; }
        public string AdminAddress { get; set; }
        public string AdminLogo { get; set; }
        public string  AdminName { get; set; }
        public string AdminPhone { get; set; }
        public string AdminEmail { get; set; }
        public string AdminTemplate { get; set; }


        public Int64? Dealerdd { get; set; }
        //public string SpeedLimiter_text { get; set; }
        //public string Orient_text { get; set; }
        //public string OwnerName_text { get; set; }
        //public string ResitrationNumber_text { get; set; }
        //public string ChassisNo_text { get; set; }
        //public string EngineNo_text { get; set; }
        //public string Modelofvehicle_text { get; set; }
        //public string yearofmanufacture_text { get; set; }

        //public string typeofspeedlimiter_text { get; set; }
        //public string SerialNo_text { get; set; }
        //public string DateofInstallation_text { get; set; }
        //public string Setspeedlimit_text { get; set; }
        //public string TechnicianName_text { get; set; }
        //public string DealerName_text { get; set; }
        //public string AddressPhone_text { get; set; }

        //public string ValidUpto_text { get; set; }
        //public string ThreeYear_text { get; set; }
        //public string DealerStamp_text { get; set; }
       


    }
}