﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.ViewModels
{
    public class spareview
    {
        public int Id { get; set; }
        [Required]
        public string ComponentId { get; set; }
        [Required]
        public string Typeofcomponent { get; set; }
        public DateTime date { get; set; }
        [DataType(DataType.Date)]
        public DateTime? ReceivedDate { get; set; }
        public Int32 Quantity { get; set; }
        public Int32 Used { get; set; }
        [Required]
        public int dealerid { get; set; }
        public int Balance { get; set; }
        public int TotalBalance { get; set; }       
        public string summary { get; set; }
        public string DealerName { get; set; }
        public Int32? DealerDD { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        
    }
}