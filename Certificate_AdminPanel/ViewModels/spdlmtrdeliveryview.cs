﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Certificate_AdminPanel.Models;
namespace Certificate_AdminPanel.ViewModels
{

    public class SpdLmtrDeliveryView
    {
        [Key]
        public int Recno { get; set; }
       // [Required(ErrorMessage = "SerialNo required")]
        public string SerialNo { get; set; }
        //[Required(ErrorMessage="TamperSealNo required")]
        //public string TamperSealNo { get; set; }
        [Required]
        //public string SpeedLimiter { get; set; }
        //[Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [Required]
        public int User { get; set; }
        public int Total { get; set; }
        public int Except { get; set; }
        public bool chk { get; set; }
        public Int64 DealerId { get; set; }
        public string DealerName { get; set; }
        public bool E_Stat { get; set; }
        public string Status { get; set; }
        //public class TamperSealNoView
        //{
        public string Used { get; set; }
        public Int64? Dealerdd { get; set; }
        public DateTime? fromdate { get; set; }

        public DateTime? todate { get; set; }
        public string TamperSealNo { get; set; }
    }
       
    
}
