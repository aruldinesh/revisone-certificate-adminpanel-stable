﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Certificate_AdminPanel.Models;
namespace Certificate_AdminPanel.ViewModels
{
    public class UserView:Gena
    {
        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 UserID { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "User Name")]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNo { get; set; }

        [Display(Name = "Mobile Number")]
        public string Mobile { get; set; }


        [Display(Name = "LoginName")]
        [DataType(DataType.Text)]
        public string LoginName { get; set; }
        public string pwd { get; set; }
        
        [DataType(DataType.DateTime)]
        [Display(Name = "Register Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]
        public DateTime RegDate { get; set; }

        [Display(Name = "Created User")]
        public string Created { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Modified User")]
        public string Modified { get; set; }
        public int Type { get; set; }
        [DataType(DataType.DateTime)]
        [Display(Name = "Modified Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}",ApplyFormatInEditMode=true)]
        public DateTime MDate { get; set; }

        [Display(Name = "Company")]
        public string Company { get; set; }
        [NotMapped]
        public bool isLogin { get; set; }
    }
}