﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.ViewModels
{
    public class ComplaintsView
    {
        public int Id { get; set; }
        [Display(Name="Complaint No")]
        public string ComplaintNo { get; set; }
         [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Cdate { get; set; }

        public string Customer { get; set; }
   
        public string ProductDescription { get; set; }
        public string SerialNo { get; set; }
        public string ComplaintDetails { get; set; }
        public string Problem { get; set; }
        public string Reason { get; set; }
         [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? DateOfReturn { get; set; }
        public string Remark { get; set; }
        public Int64 Cuser { get; set; }
        public Int64 MUser { get; set; }
        public Int64 DealerId { get; set; }
        public bool Stat { get; set; }

        public string DealerName { get; set; }

        [Display(Name="Rectififed By")]
        public string RectifiedBy { get; set; }
    }
}