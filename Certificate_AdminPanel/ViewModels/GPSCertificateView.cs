﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel.ViewModels
{
    public class GPSCertificateView
    {
        [Required, Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CertificateNo { get; set; }

        //[Required]
        [DataType(DataType.Text)]
        [Display(Name = "Vehicle Reg No")]
        public string VehicleNo { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Owner Name")]
        public string OwnerName { get; set; }


        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Chassis No")]
        public string ChassisNo { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Engine No")]
        public string EngineNo { get; set; }

        [Required]
        [Display(Name = "Manufacture Year")]
        public Int32 ManufactureYear { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Make Of Vehicle")]
        public string MakeOfVehicle { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Model Of Vehicle")]
        public string ModelOfVehicle { get; set; }

        [Required]
        [DataType(DataType.Text)]
      
        public decimal fuelmileageratio { get; set; }
        public decimal OdometerReading { get; set; }
       
        public double Maxspeed { get; set; }
        public string SIMNO { get; set; }
        public string IMEINO { get; set; }
        public string SOSNO { get; set; }
        //public string OwnerName { get; set; }
        public string OwnerPhone { get; set; }
        public string DriverName { get; set; }
        public string DriverPhone { get; set; }
      
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Serial No")]
        public string TerminalId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Install Date")]
        public DateTime InstallDate { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Expiry Date")]
        public DateTime ExpDate { get; set; }
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Technician Name")]
        public string TechnicianName { get; set; }

    
        [Required]
        [Display(Name = "Dealer ID")]
        public Int64 DealerId { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Created Date")]
        public DateTime Cdate { get; set; }

        [Required]
        [Display(Name = "C User")]
        public Int64 Cuser { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Modified Date")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        public DateTime Mdate { get; set; }

        [Required]
        [Display(Name = "M User")]
        public Int64 Muser { get; set; }

        [Required]
        [Display(Name = "EStat")]
        public bool E_stat { get; set; }

        [Required]
        [Display(Name = "Duplicate")]
        public Int32 Duplicate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? CalibrationDate { get; set; }
        public string CountryName { get; set; }
        [Key]
        public Int64 CompID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string PhoneNo { get; set; }

        public string ImageUrl { get; set; }

        public string Expired { get; set; }

        public string DealerLogo { get; set; }

        public DateTime? fromdate { get; set; }

        public DateTime? todate { get; set; }
        public Int64 AdminID { get; set; }
        public string AdminAddress { get; set; }
        public string AdminLogo { get; set; }
        public string AdminName { get; set; }
        public string AdminPhone { get; set; }
        public string AdminEmail { get; set; }
        public string AdminTemplate { get; set; }


        public Int64? Dealerdd { get; set; }
        public string ApplicableStandard { get; set; }
        public string Country { get; set; }
    }
}