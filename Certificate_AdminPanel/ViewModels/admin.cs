﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Certificate_AdminPanel.Models;


namespace Certificate_AdminPanel.ViewModels
{
    public class admin : Gena
    {
        [Key, DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public Int64 LoginID { get; set; }

        [Required]
        public Int64 RoleID { get; set; }

        [Display(Name = "Login Name")]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9_@.-]*$", ErrorMessage = "Please insert proper value")]
        [Required]

        public string LoginName { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        //[Required]
        public string Password { get; set; }
        public string pwd { get; set; }
        public int Type { get; set; }

        public Int64 AdminID { get; set; }
        [ForeignKey("AdminID")]
        public virtual AdminMaster AdminMaster { get; set; }
        //[ForeignKey("LoginID")]
        //public virtual AdminLogin adminlog { get; set; }
        [Required]
        public string Name { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:[0-9]+(?:-[0-9])?)*$", ErrorMessage = "Please enter Proper phone number")]
        [StringLength(20)]
        public string PhoneNo { get; set; }
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:[0-9]+(?:-[0-9])?)*$", ErrorMessage = "Please enter Proper Mobile number")]
        [StringLength(20)]
        public string MobileNo { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "Invalid Email")]
        [Required]
        public string Email { get; set; }

        //public Int64 UserID { get; set; }

        public Int32 TimeZoneID { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}")]
        public DateTime MDate { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Logo { get; set; }


        [Required]
        //[DateGreaterThan("MDate")]
        [DisplayFormat(DataFormatString = "{0:dd'/'MMM'/'yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ExpDate { get; set; }


        [Key]
        public Int32 tab_id { get; set; }

        public string time_zone { get; set; }

        public string Gmt_minute { get; set; }
        public bool GPS_stat { get; set; }
        [NotMapped]
        public string TemplateName { get; set; }
        [NotMapped]
        public bool isLogin { get; set; }

    }

    public class LoggedInUser
    {
        public string Name { get; set; }
        public bool isLogin { get; set; }
        public DateTime LoginTime { get; set; }
        public bool State { get; set; }
    }
}