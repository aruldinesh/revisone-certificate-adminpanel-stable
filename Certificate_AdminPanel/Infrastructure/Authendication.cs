﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Security.Cryptography;
using System.Text;
using Certificate_AdminPanel.Models;
using System.Data;

namespace Certificate_AdminPanel.Infrastructure
{
    public class Authendications : MembershipProvider
    {


        public override string ApplicationName
        {
            get
            {
                return "Certificate_AdminPanel";
            }
            set { throw new NotImplementedException(); }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            DatContext db = new DatContext();
            LoginMaster objlogin = db.LoginMst.SingleOrDefault(l => l.LoginName == username && l.Password == GetMD5Hash(oldPassword));
            if (objlogin != null)
            {
                objlogin.Password = GetMD5Hash(newPassword);
                db.Entry(objlogin).State = EntityState.Modified;
                db.SaveChanges();
                db.Dispose();
                return true;
            }
            return false;
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            //ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);
            //OnValidatingPassword(args);

            //if (args.Cancel)
            //{
            //    status = MembershipCreateStatus.InvalidPassword;
            //    return null;
            //}

            //if (RequiresUniqueEmail && GetUserNameByEmail(email) != string.Empty)
            //{
            //    status = MembershipCreateStatus.DuplicateEmail;
            //    return null;
            //}

            //MembershipUser user = GetUser(username, true);

            //if (user == null)
            //{
            //    UserObj userObj = new UserObj();
            //    userObj.UserName = username;
            //    userObj.Password = GetMD5Hash(password);
            //    userObj.UserEmailAddress = email;

            //    UserRepository userRep = new UserRepository();
            //    userRep.RegisterUser(userObj);

            status = MembershipCreateStatus.Success;

            return GetUser("", true);
            //}
            //else
            //{
            //    status = MembershipCreateStatus.DuplicateUserName;
            //}

            //return null;
        }


        //public  MembershipUser CreateUser( RegUserModel rmodel,out MembershipCreateStatus status)
        //{
        //    ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(rmodel.login.LoginName, rmodel.login.Password, true);
        //    OnValidatingPassword(args);

        //    if (args.Cancel)
        //    {
        //        status = MembershipCreateStatus.InvalidPassword;
        //        return null;
        //    }

        //    if (RequiresUniqueEmail && GetUserNameByEmail(rmodel.user.Address) != string.Empty)
        //    {
        //        status = MembershipCreateStatus.DuplicateEmail;
        //        return null;
        //    }

        //    MembershipUser user = GetUser(rmodel.user.Name, true);

        //    if (user == null)
        //    {

        //        UserRepository userRep = new UserRepository();
        //        rmodel.login.Password = GetMD5Hash(rmodel.login.Password);
        //        if (userRep.RegisterUser(rmodel) == 0)
        //        {
        //            status = MembershipCreateStatus.Success;

        //            return GetUser(rmodel.user.Name, true);
        //        }
        //        else 
        //        {
        //            status = MembershipCreateStatus.DuplicateUserName;
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        status = MembershipCreateStatus.DuplicateUserName;
        //    }

        //    return null;
        //}

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            UserRepository userRep = new UserRepository();
            DatContext db = new DatContext();
            LoginMaster user = userRep.GetAllUsers().SingleOrDefault(u => u.LoginName == username);

            if (user != null)
            {
                MembershipUser memUser = new MembershipUser("Authendication", username, user.UserID, user.UserMst.Address,
                                                            string.Empty, string.Empty,
                                                            true, false, DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.MinValue,
                                                            DateTime.Now, DateTime.Now);
                return memUser;
            }
            return null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 6; }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { return false; }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            string sha1Pswd = GetMD5Hash(password);
            UserRepository user = new UserRepository();
            bool userObj = user.GetUserObjByUserName(username, sha1Pswd);
            if (userObj != false)
                return true;
            return false;
        }

        public static string GetMD5Hash(string value)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}