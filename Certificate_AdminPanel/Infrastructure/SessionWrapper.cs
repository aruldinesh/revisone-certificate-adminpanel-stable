﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Certificate_AdminPanel
{

    public static class SessionWrapper
    {
        //for admin
        public static bool IsAdmin
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID+"IsAdmin"])
                    return Convert.ToBoolean(HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "IsAdmin"]);
                else
                    return false;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "IsAdmin"] = value;
            }
        }


        /// <summary>
        /// 
        /// Get the Role Of Admin
        /// Admin Level
        /// 
        /// 0 = Super Admin
        /// 1 = Sub Admin
        /// 
        /// </summary>
        public static AdminLevel AdminLevel
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminLevel"])
                    return (AdminLevel)HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminLevel"];
                else
                    return AdminLevel.None;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminLevel"] = value;
            }
        }

        //for login display name
        public static string DisplayName
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayName"])
                    return HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayName"].ToString();
                else
                    return "Thinture Technologies pvt,ltd.,";
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayName"] = value;
            }
        }
        //for display admin logo
        public static string DisplayLogo
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayLogo"])
                    return HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayLogo"].ToString();
                else
                    return "Guest";
 
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "DisplayLogo"] = value;
            }
 
        }
        //for admin id 
        public static Int64 AdminID
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminID"])
                    return Convert.ToInt64( HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminID"]);
                else return 0;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "AdminID"] = value;
            }
        }

        //for user company id
        public static Int64 CompanyID
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "CompanyID"])
                    return Convert.ToInt64( HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "CompanyID"]);
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "CompanyID"] = value;
            }
        }

        //for user id
        public static Int64 UserID
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"])
                    return Convert.ToInt64(HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"]);
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"] = value;
            }
        }

        public static Int64 Inspection
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"])
                    return Convert.ToInt64(HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"]);
                else
                    return 0;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "UserID"] = value;
            }
        }
        //public static Int64 InspectionID
        //{
        //    get
        //    {
        //        if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "InspectionID"])
        //            return Convert.ToInt64(HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "InspectionID"]);
        //        else
        //            return 0;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "InspectionID"] = value;
        //    }
        //}

        public static int TimeZone
        {
            get
            {
                if (null != HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "TimeZone"])
                    return Convert.ToInt16(HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "TimeZone"]);
                else return 0;
            }
            set
            {
                HttpContext.Current.Session[HttpContext.Current.Session.SessionID + "TimeZone"] = value;
            }
        }


        //for logfile
        public static string LogFile
        {
            get{return "d:/logs/logs.txt";}
        }
     }
}

// for user level validation 1,2,3,4,5
[Flags]
public enum AdminLevel {Admin,SubAdmin,CompanyAdmin,CompanyUser,Inspection,None};